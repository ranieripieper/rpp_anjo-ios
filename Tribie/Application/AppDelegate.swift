//
//  AppDelegate.swift
//  Tribie
//
//  Created by Gilson Gil on 9/14/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
    SVProgressHUDHelper.setUp()
    Fabric.with([Crashlytics.self])
    window = UIWindow(frame: UIScreen.main.bounds)
    window?.makeKeyAndVisible()
    loadApplication(fromSignup: false)
    return true
  }
  
  func applicationWillEnterForeground(_ application: UIApplication) {
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: TribieConstants.reloadMainFromBackgroundNotificationKey), object: nil)
  }
}

// MARK: - Public
extension AppDelegate {
  func loadApplication(fromSignup: Bool) {
    if !onboarded() {
      window?.rootViewController = OnboardingViewController()
    } else if User.current != nil {
      window?.rootViewController = MainNavigationController(fromSignup: fromSignup)
    } else {
      window?.rootViewController = LoginContainer()
    }
  }
}

// MARK: - Private
fileprivate extension AppDelegate {
  func onboarded() -> Bool {
    return UserDefaults.standard.bool(forKey: TribieConstants.onboardedUserDefaultsKey)
  }
}
