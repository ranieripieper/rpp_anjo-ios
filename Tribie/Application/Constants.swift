//
//  Constants.swift
//  Tribie
//
//  Created by Gilson Gil on 9/20/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct TribieConstants {
  // Animations
  static let defaultAnimationDuration: TimeInterval = 0.3
  static let defaultAnimationDelay: TimeInterval = 0
  
  // Distance
  static let textFieldVerticalMargin: Float = 20
  static let textFieldHorizontalMargin: Float = 12
  
  // UserDefaults
  static let onboardedUserDefaultsKey = "ONBOARDEDUSERDEFAULTSKEY"
  
  // Notifications
  static let reloadChatViewNotificationKey = "RELOADCHATVIEWNOTIFICATIONKEY"
  static let reloadMainFromBackgroundNotificationKey = "RELOADMAINFROMBACKGROUNDNOTIFICATIONKEY"
}
