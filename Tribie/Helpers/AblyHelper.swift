//
//  AblyHelper.swift
//  Tribie
//
//  Created by Gilson Gil on 9/15/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import Ably

protocol AblyHelperDelegate {
  func handle(_ event: Event)
  func append(paginatedResult: ARTPaginatedResult<ARTMessage>)
  func remove(paginatedResult: ARTPaginatedResult<ARTMessage>)
}

struct AblyHelper {
  fileprivate static let ablyAPIKey = "-akOoA.UK_i9Q:nch6mdKWmjHULyPn"
  
  fileprivate let ably = ARTRealtime(key: AblyHelper.ablyAPIKey)
  
  fileprivate let userId: String
  
  var delegate: AblyHelperDelegate?
  
  init(userId: String) {
    self.userId = userId
    self.delegate = nil
  }
}

extension AblyHelper {
  func connect(completion: @escaping (Bool) -> ()) {
    ably.connect()
    ably.connection.on(.connected) { _ in
      let channel = "tribie:user_" + self.userId
      self.subscribeTo(channelString: channel)
      completion(true)
    }
    ably.connection.on(.failed) {_ in 
      completion(false)
    }
  }
  
  func disconnect() {
    ably.connection.close()
  }
  
  func subscribeTo(channelString: String) {
    let channel = ably.channels.get(channelString)
    channel.subscribe(handleIncomingMessage)
//    channel.on(.attached) { error in
//      guard channelString.contains("discomfort") else {
//        return
//      }
//      let query = ARTRealtimeHistoryQuery()
//      query.untilAttach = true
//      query.limit = 5
//      try? channel.history(query) { results, error in
//        self.handleHistory(results)
//      }
//    }
  }
  
//  func handleHistory(_ paginatedResult: ARTPaginatedResult<ARTMessage>?) {
//    guard let messages = paginatedResult?.items, messages.count > 0 else {
//      return
//    }
//    RealmHelper.hasEvent(with: messages.first?.id) { has in
//      guard !has else {
//        return
//      }
//      RealmHelper.hasEvent(with: messages.last?.id) { has in
//        guard !has else {
//          NotificationCenter.default.post(name: Notification.Name(rawValue: TribieConstants.reloadChatViewNotificationKey), object: nil)
//          return
//        }
//        guard let paginatedResult = paginatedResult else {
//          NotificationCenter.default.post(name: Notification.Name(rawValue: TribieConstants.reloadChatViewNotificationKey), object: nil)
//          return
//        }
//        self.delegate?.append(paginatedResult: paginatedResult)
//        self.next(paginatedResult)
//      }
//      messages.forEach {
//        Event.event($0) { event in
//          guard let event = event else {
//            return
//          }
//          RealmHelper.add(event: event)
//        }
//      }
//    }
//  }
  
//  func next(_ paginatedResult: ARTPaginatedResult<ARTMessage>) {
//    paginatedResult.next { result, error in
//      self.handleHistory(result)
//      self.delegate?.remove(paginatedResult: paginatedResult)
//    }
//  }
  
  func unsubscribe(to channel: String) {
    let channel = ably.channels.get(channel)
    channel.unsubscribe()
  }
  
  func handleIncomingMessage(message: ARTMessage) {
    Event.event(message) { event in
      guard let event = event else {
        return
      }
      RealmHelper.add(event: event)
      self.delegate?.handle(event)
    }
  }
  
  func send(message: String, to discomfort: Discomfort, from username: String, completion: @escaping (_ inner: () throws -> ()) -> ()) {
    guard let channel = discomfort.channel else {
      return
    }
    let dict = ["channel": channel, "message": message, "username": username, "discomfort": ["description": discomfort.description, "id": discomfort.discomfortId, "status": "talking", "title": discomfort.title, "tribier_id": discomfort.tribierId ?? 0, "tribier_nickname": discomfort.tribierNickname ?? "", "user_id": discomfort.userId ?? 0, "user_nickname": discomfort.userNickname ?? ""]] as [String : Any]
    ably.channels.get(channel).publish("message", data: dict, clientId: userId) { error in
      guard error == nil else {
        completion { throw error! }
        return
      }
      
      completion { return }
    }
  }
}
