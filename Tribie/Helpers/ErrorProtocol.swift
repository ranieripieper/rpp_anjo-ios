//
//  ErrorProtocol.swift
//  Tribie
//
//  Created by Gilson Gil on 9/20/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

protocol ErrorProtocol: Error {
  func localizedDescription() -> String
}
