//
//  NSAttributedString+Tribie.swift
//  Tribie
//  
//
//  Created by Gilson Gil on 9/21/16. 
//    Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

extension NSAttributedString {
  static func placeholder(string: String) -> NSAttributedString {
    let attr = NSAttributedString(string: string, attributes: [NSForegroundColorAttributeName: UIColor.trbPinkishGrey, NSFontAttributeName: UIFont.avenirRoman(size: 15)])
    return attr
  }
  
  static func placeholderError(string: String) -> NSAttributedString {
    let attr = NSAttributedString(string: string, attributes: [NSForegroundColorAttributeName: UIColor.trbRedPink, NSFontAttributeName: UIFont.avenirRoman(size: 15)])
    return attr
  }
  
  static func textView(string: String) -> NSAttributedString {
    let attr = NSAttributedString(string: string, attributes: textViewAttributes())
    return attr
  }
  
  static func textViewAttributes() -> [String: AnyObject] {
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.lineBreakMode = .byWordWrapping
    return [NSForegroundColorAttributeName: UIColor.trbBrownishGrey, NSFontAttributeName: UIFont.avenirRoman(size: 15.3), NSParagraphStyleAttributeName: paragraphStyle]
  }
}
