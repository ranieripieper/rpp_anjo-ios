//
//  NavigationControllerProtocol.swift
//  Tribie
//
//  Created by Gilson Gil on 9/14/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

import UIKit

protocol NavigationControllerProtocol {
  var navigationController: UINavigationController? { get }
  var navigationItem: UINavigationItem { get }
  
  func push(viewController: UIViewController)
}

extension NavigationControllerProtocol {
  func push(viewController: UIViewController) {
    navigationController?.pushViewController(viewController, animated: true)
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
  }
}
