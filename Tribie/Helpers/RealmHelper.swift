//
//  RealmHelper.swift
//  Tribie
//
//  Created by Gil on 30/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import RealmSwift

struct RealmHelper {
  private static let thread = OneThread(start: true, queue: nil)
  
  static func createEvent(_ id: String, name: String?, clientId: String?, channel: String, message: String, date: Date?, discomfortDict: [String: AnyObject], completion: @escaping (Event?) -> ()) {
    thread.enqueue {
      let event = Event()
      event.id = id
      event.name = name ?? ""
      event.clientId = Int(clientId ?? "") ?? 0
      let aChannel: String
      if channel.hasPrefix("tribie:") {
        aChannel = channel
      } else {
        aChannel = "tribie:" + channel
      }
      event.channel = aChannel
      event.message = message
      event.date = date ?? Date()
      if let discomfortDict = discomfortDict["discomfort"] as? [String: AnyObject] {
        event.discomfortId = discomfortDict["id"] as? Int ?? 0
        event.discomfortName = discomfortDict["title"] as? String ?? ""
        event.discomfortDescription = discomfortDict["description"] as? String ?? ""
        event.discomfortTribierNickname = discomfortDict["tribier_nickname"] as? String ?? ""
        event.discomfortUserNickname = discomfortDict["user_nickname"] as? String ?? ""
        event.discomfortTribierId = discomfortDict["tribier_id"] as? Int ?? 0
        event.discomfortUserId = discomfortDict["user_id"] as? Int ?? 0
      }
      completion(event)
    }
  }
  
  static func add(event: Event) {
    thread.enqueue {
      guard let realm = try? Realm() else {
        return
      }
      try? realm.write {
        realm.add(event, update: true)
      }
    }
  }
  
  static func hasEvent(with id: String?, completion: @escaping (Bool) -> ()) {
    guard let id = id else {
      completion(true)
      return
    }
    thread.enqueue {
      guard let realm = try? Realm() else {
        completion(false)
        return
      }
      let hasEvent = realm.object(ofType: Event.self, forPrimaryKey: id) != nil
      completion(hasEvent)
    }
  }
  
  static func addHistory(events: [Event], completion: @escaping (Bool) -> ()) {
    guard let lastEvent = events.last else {
      completion(false)
      return
    }
    thread.enqueue {
      guard let realm = try? Realm() else {
        return
      }
      let newEvents = realm.object(ofType: Event.self, forPrimaryKey: lastEvent.id) == nil
      events.forEach {
        self.add(event: $0)
      }
      completion(newEvents)
    }
  }
  
  static func notification(_ channel: String, token: @escaping (NotificationToken?) -> (), completion: @escaping ([Event]) -> ()) {
    thread.enqueue {
      guard let realm = try? Realm() else {
        return
      }
      let aChannel: String
      if channel.hasPrefix("tribie:") {
        aChannel = channel
      } else {
        aChannel = "tribie:" + channel
      }
      let predicate = NSPredicate(format: "name == %@ && channel == %@", argumentArray: ["message", aChannel])
      let results = realm.objects(Event.self).filter(predicate).sorted(byProperty: "date", ascending: true)
      let eventToken = results.addNotificationBlock { changes in
        switch changes {
        case .initial(let events):
          completion(events.flatMap { $0 })
        case .update(let events, _, _, _):
          completion(events.flatMap { $0 })
        case .error:
          break
        }
      }
      token(eventToken)
    }
  }
  
  static func closeChannel(_ event: Event, completion: @escaping (Bool) -> ()) {
    thread.enqueue {
      guard let realm = try? Realm() else {
        return
      }
      let channel: String
      if event.channel.hasPrefix("tribie:") {
        channel = event.channel
      } else {
        channel = "tribie:" + event.channel
      }
      let alreadyClosedEvent = realm.objects(Event.self).filter("channel == %@ && name == %@", channel, event.name).first
      guard alreadyClosedEvent == nil else {
        completion(false)
        return
      }
      try? realm.write {
        realm.add(event)
      }
      completion(true)
    }
  }
  
  static func isClosed(_ channel: String?, completion: @escaping (Bool) -> ()) {
    guard let channel = channel else {
      completion(false)
      return
    }
    thread.enqueue {
      guard let realm = try? Realm() else {
        completion(false)
        return
      }
      let aChannel: String
      if channel.hasPrefix("tribie:") {
        aChannel = channel
      } else {
        aChannel = "tribie:" + channel
      }
      let alreadyClosedEvent = realm.objects(Event.self).filter("channel == %@ && name == %@", aChannel, EventType.closed.rawValue()).first
      completion(alreadyClosedEvent != nil)
    }
  }
  
  static func otherUser(in channel: String?, completion: @escaping (String) -> ()) {
    guard let channel = channel else {
      completion("")
      return
    }
    thread.enqueue {
      guard let realm = try? Realm() else {
        completion("")
        return
      }
      let events = realm.objects(Event.self).filter("channel == %@ || channel == %@", channel, "tribie:\(channel)")
      var isUser: Bool? = nil
      for event in events {
        if isUser == nil {
          if event.discomfortTribierId != 0 {
            isUser = !User.isCurrent(event.discomfortTribierId)
          }
        }
        if isUser != nil {
          if isUser! {
            if event.discomfortTribierNickname.characters.count > 0 {
              completion(event.discomfortTribierNickname)
              return
            }
          } else {
            if event.discomfortUserNickname.characters.count > 0 {
              completion(event.discomfortUserNickname)
              return
            }
          }
        }
      }
      completion("")
    }
  }
}
