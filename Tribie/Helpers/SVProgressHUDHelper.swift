//
//  SVProgressHUDHelper.swift
//  Tribie
//
//  Created by Gil on 28/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import SVProgressHUD

struct SVProgressHUDHelper {
  static func setUp() {
    let style = SVProgressHUDStyle.light
    SVProgressHUD.setDefaultStyle(style)
    let type = SVProgressHUDMaskType.gradient
    SVProgressHUD.setDefaultMaskType(type)
  }
  
  static func show() {
    SVProgressHUD.show()
  }
  
  static func dismiss() {
    SVProgressHUD.dismiss()
  }
}
