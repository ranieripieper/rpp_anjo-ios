//
//  String+Tribie.swift
//  Tribie
//
//  Created by Gilson Gil on 9/14/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

extension String {
  static func defaultErrorMessage() -> String {
    return NSLocalizedString("DEFAULT_ERROR_MESSAGE", comment: "")
  }
  
  static func signupNamePlaceholder() -> String {
    return NSLocalizedString("SIGNUP_NAME_PLACEHOLDER", comment: "")
  }
  static func signupEmailPlaceholder() -> String {
    return NSLocalizedString("SIGNUP_EMAIL_PLACEHOLDER", comment: "")
  }
  static func signupPasswordPlaceholder() -> String {
    return NSLocalizedString("SIGNUP_PASSWORD_PLACEHOLDER", comment: "")
  }
  static func signupAgePlaceholder() -> String {
    return NSLocalizedString("SIGNUP_AGE_PLACEHOLDER", comment: "")
  }
  static func signupTitle() -> String {
    return NSLocalizedString("SIGNUP_TITLE", comment: "")
  }
  static func signupSubtitle() -> String {
    return NSLocalizedString("SIGNUP_SUBTITLE", comment: "")
  }
  static func signupSignupButton() -> String {
    return NSLocalizedString("SIGNUP_SIGNUP_BUTTON", comment: "")
  }
  static func signupLoginButton() -> String {
    return NSLocalizedString("SIGNUP_LOGIN_BUTTON", comment: "")
  }
  static func signupLoginButtonHighlight() -> String {
    return NSLocalizedString("SIGNUP_LOGIN_BUTTON_HIGHLIGHT", comment: "")
  }
  static func signupTermsButton() -> String {
    return NSLocalizedString("SIGNUP_TERMS_BUTTON", comment: "")
  }
  static func signupTermsButtonHighlight() -> String {
    return NSLocalizedString("SIGNUP_TERMS_BUTTON_HIGHLIGHT", comment: "")
  }
  static func signupErrorAgeMinor() -> String {
    return NSLocalizedString("SIGNUP_ERROR_AGE_MINOR", comment: "")
  }
  static func signupErrorInvalidPassword() -> String {
    return NSLocalizedString("SIGNUP_ERROR_INVALID_PASSWORD", comment: "")
  }
  
  static func termsTitle() -> String {
    return NSLocalizedString("TERMS_TITLE", comment: "")
  }
  
  static func loginTitle() -> String {
    return NSLocalizedString("LOGIN_TITLE", comment: "")
  }
  static func loginEmailPlaceholder() -> String {
    return NSLocalizedString("LOGIN_EMAIL_PLACEHOLDER", comment: "")
  }
  static func loginPasswordPlaceholder() -> String {
    return NSLocalizedString("LOGIN_PASSWORD_PLACEHOLDER", comment: "")
  }
  static func loginLoginButton() -> String {
    return NSLocalizedString("LOGIN_LOGIN_BUTTON", comment: "")
  }
  static func loginForgotPasswordButton() -> String {
    return NSLocalizedString("LOGIN_FORGOTPASSWORD_BUTTON", comment: "")
  }
  static func loginForgotPasswordButtonHighlight() -> String {
    return NSLocalizedString("LOGIN_FORGOTPASSWORD_BUTTON_HIGHLIGHT", comment: "")
  }
  static func loginErrorWrongCredentials() -> String {
    return NSLocalizedString("LOGIN_ERROR_WRONG_CREDENTIALS", comment: "")
  }
  static func loginForgotPasswordSuccessMessage() -> String {
    return NSLocalizedString("LOGIN_FORGOTPASSWORD_SUCCESS_MESSAGE", comment: "")
  }
  
  static func menuDisappearedText() -> String {
    return NSLocalizedString("MENU_DISAPPEARED_TEXT", comment: "")
  }
  static func menuAccomplishedText() -> String {
    return NSLocalizedString("MENU_ACCOMPLISHED_TEXT", comment: "")
  }
  static func menuNonsenseText() -> String {
    return NSLocalizedString("MENU_NONSENSE_TEXT", comment: "")
  }
  static func menuSettingsText() -> String {
    return NSLocalizedString("MENU_SETTINGS_TEXT", comment: "")
  }
  static func menuDisappearedSelectedText() -> String {
    return NSLocalizedString("MENU_DISAPPEARED_SELECTEDTEXT", comment: "")
  }
  static func menuAccomplishedSelectedText() -> String {
    return NSLocalizedString("MENU_ACCOMPLISHED_SELECTEDTEXT", comment: "")
  }
  static func menuNonsenseSelectedText() -> String {
    return NSLocalizedString("MENU_NONSENSE_SELECTEDTEXT", comment: "")
  }
  static func menuSettingsSelectedText() -> String {
    return NSLocalizedString("MENU_SETTINGS_SELECTEDTEXT", comment: "")
  }
  
  static func settingsGetInvites() -> String {
    return NSLocalizedString("SETTINGS_GETINVITES", comment: "")
  }
  static func settingsNotifications() -> String {
    return NSLocalizedString("SETTINGS_NOTIFICATIONS", comment: "")
  }
  static func settingsLogout() -> String {
    return NSLocalizedString("SETTINGS_LOGOUT", comment: "")
  }
  static func settingsDeleteAccount() -> String {
    return NSLocalizedString("SETTINGS_DELETEACCOUNT", comment: "")
  }
  static func settingsDeleteAccountTitle() -> String {
    return NSLocalizedString("SETTINGS_DELETEACCOUNT_TITLE", comment: "")
  }
  static func settingsDeleteAccountSubtitle() -> String {
    return NSLocalizedString("SETTINGS_DELETEACCOUNT_SUBTITLE", comment: "")
  }
  static func settingsDeleteAccountYes() -> String {
    return NSLocalizedString("SETTINGS_DELETEACCOUNT_YES", comment: "")
  }
  static func settingsDeleteAccountNo() -> String {
    return NSLocalizedString("SETTINGS_DELETEACCOUNT_NO", comment: "")
  }

  static func welcomeTitle() -> String {
    return NSLocalizedString("WELCOME_TITLE", comment: "")
  }
  static func welcomeContent() -> String {
    return NSLocalizedString("WELCOME_CONTENT", comment: "")
  }
  static func welcomeButton() -> String {
    return NSLocalizedString("WELCOME_BUTTON", comment: "")
  }
  
  static func createProblemDescription() -> String {
    return NSLocalizedString("CREATEPROBLEM_DESCRIPTION", comment: "")
  }
  static func createProblemTitle() -> String {
    return NSLocalizedString("CREATEPROBLEM_TITLE", comment: "")
  }
  static func createProblemTitlePlaceholder() -> String {
    return NSLocalizedString("CREATEPROBLEM_TITLE_PLACEHOLDER", comment: "")
  }
  static func createProblemDescriptionPlaceholder() -> String {
    return NSLocalizedString("CREATEPROBLEM_DESCRIPTION_PLACEHOLDER", comment: "")
  }
  static func createProblemSearchButton() -> String {
    return NSLocalizedString("CREATEPROBLEM_SEARCH_BUTTON", comment: "")
  }
  
  static func waitingMatchLabel() -> String {
    return NSLocalizedString("WAITINGMATCH_LABEL", comment: "")
  }
  static func waitingMatchReport() -> String {
    return NSLocalizedString("WAITINGMATCH_REPORT", comment: "")
  }
  
  static func chatInputPlaceholder(recieverName: String) -> String {
    return String(format: NSLocalizedString("CHAT_INPUT_PLACEHOLDER", comment: ""), arguments: [recieverName])
  }
  
  static func newConnectionIntroTitle(name: String) -> String {
    return String(format: NSLocalizedString("NEWCONNECTION_INTRO_TITLE", comment: ""), arguments: [name])
  }
  static func newConnectionUserDescription(name: String, discomfortTitle: String) -> String {
    return String(format: NSLocalizedString("NEWCONNECTION_USER_DESCRIPTION", comment: ""), arguments: [name, discomfortTitle])
  }
  static func newConnectionOk() -> String {
    return NSLocalizedString("NEWCONNECTION_OK", comment: "")
  }
  static func newConnectionTribierDescription(name: String, discomfortTitle: String, discomfortDescription: String) -> String {
    return String(format: NSLocalizedString("NEWCONNECTION_TRIBIER_DESCRIPTION", comment: ""), arguments: [name, discomfortTitle, discomfortDescription, name, name])
  }
  static func newConnectionHelp(name: String) -> String {
    return String(format: NSLocalizedString("NEWCONNECTION_HELP", comment: ""), arguments: [name])
  }
  static func newConnectionNext() -> String {
    return NSLocalizedString("NEWCONNECTION_NEXT", comment: "")
  }
  static func newConnectionReport(name: String) -> String {
    return String(format: NSLocalizedString("NEWCONNECTION_REPORT", comment: ""), arguments: [name])
  }
  static func newConnectionReportHighlight() -> String {
    return NSLocalizedString("NEWCONNECTION_REPORT_HIGHLIGHT", comment: "")
  }
  static func newConnectionHelpTitle() -> String {
    return NSLocalizedString("NEWCONNECTION_HELP_TITLE", comment: "")
  }
  static func newConnectionHelpDescription(name: String, discomfortTitle: String) -> String {
    return String(format: NSLocalizedString("NEWCONNECTION_HELP_DESCRIPTION", comment: ""), arguments: [name, discomfortTitle])
  }
  static func newConnectionReportTitle() -> String {
    return NSLocalizedString("NEWCONNECTION_REPORT_TITLE", comment: "")
  }
  static func newConnectionReportDescription() -> String {
    return NSLocalizedString("NEWCONNECTION_REPORT_DESCRIPTION", comment: "")
  }
  
  static func alertAccomplishedTitle() -> String {
    return NSLocalizedString("ALERT_ACCOMPLISHED_TITLE", comment: "")
  }
  static func alertAccomplishedSubtitle(name: String) -> String {
    return String(format: NSLocalizedString("ALERT_ACCOMPLISHED_SUBTITLE", comment: ""), arguments: [name])
  }
  static func alertGoneTitle(name: String) -> String {
    return String(format: NSLocalizedString("ALERT_GONE_TITLE", comment: ""), arguments: [name])
  }
  static func alertGoneSubtitle(name: String) -> String {
    return String(format: NSLocalizedString("ALERT_GONE_SUBTITLE", comment: ""), arguments: [name])
  }
  
  static func onboardingSkip() -> String {
    return NSLocalizedString("ONBOARDING_SKIP", comment: "")
  }
  static func onboardingNext() -> String {
    return NSLocalizedString("ONBOARDING_NEXT", comment: "")
  }
  static func onboardingStart() -> String {
    return NSLocalizedString("ONBOARDING_START", comment: "")
  }
  static func onboarding1Title() -> String {
    return NSLocalizedString("ONBOARDING_1_TITLE", comment: "")
  }
  static func onboarding1Subtitle() -> String {
    return NSLocalizedString("ONBOARDING_1_SUBTITLE", comment: "")
  }
  static func onboarding2Title() -> String {
    return NSLocalizedString("ONBOARDING_2_TITLE", comment: "")
  }
  static func onboarding2Subtitle() -> String {
    return NSLocalizedString("ONBOARDING_2_SUBTITLE", comment: "")
  }
  static func onboarding3Title() -> String {
    return NSLocalizedString("ONBOARDING_3_TITLE", comment: "")
  }
  static func onboarding3Subtitle() -> String {
    return NSLocalizedString("ONBOARDING_3_SUBTITLE", comment: "")
  }
  static func onboarding4Title() -> String {
    return NSLocalizedString("ONBOARDING_4_TITLE", comment: "")
  }
  static func onboarding4Subtitle() -> String {
    return NSLocalizedString("ONBOARDING_4_SUBTITLE", comment: "")
  }
}
