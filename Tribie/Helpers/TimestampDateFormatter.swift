//
//  TimestampDateFormatter.swift
//  Tribie
//
//  Created by Gilson Gil on 9/22/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct TimestampDateFormatter {  
  static let dateFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.dateStyle = DateFormatter.Style.short
    dateFormatter.locale = NSLocale.current
    dateFormatter.timeZone = NSTimeZone.default
    dateFormatter.doesRelativeDateFormatting = true
    return dateFormatter
  }()
  
  static let timeFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = NSLocale.current
    dateFormatter.timeZone = NSTimeZone.default
    dateFormatter.dateFormat = "HH'h'mm"
    return dateFormatter
  }()
}
