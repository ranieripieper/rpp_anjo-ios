//
//  UIImage+Tribie.swift
//  Tribie
//  
//
//  Created by Gilson Gil on 9/21/16. 
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

extension UIImage {
  static func icnSend() -> UIImage? {
    return UIImage(named: "icnSend")
  }
  
  static func bgWelcome() -> UIImage? {
    return UIImage(named: "bgWelcome")
  }
  
  static func imgBottomTerms() -> UIImage? {
    return UIImage(named: "imgBottomTerms")
  }
  
  static func bgChat() -> UIImage? {
    return UIImage(named: "bgChat")
  }
  
  static func icnMenu() -> UIImage? {
    return UIImage(named: "icnMenu")
  }
  
  static func bgNewInvite() -> UIImage? {
    return UIImage(named: "bgNewInvite")
  }
  
  static func icnTime() -> UIImage? {
    return UIImage(named: "icnTime")
  }
  
  static func icnSettings() -> UIImage? {
    return UIImage(named: "icnSettings")
  }
  
  static func icnReport() -> UIImage? {
    return UIImage(named: "icnReport")
  }
  
  static func icnAccomplished() -> UIImage? {
    return UIImage(named: "icnAccomplished")
  }
  
  static func icnHidePassword() -> UIImage? {
    return UIImage(named: "icnHidePassword")
  }
  
  static func btnVerSenha() -> UIImage? {
    return UIImage(named: "btnVerSenha")
  }
  
  static func img48() -> UIImage? {
    return UIImage(named: "img48")
  }
  
  static func imgWelcome() -> UIImage? {
    return UIImage(named: "imgWelcome")
  }
  
  static func imgNew() -> UIImage? {
    return UIImage(named: "imgNew")
  }
  
  static func imgPrivacy() -> UIImage? {
    return UIImage(named: "imgPrivacy")
  }
  
  static func imgHelping() -> UIImage? {
    return UIImage(named: "imgHelping")
  }
}
