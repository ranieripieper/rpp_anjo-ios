//
//  UpdatableCell.swift
//
//  Created by Gilson Gil on 3/21/16.
//  Copyright © 2016 Equipa. All rights reserved.
//

import UIKit

protocol Updatable: class {
  associatedtype ViewModel
  
  func update(viewModel: ViewModel)
}

struct CellConfigurator<Cell> where Cell: Updatable {
  let viewModel: Cell.ViewModel
  let reuseIdentifier: String = NSStringFromClass(Cell.self)
  let cellClass: AnyClass = Cell.self
  
  func update(cell: UITableViewCell) {
    if let cell = cell as? Cell {
      cell.update(viewModel: viewModel)
    }
  }
  
  func update(cell: UICollectionViewCell) {
    if let cell = cell as? Cell {
      cell.update(viewModel: viewModel)
    }
  }
  
  func update(header: UITableViewHeaderFooterView) {
    if let header = header as? Cell {
      header.update(viewModel: viewModel)
    }
  }
  
  func register(tableView: UITableView?) {
    if cellClass.isSubclass(of: UITableViewCell.self) {
      tableView?.register(cellClass, forCellReuseIdentifier: reuseIdentifier)
    } else {
      tableView?.register(cellClass, forHeaderFooterViewReuseIdentifier: reuseIdentifier)
    }
  }
  
  func register(collectionView: UICollectionView?) {
    collectionView?.register(cellClass, forCellWithReuseIdentifier: reuseIdentifier)
  }
  
  func currentViewModel() -> Any {
    return viewModel
  }
}

protocol CellConfiguratorType {
  var reuseIdentifier: String { get }
  var cellClass: AnyClass { get }
  
  func update(cell: UITableViewCell)
  func update(cell: UICollectionViewCell)
  func update(header: UITableViewHeaderFooterView)
  func register(tableView: UITableView?)
  func register(collectionView: UICollectionView?)
  func currentViewModel() -> Any
}

extension CellConfigurator: CellConfiguratorType {}

extension UITableView {
  func register(configurators: [CellConfiguratorType]?) {
    configurators?.forEach {
      $0.register(tableView: self)
    }
  }
}

extension UICollectionView {
  func register(configurators: [CellConfiguratorType]?) {
    configurators?.forEach {
      $0.register(collectionView: self)
    }
  }
}
