//
//  Discomfort.swift
//  Tribie
//
//  Created by Gil on 27/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

enum DiscomfortStatus {
  case created, waitingResponse, tribierRejected, talking, missionAccomplished, userTalkingNonsense, userDisappeared, tribierDisappeared, tribierTalkingNonsense, tribierLogout, userLogout, tribierDeleteAccount, userDeleteAccount, userRejectedReport, tribierCanHelping, timeout, message
  
  init?(rawValue: String) {
    switch rawValue {
    case "created":
      self = .created
    case "waiting_response":
      self = .waitingResponse
    case "tribier_rejected":
      self = .tribierRejected
    case "talking":
      self = .talking
    case "mission_accomplished":
      self = .missionAccomplished
    case "user_talking_nonsense":
      self = .userTalkingNonsense
    case "user_disappeared":
      self = .userDisappeared
    case "tribier_disappeared":
      self = .tribierDisappeared
    case "tribier_talking_nonsense":
      self = .tribierTalkingNonsense
    case "tribier_logout":
      self = .tribierLogout
    case "user_logout":
      self = .userLogout
    case "tribier_delete_account":
      self = .tribierDeleteAccount
    case "user_delete_account":
      self = .userDeleteAccount
    case "tribier_rejected_with_report":
      self = .userRejectedReport
    case "tribier_can_helping":
      self = .tribierCanHelping
    case "timeout":
      self = .timeout
    case "message":
      self = .message
    default:
      return nil
    }
  }
  
  func priority() -> Int {
    switch self {
    case .talking:
      return 1
    case .created, .waitingResponse:
      return 3
    default:
      return 10
    }
  }
  
  func isChatable() -> Bool {
    switch self {
    case .talking:
      return true
    default:
      return false
    }
  }
}

struct Discomfort {
  let discomfortId: Int
  let title: String
  let description: String
  let status: DiscomfortStatus
  let userId: Int?
  let userNickname: String?
  let tribierId: Int?
  let tribierNickname: String?
  var otherNickname: String {
    guard let currentUserId = User.current?.userId else {
      return ""
    }
    if currentUserId == userId {
      return tribierNickname ?? ""
    } else {
      return userNickname ?? ""
    }
  }
  var channel: String? {
    guard let userId = userId, let tribierId = tribierId else {
      return nil
    }
    return "tribie:discomfort_\(discomfortId)_user_\(userId)_tribier_\(tribierId)"
  }
  var userImageId: String {
    let imageName = String((userNickname?.hash ?? userId ?? 0).imageIndex())
    return imageName
  }
  var tribierImageId: String {
    let imageName = String((tribierNickname?.hash ?? tribierId ?? 1).imageIndex())
    return imageName
  }
  var otherImageId: String {
    return String((otherNickname.hash).imageIndex())
  }
  
  init?(json: [String: AnyObject]) {
    guard let discomfortId = json["id"] as? Int, let title = json["title"] as? String, let description = json["description"] as? String, let statusRaw = json["status"] as? String, let status = DiscomfortStatus(rawValue: statusRaw) else {
      return nil
    }
    self.discomfortId = discomfortId
    self.title = title
    self.description = description
    self.status = status
    self.userId = json["user_id"] as? Int
    self.userNickname = json["user_nickname"] as? String
    self.tribierId = json["tribier_id"] as? Int
    self.tribierNickname = json["tribier_nickname"] as? String
  }
  
  func isChatable() -> Bool {
    return status.isChatable()
  }
  
  func isSubscribable() -> Bool {
    switch status {
    case .created, .talking, .tribierCanHelping, .waitingResponse:
      return true
    default:
      return false
    }
  }
}

extension Int {
  func imageIndex() -> Int {
    var n = abs(self)
    var sum = 0
    while n > 0 {
      sum += n % 10
      n /= 10
    }
    return (sum % 50) + 1
  }
}
