//
//  Event.swift
//  Tribie
//
//  Created by Gil on 30/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import RealmSwift
import Ably

enum EventType {
  case none, tribierAcceptTalking, tribierRejectTalking, tribierDeleteAccount, userDeleteAccount, tribierLogout, userLogout, userTalkingNonsense, userDisappeared, tribierDisappeared, tribierTalkingNonsense, missionAccomplished, tribierCanHelping, tribierResponseTimeout, message, closed
  
  init(rawValue: String) {
    switch rawValue {
    case "tribier_accept_talking":
      self = .tribierAcceptTalking
    case "tribier_reject_talking":
      self = .tribierRejectTalking
    case "tribier_delete_account":
      self = .tribierDeleteAccount
    case "user_delete_account":
      self = .userDeleteAccount
    case "tribier_logout":
      self = .tribierLogout
    case "user_logout":
      self = .userLogout
    case "user_talking_nonsense":
      self = .userTalkingNonsense
    case "user_disappeared":
      self = .userDisappeared
    case "tribier_disappeared":
      self = .tribierDisappeared
    case "tribier_talking_nonsense":
      self = .tribierTalkingNonsense
    case "mission_accomplished":
      self = .missionAccomplished
    case "tribier_can_helping":
      self = .tribierCanHelping
    case "tribier_response_timeout":
      self = .tribierResponseTimeout
    case "message":
      self = .message
    case "closed":
      self = .closed
    default:
      self = .none
    }
  }
  
  func rawValue() -> String {
    switch self {
    case .tribierAcceptTalking:
      return "tribier_accept_talking"
    case .tribierRejectTalking:
      return "tribier_reject_talking"
    case .tribierDeleteAccount:
      return "tribier_delete_account"
    case .userDeleteAccount:
      return "user_delete_account"
    case .tribierLogout:
      return "tribier_logout"
    case .userLogout:
      return "user_logout"
    case .userTalkingNonsense:
      return "user_talking_nonsense"
    case .userDisappeared:
      return "user_disappeared"
    case .tribierDisappeared:
      return "tribier_disappeared"
    case .tribierTalkingNonsense:
      return "tribier_talking_nonsense"
    case .missionAccomplished:
      return "mission_accomplished"
    case .tribierCanHelping:
      return "tribier_can_helping"
    case .tribierResponseTimeout:
      return "tribier_response_timeout"
    case .message:
      return "message"
    case .closed:
      return "closed"
    case .none:
      return ""
    }
  }
}

final class Event: Object {
  dynamic var id: String = ""
  dynamic var name: String = ""
  dynamic var message: String = ""
  dynamic var channel: String = ""
  dynamic var date: Date = Date()
  dynamic var clientId: Int = 0
  dynamic var discomfortId: Int = 0
  dynamic var discomfortName: String = ""
  dynamic var discomfortDescription: String = ""
  dynamic var discomfortStatus: String = ""
  dynamic var discomfortUserId: Int = 0
  dynamic var discomfortUserNickname: String = ""
  dynamic var discomfortTribierId: Int = 0
  dynamic var discomfortTribierNickname: String = ""
  var type: EventType {
    return EventType(rawValue: name)
  }
  var discomfort: Discomfort {
    return Discomfort(json: ["id": discomfortId as AnyObject, "title": discomfortName as AnyObject, "description": discomfortDescription as AnyObject, "user_nickname": discomfortUserNickname as AnyObject, "status": name as AnyObject, "user_id": discomfortUserId as AnyObject, "tribier_nickname": discomfortTribierNickname as AnyObject, "tribier_id": discomfortTribierId as AnyObject])!
  }
  
  override static func primaryKey() -> String? {
    return "id"
  }
  
  override static func indexedProperties() -> [String] {
    return ["date"]
  }
  
  override static func ignoredProperties() -> [String] {
    return ["type", "discomfort"]
  }
  
  static func event(_ message: ARTMessage, completion: @escaping (Event?) -> ()) {
    guard let dict = message.data as? [String: AnyObject], let channel = dict["channel"] as? String, let text = dict["message"] as? String else {
      completion(nil)
      return
    }
    
    RealmHelper.createEvent(message.id, name: message.name, clientId: message.clientId, channel: channel, message: text, date: message.timestamp, discomfortDict: dict, completion: completion)
  }
  
  static func closeEvent(_ discomfort: Discomfort, completion: @escaping (Bool?) -> ()) {
    guard let channel = discomfort.channel else {
      completion(nil)
      return
    }
    let dict: [String: AnyObject] = ["id": discomfort.discomfortId as AnyObject,
                                     "title": discomfort.title as AnyObject,
                                     "description": discomfort.description as AnyObject,
                                     "user_nickname": discomfort.otherNickname as AnyObject]
    let discomfortDict: [String: AnyObject] = ["discomfort": dict as AnyObject]
    RealmHelper.createEvent(channel.hash.description + channel, name: EventType.closed.rawValue(), clientId: nil, channel: channel, message: "", date: Date(), discomfortDict: discomfortDict) { event in
      guard let event = event else {
        completion(nil)
        return
      }
      RealmHelper.closeChannel(event) { success in
        completion(success)
      }
    }
  }
}
