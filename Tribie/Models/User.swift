//
//  User.swift
//  Tribie
//
//  Created by Gilson Gil on 9/14/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import Locksmith

struct User {
  let userId: Int
  let name: String
  let birthDate: String
  let profileType: String
  var username: String {
    return String(userId) + name
  }
  var avatarImageId: String {
    return String(username.hash.imageIndex())
  }
  
  init(userId: Int, name: String, birthDate: String, profileType: String) {
    self.userId = userId
    self.name = name
    self.birthDate = birthDate
    self.profileType = profileType
  }
  
  init?(json: [String: AnyObject]) {
    guard let userId = json["id"] as? Int, let name = json["full_name"] as? String, let birthDate = json["birth_date"] as? String, let profileType = json["profile_type"] as? String else {
      return nil
    }
    self.userId = userId
    self.name = name
    self.birthDate = birthDate
    self.profileType = profileType
  }
  
  func setTribier(_ tribier: Bool) {
    let newUser = User(userId: userId, name: name, birthDate: birthDate, profileType: tribier ? "tribier_user" : "common_user")
    newUser.persist(token: nil)
  }
  
  func persist(token: String?) {
    let user: [String: AnyObject] = [
      "userId": userId as AnyObject,
      "name": name as AnyObject,
      "birthDate": birthDate as AnyObject,
      "profileType": profileType as AnyObject
    ]
    UserDefaults.standard.set(user, forKey: "PersistedUser")
    UserDefaults.standard.synchronize()
    guard let token = token else {
      return
    }
    UserCredential.persistToken(username: username, token: token)
  }
  
  static var current: User? {
    guard let userData = UserDefaults.standard.object(forKey: "PersistedUser") as? [String: AnyObject], let userId = userData["userId"] as? Int, let name = userData["name"] as? String, let birthDate = userData["birthDate"] as? String, let profileType = userData["profileType"] as? String else {
      return nil
    }
    let user = User(userId: userId, name: name, birthDate: birthDate, profileType: profileType)
    return user
  }
  
  static func isCurrent(_ userId: Int?) -> Bool {
    guard let current = current, let userId = userId, current.userId == userId else {
      return false
    }
    return true
  }
  
  static func depersist() {
    UserDefaults.standard.removeObject(forKey: "PersistedUser")
    UserDefaults.standard.synchronize()
    UserCredential.flush()
  }
}

struct UserCredential: CreateableSecureStorable, ReadableSecureStorable, DeleteableSecureStorable, GenericPasswordSecureStorable {
  let username: String
  let token: String
  let service = "API"
  var account: String { return username }
  var data: [String : Any] { return ["token": token as Any] }
  
  static func persistToken(username: String, token: String) {
    do {
      try UserCredential(username: username, token: token).updateInSecureStore()
    } catch {
      do {
        try UserCredential(username: username, token: token).createInSecureStore()
      } catch {
        
      }
    }
  }
  
  static func flush() {
    guard let user = User.current, let data = UserCredential(username: user.username, token: "").readFromSecureStore()?.data, let _ = data["token"] as? String else {
      return
    }
    do {
      try UserCredential(username: user.username, token: "").deleteFromSecureStore()
      
    } catch {
      
    }
  }
  
  static func persistedToken() -> String? {
    guard let user = User.current, let data = UserCredential(username: user.username, token: "").readFromSecureStore()?.data, let token = data["token"] as? String else {
      return "6a601cecb227cef3631b0131a877d463"
    }
    return token
  }
}

