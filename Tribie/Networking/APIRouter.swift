//
//  APIRouter.swift
//  Tribie
//
//  Created by Gilson Gil on 9/14/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import Alamofire

enum NetworkingError: ErrorProtocol {
  case Custom(String)
  case UnexpectedReturnType
  
  init(_ message: String) {
    self = .Custom(message)
  }
  
  init?(_ json: [String: AnyObject]) {
    guard let isError = json["error"] as? Bool, isError else {
      return nil
    }
    guard let error = json["full_errors_messages"]?.firstObject as? String else {
      self = .UnexpectedReturnType
      return
    }
    self = .Custom(error)
  }
  
  func localizedDescription() -> String {
    switch self {
    case .Custom(let message):
      return message
    case .UnexpectedReturnType:
      return String.defaultErrorMessage()
    }
  }
}

enum APIRouter: URLRequestConvertible {
  static let baseURLString = "http://tribie.me/api/v1/"
  
  case signup(String, String, String, String)
  case signin(String, String)
  case recoverPassword(String)
  case signout
  case deleteAccount
  case currentUser
  case becomeTribier(String)
  case createDiscomfort(String, String)
  case discomforts
  case currentDiscomfort
  case currentDiscomfortHelping
  case acceptDiscomfort(String)
  case rejectDiscomfort(String, String?)
  case discomfortDisappeared(String, Bool)
  case discomfortNonsense(String, Bool)
  case discomfortAccomplished(String)
  
  func asURLRequest() throws -> URLRequest {
    let method: HTTPMethod = {
      switch self {
      case .signup, .signin, .recoverPassword, .createDiscomfort, .acceptDiscomfort, .rejectDiscomfort:
        return .post
      case .currentUser, .discomforts, .currentDiscomfort, .currentDiscomfortHelping:
        return .get
      case .signout, .deleteAccount:
        return .delete
      case .becomeTribier, .discomfortDisappeared, .discomfortNonsense, .discomfortAccomplished:
        return .put
      }
    }()
    
    let path: String = {
      switch self {
      case .signup:
        return "users"
      case .signin, .signout:
        return "users/auth"
      case .recoverPassword:
        return "users/password_reset"
      case .deleteAccount:
        return "users/me/account"
      case .currentUser:
        return "users/me"
      case .becomeTribier:
        return "users/me/profile_type"
      case .createDiscomfort:
        return "discomforts"
      case .discomforts:
        return "users/me/discomforts"
      case .currentDiscomfort:
        return "users/me/discomfort"
      case .currentDiscomfortHelping:
        return "users/me/discomforts_helping"
      case .acceptDiscomfort(let discomfortId):
        return "users/me/discomfort/\(discomfortId)/accept"
      case .rejectDiscomfort(let discomfortId, _):
        return "users/me/discomfort/\(discomfortId)/reject"
      case .discomfortDisappeared(let discomfortId, _):
        return "discomforts/\(discomfortId)/finish_talking"
      case .discomfortNonsense(let discomfortId, _):
        return "discomforts/\(discomfortId)/finish_talking"
      case .discomfortAccomplished(let discomfortId):
        return "discomforts/\(discomfortId)/finish_talking"
      }
    }()
    
    let parameters: Parameters? = {
      switch self {
      case .signup(let name, let email, let password, let birthDate):
        let user: [String: String] = ["name": name, "email": email, "password": password, "password_confirmation": password, "birth_date": birthDate]
        return ["user": user]
      case .signin(let email, let password):
        return ["email": email, "password": password]
      case .recoverPassword(let email):
        let user = ["email": email]
        return ["user": user]
      case .becomeTribier(let profileType):
        let user = ["profile_type": profileType]
        return ["user": user]
      case .createDiscomfort(let title, let description):
        let discomfort = ["title": title, "description": description]
        return ["discomfort": discomfort]
      case .discomforts:
        return ["per_page": 20, "page": 1]
      case .rejectDiscomfort(_, let parameter):
        guard let parameter = parameter else {
          return nil
        }
        return ["discomfort": ["status": parameter]]
      case .discomfortDisappeared(_, let isTribier):
        let status: String
        if isTribier {
          status = "user_disappeared"
        } else {
          status = "tribier_disappeared"
        }
        return ["discomfort": ["status": status]]
      case .discomfortNonsense(_, let isTribier):
        let status: String
        if isTribier {
          status = "user_talking_nonsense"
        } else {
          status = "tribier_talking_nonsense"
        }
        return ["discomfort": ["status": status]]
      case .discomfortAccomplished:
        return ["discomfort": ["status": "mission_accomplished"]]
      default:
        return nil
      }
    }()
    
    let encoding: ParameterEncoding = {
      switch self {
      case .discomforts:
        return URLEncoding.queryString
      default:
        return JSONEncoding.default
      }
    }()
    
    do {
      let url = try APIRouter.baseURLString.asURL().appendingPathComponent(path)
      var urlRequest = URLRequest(url: url)
      urlRequest.httpMethod = method.rawValue
      urlRequest.addValue("ios", forHTTPHeaderField: "X-Provider")
      urlRequest.addValue(NSLocale.current.identifier, forHTTPHeaderField: "X-Locale")
      if let token = UserCredential.persistedToken() {
        urlRequest.addValue(token, forHTTPHeaderField: "X-Token")
      }
      
      return try encoding.encode(urlRequest, with: parameters)
    } catch {
      throw error
    }
  }
}
