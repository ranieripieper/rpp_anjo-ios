//
//  DiscomfortService.swift
//  Tribie
//
//  Created by Gil on 27/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import Alamofire

enum DiscomfortError: Error, ErrorProtocol {
  case notFound
  
  func localizedDescription() -> String {
    switch self {
    case .notFound:
      return ""
    }
  }
}

struct DiscomfortService {
  static func create(_ title: String, description: String, completion: @escaping (_ inner: () throws -> ()) -> ()) {
    Alamofire.request(APIRouter.createDiscomfort(title, description))
      .responseJSON { response in
        guard response.result.isSuccess && response.result.error == nil else {
          completion { throw response.result.error! }
          return
        }
        guard let json = response.result.value as? [String: AnyObject] else {
          completion { throw NetworkingError.UnexpectedReturnType }
          return
        }
        let error = NetworkingError(json)
        guard error == nil else {
          completion { throw error! }
          return
        }
        completion { return }
    }
  }
  
  static func discomforts(_ completion: @escaping (_ inner: () throws -> [Discomfort]) -> ()) {
    Alamofire.request(APIRouter.discomforts)
      .responseJSON { response in
        guard response.result.isSuccess && response.result.error == nil else {
          completion { throw response.result.error! }
          return
        }
        guard let json = response.result.value as? [String: AnyObject] else {
          completion { throw NetworkingError.UnexpectedReturnType }
          return
        }
        let error = NetworkingError(json)
        guard error == nil else {
          completion { throw error! }
          return
        }
        guard let discomfortsData = json["data"] as? [[String: AnyObject]] else {
          return
        }
        let discomforts = discomfortsData.flatMap { Discomfort(json: $0) }
        completion { return discomforts }
    }
  }
  
  static func current(_ completion: @escaping (_ inner: () throws -> Discomfort) -> ()) {
    Alamofire.request(APIRouter.currentDiscomfort)
    .responseJSON { response in
      guard response.result.isSuccess && response.result.error == nil else {
        completion { throw response.result.error! }
        return
      }
      guard let json = response.result.value as? [String: AnyObject] else {
        completion { throw NetworkingError.UnexpectedReturnType }
        return
      }
      let error = NetworkingError(json)
      guard error == nil else {
        completion { throw error! }
        return
      }
      guard json.count > 0, let discomfort = Discomfort(json: json) else {
        completion { throw DiscomfortError.notFound }
        return
      }
      completion { return discomfort }
    }
  }
  
  static func currentHelping(_ completion: @escaping (_ inner: () throws -> Discomfort) -> ()) {
    Alamofire.request(APIRouter.currentDiscomfortHelping)
      .responseJSON { response in
        guard response.result.isSuccess && response.result.error == nil else {
          completion { throw response.result.error! }
          return
        }
        guard let json = response.result.value as? [String: AnyObject] else {
          completion { throw NetworkingError.UnexpectedReturnType }
          return
        }
        let error = NetworkingError(json)
        guard error == nil else {
          completion { throw error! }
          return
        }
        guard json.count > 0, let discomfort = Discomfort(json: json)else {
          completion { throw DiscomfortError.notFound }
          return
        }
        completion { return discomfort }
    }
  }
  
  static func accept(_ discomfortId: Int, completion: @escaping (_ inner: () throws -> ()) -> ()) {
    Alamofire.request(APIRouter.acceptDiscomfort(String(discomfortId)))
    .responseJSON { response in
      guard response.result.isSuccess && response.result.error == nil else {
        completion { throw response.result.error! }
        return
      }
      guard let json = response.result.value as? [String: AnyObject] else {
        completion { throw NetworkingError.UnexpectedReturnType }
        return
      }
      let error = NetworkingError(json)
      guard error == nil else {
        completion { throw error! }
        return
      }
      completion { return }
    }
  }
  
  static func reject(_ discomfortId: Int, report: Bool, completion: @escaping (_ inner: () throws -> ()) -> ()) {
    let params: String?
    if report {
      params = "tribier_rejected_with_report"
    } else {
      params = nil
    }
    Alamofire.request(APIRouter.rejectDiscomfort(String(discomfortId), params))
      .responseJSON { response in
        guard response.result.isSuccess && response.result.error == nil else {
          completion { throw response.result.error! }
          return
        }
        guard let json = response.result.value as? [String: AnyObject] else {
          completion { throw NetworkingError.UnexpectedReturnType }
          return
        }
        let error = NetworkingError(json)
        guard error == nil else {
          completion { throw error! }
          return
        }
        completion { return }
    }
  }
  
  static func disappeared(_ discomfortId: Int, tribierId: Int, completion: @escaping (_ inner: () throws -> ()) -> ()) {
    Alamofire.request(APIRouter.discomfortDisappeared(String(discomfortId), User.isCurrent(tribierId)))
      .responseJSON { response in
        guard response.result.isSuccess && response.result.error == nil else {
          completion { throw response.result.error! }
          return
        }
        guard let json = response.result.value as? [String: AnyObject] else {
          completion { throw NetworkingError.UnexpectedReturnType }
          return
        }
        let error = NetworkingError(json)
        guard error == nil else {
          completion { throw error! }
          return
        }
        completion { return }
    }
  }
  
  static func nonsense(_ discomfortId: Int, tribierId: Int, completion: @escaping (_ inner: () throws -> ()) -> ()) {
    Alamofire.request(APIRouter.discomfortNonsense(String(discomfortId), User.isCurrent(tribierId)))
      .responseJSON { response in
        guard response.result.isSuccess && response.result.error == nil else {
          completion { throw response.result.error! }
          return
        }
        guard let json = response.result.value as? [String: AnyObject] else {
          completion { throw NetworkingError.UnexpectedReturnType }
          return
        }
        let error = NetworkingError(json)
        guard error == nil else {
          completion { throw error! }
          return
        }
        completion { return }
    }
  }
  
  static func accomplished(_ discomfortId: Int, completion: @escaping (_ inner: () throws -> ()) -> ()) {
    Alamofire.request(APIRouter.discomfortAccomplished(String(discomfortId)))
      .responseJSON { response in
        guard response.result.isSuccess && response.result.error == nil else {
          completion { throw response.result.error! }
          return
        }
        guard let json = response.result.value as? [String: AnyObject] else {
          completion { throw NetworkingError.UnexpectedReturnType }
          return
        }
        let error = NetworkingError(json)
        guard error == nil else {
          completion { throw error! }
          return
        }
        completion { return }
    }
  }
}
