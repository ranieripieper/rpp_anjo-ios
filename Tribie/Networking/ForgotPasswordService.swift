//
//  ForgotPasswordService.swift
//  Tribie
//
//  Created by Gil on 27/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import Alamofire

struct ForgotPasswordService {
  static func reset(_ email: String, completion: @escaping (_ inner: () throws -> ()) -> ()) {
    Alamofire.request(APIRouter.recoverPassword(email))
      .responseJSON { response in
        guard response.result.isSuccess && response.result.error == nil else {
          completion { throw response.result.error! }
          return
        }
        guard let json = response.result.value as? [String: AnyObject] else {
          completion { throw NetworkingError.UnexpectedReturnType }
          return
        }
        let error = NetworkingError(json)
        guard error == nil else {
          completion { throw error! }
          return
        }
        completion { return }
    }
  }
}
