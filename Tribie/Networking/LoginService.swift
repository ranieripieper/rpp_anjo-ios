//
//  LoginService.swift
//  Tribie
//
//  Created by Gil on 27/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import Alamofire

struct LoginService {
  static func signin(_ email: String, password: String, completion: @escaping (_ inner: () throws -> ()) -> ()) {
    Alamofire.request(APIRouter.signin(email, password))
      .responseJSON { response in
        guard response.result.isSuccess && response.result.error == nil else {
          completion { throw response.result.error! }
          return
        }
        guard let json = response.result.value as? [String: AnyObject] else {
          completion { throw NetworkingError.UnexpectedReturnType }
          return
        }
        let error = NetworkingError(json)
        guard error == nil else {
          completion { throw error! }
          return
        }
        guard let userData = json["data"] as? [String: AnyObject] else {
          completion { throw NetworkingError.UnexpectedReturnType }
          return
        }
        guard let user = User(json: userData), let tokenData = json["auth_data"] as? [String: AnyObject], let token = tokenData["auth_token"] as? String else {
          completion { throw NetworkingError.UnexpectedReturnType }
          return
        }
        user.persist(token: token)
        completion { return }
    }
  }
  
  static func signout(_ completion: @escaping (_ inner: () throws -> ()) -> ()) {
    Alamofire.request(APIRouter.signout)
      .responseJSON { response in
        completion { return }
    }
  }
}
