//
//  SettingsService.swift
//  Tribie
//
//  Created by Gil on 04/10/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import Alamofire

struct SettingsService {
  static func becomeTribier(_ become: Bool, completion: @escaping (_ inner: () throws -> ()) -> ()) {
    let profileType: String
    if become {
      profileType = "tribier_user"
    } else {
      profileType = "common_user"
    }
    Alamofire.request(APIRouter.becomeTribier(profileType))
      .responseJSON { response in
        guard response.result.isSuccess && response.result.error == nil else {
          completion { throw response.result.error! }
          return
        }
        guard let json = response.result.value as? [String: AnyObject] else {
          completion { throw NetworkingError.UnexpectedReturnType }
          return
        }
        let error = NetworkingError(json)
        guard error == nil else {
          completion { throw error! }
          return
        }
        completion { return }
    }
  }
}
