//
//  AlertController.swift
//  Tribie
//
//  Created by Gil on 05/10/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

enum AlertControllerStyle {
  case alert
  case action
}

protocol AlertControllerDelegate: class {
  func deleteAccount()
  func didDismiss()
}

final class AlertController: UIViewController {
  fileprivate let style: AlertControllerStyle
  
  fileprivate let dimView: UIView = {
    let view = UIView()
    view.backgroundColor = .black
    view.alpha = 0
    return view
  }()
  
  fileprivate let avatarButton: RoundButton = {
    let button = RoundButton()
    button.isUserInteractionEnabled = false
    button.setBackgroundImage(UIImage.bgNewInvite(), for: .normal)
    button.imageView?.contentMode = .scaleAspectFill
    if let user = User.current {
      button.setImage(UIImage(named: user.avatarImageId), for: .normal)
    }
    return button
  }()
  
  fileprivate let backgroundView: UIView = {
    let view = UIView()
    view.backgroundColor = UIColor.trbWheat
    view.layer.cornerRadius = 14
    view.layer.masksToBounds = true
    view.alpha = 0.5
    return view
  }()
  
  weak var delegate: AlertControllerDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(title: String, message: String, style: AlertControllerStyle) {
    self.style = style
    super.init(nibName: nil, bundle: nil)
    setUp(title, message)
  }
  
  private func setUp(_ title: String, _ message: String) {
    view.addSubview(dimView)
    
    view.addSubview(backgroundView)
    
    view.addSubview(avatarButton)
    
    let titleLabel = createTitleLabel(title)
    backgroundView.addSubview(titleLabel)
    
    let messageLabel = createMessageLabel(message)
    backgroundView.addSubview(messageLabel)
    
    let margin: CGFloat = 20
    
    constrain(dimView, backgroundView, avatarButton, titleLabel, messageLabel) { dim, bg, avatar, title, message in
      dim.top == dim.superview!.top
      dim.left == dim.superview!.left
      dim.bottom == dim.superview!.bottom
      dim.right == dim.superview!.right
      
      bg.top >= bg.superview!.top + margin
      bg.left == bg.superview!.left + margin
      bg.bottom <= bg.superview!.bottom - margin
      bg.right == bg.superview!.right - margin
      bg.center == bg.superview!.center
      
      avatar.width == avatar.height
      avatar.centerX == avatar.superview!.centerX
      avatar.centerY == bg.top
      avatar.width == 120
      
      title.top == avatar.bottom + margin
      title.left >= title.superview!.left + margin
      title.right <= title.superview!.right - margin
      title.centerX == title.superview!.centerX
      
      message.top == title.bottom + margin
      message.left >= message.superview!.left + margin
      message.right <= message.superview!.right - margin
      message.centerX == message.superview!.centerX
    }
    
    switch style {
    case .action:
      let yesButton = createYesButton()
      backgroundView.addSubview(yesButton)
      
      let noButton = createNoButton()
      backgroundView.addSubview(noButton)
      
      let divisor = createDivisor()
      backgroundView.addSubview(divisor)
      
      constrain(yesButton, noButton, divisor, messageLabel) { yes, no, divisor, message in
        yes.top == message.bottom + margin
        yes.left == yes.superview!.left
        yes.bottom == yes.superview!.bottom
        yes.right == yes.superview!.centerX
        yes.height == 60
        
        no.top == message.bottom + margin
        no.left == no.superview!.centerX
        no.bottom == no.superview!.bottom
        no.right == no.superview!.right
        no.height == 60
        
        divisor.top == message.bottom + margin
        divisor.centerX == divisor.superview!.centerX
        divisor.width == 1
        divisor.bottom == divisor.superview!.bottom
      }
    case .alert:
      constrain(messageLabel) { message in
        message.bottom == message.superview!.bottom - margin
      }
    }
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesEnded(touches, with: event)
    guard !(touches.first?.view is UIControl) else {
      return
    }
    dismiss()
  }
}

// MARK: - Views
fileprivate extension AlertController {
  func createTitleLabel(_ title: String) -> UILabel {
    let label = UILabel()
    label.numberOfLines = 0
    label.textAlignment = .center
    label.textColor = UIColor.trbBrownishGrey
    label.text = title
    label.font = UIFont.champagneBold(size: 32)
    return label
  }
  
  func createMessageLabel(_ message: String) -> UILabel {
    let label = UILabel()
    label.numberOfLines = 0
    label.textAlignment = .center
    label.textColor = UIColor.trbBrownishGrey
    label.text = message
    label.font = UIFont.champagne(size: 20)
    return label
  }
  
  func createYesButton() -> UIButton {
    let button = UIButton(type: .custom)
    button.backgroundColor = .white
    button.setTitle(String.settingsDeleteAccountYes(), for: .normal)
    button.setTitleColor(UIColor.trbTiffanyBlue, for: .normal)
    button.titleLabel?.font = UIFont.champagne(size: 24)
    button.addTarget(self, action: #selector(yesTapped), for: .touchUpInside)
    return button
  }
  
  func createNoButton() -> UIButton {
    let button = UIButton(type: .custom)
    button.backgroundColor = .white
    button.setTitle(String.settingsDeleteAccountNo(), for: .normal)
    button.setTitleColor(UIColor.trbTiffanyBlue, for: .normal)
    button.titleLabel?.font = UIFont.champagneBold(size: 24)
    button.addTarget(self, action: #selector(noTapped), for: .touchUpInside)
    return button
  }
  
  func createDivisor() -> UIView {
    let view = UIView()
    view.backgroundColor = UIColor.trbWhiteTwo
    return view
  }
}

// MARK: - Actions
extension AlertController {
  func yesTapped() {
    delegate?.deleteAccount()
    dismiss()
  }
  
  func noTapped() {
    dismiss()
  }
}

// MARK: - Public
extension AlertController {
  static func present(_ title: String, message: String, style: AlertControllerStyle, in parentViewController: UIViewController) -> AlertController {
    let viewController = AlertController(title: title, message: message, style: style)
    DispatchQueue.main.async {
      viewController.willMove(toParentViewController: parentViewController)
      parentViewController.addChildViewController(viewController)
      viewController.view.frame = parentViewController.view.bounds
      viewController.didMove(toParentViewController: parentViewController)
      parentViewController.view.addSubview(viewController.view)
      viewController.backgroundView.transform = CGAffineTransform.identity.scaledBy(x: 0.01, y: 0.01)
      viewController.avatarButton.transform = CGAffineTransform.identity.scaledBy(x: 0.01, y: 0.01)
      UIView.animate(withDuration: TribieConstants.defaultAnimationDuration, delay: TribieConstants.defaultAnimationDelay, usingSpringWithDamping: 0.8, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
        viewController.backgroundView.transform = CGAffineTransform.identity
        viewController.backgroundView.alpha = 1
        viewController.avatarButton.transform = CGAffineTransform.identity
        viewController.dimView.alpha = 0.8
      }, completion: nil)
    }
    return viewController
  }
  
  func dismiss() {
    delegate?.didDismiss()
    UIView.animate(withDuration: TribieConstants.defaultAnimationDuration / 2, delay: TribieConstants.defaultAnimationDelay, options: .curveEaseInOut, animations: { [weak self] in
      self?.backgroundView.transform = CGAffineTransform.identity.scaledBy(x: 0.01, y: 0.01)
      self?.avatarButton.transform = CGAffineTransform.identity.scaledBy(x: 0.01, y: 0.01)
      self?.dimView.alpha = 0
      }, completion: { [weak self] _ in
        self?.view.removeFromSuperview()
        self?.removeFromParentViewController()
      })
  }
}
