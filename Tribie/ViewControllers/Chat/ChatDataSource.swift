//
//  ChatDataSource.swift
//  Tribie
//
//  Created by Gilson Gil on 9/21/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Chatto

final class ChatDataSource: ChatDataSourceProtocol {
  var hasMoreNext: Bool = false
  var hasMorePrevious: Bool = false
  var chatItems: [ChatItemProtocol] = []
  weak var delegate: ChatDataSourceDelegateProtocol?
  
  func loadNext() {
    
  }
  
  func loadPrevious() {
    
  }
  
  func adjustNumberOfMessages(preferredMaxCount: Int?, focusPosition: Double, completion:(_ didAdjust: Bool) -> Void) {
    completion(false)
  }
  
  var nextMessageId = 1
  func addTextMessage(text: String, userId: Int, username: String, date: Date, avatarImageName: String) {
    let uid = "\(nextMessageId)"
    self.nextMessageId += 1
    let isIncoming = !User.isCurrent(userId)
    let message = ChatItem(uid: uid, isIncoming: isIncoming, message: text, userId: userId, username: username, date: date, avatarImageName: avatarImageName)
    chatItems.append(message)
  }
}
