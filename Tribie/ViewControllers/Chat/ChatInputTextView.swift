//
//  ChatInputTextView.swift
//  Tribie
//
//  Created by Gilson Gil on 9/21/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol ChatInputTextViewDelegate: class {
  func isTextInputEmpty(empty: Bool)
}

final class ChatInputTextView: UITextView {
  internal let recieverName: String
  
  weak var chatDelegate: ChatInputTextViewDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(recieverName: String) {
    self.recieverName = recieverName
    super.init(frame: .zero, textContainer: nil)
    setUp()
    delegate = self
    attributedText = NSAttributedString.placeholder(string: String.chatInputPlaceholder(recieverName: recieverName))
  }
  
  private func setUp() {
    backgroundColor = UIColor.trbAlmostWhite
    textContainerInset = UIEdgeInsets(top: CGFloat(TribieConstants.textFieldVerticalMargin),
                                      left: CGFloat(TribieConstants.textFieldHorizontalMargin),
                                      bottom: CGFloat(TribieConstants.textFieldVerticalMargin),
                                      right: CGFloat(TribieConstants.textFieldHorizontalMargin))
  }
}

extension ChatInputTextView: UITextViewDelegate {
  func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
    if textView.text == String.chatInputPlaceholder(recieverName: recieverName) {
      textView.text = ""
    }
    textColor = UIColor.trbBrownishGrey
    return true
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    if textView.text == "" {
      textView.attributedText = NSAttributedString.placeholder(string: String.chatInputPlaceholder(recieverName: recieverName))
    }
  }
  
  func textViewDidChange(_ textView: UITextView) {
    superview?.invalidateIntrinsicContentSize()
    superview?.setNeedsLayout()
    superview?.layoutIfNeeded()
    let empty = textView.text == nil || textView.text.characters.count == 0 || textView.text == String.chatInputPlaceholder(recieverName: recieverName)
    chatDelegate?.isTextInputEmpty(empty: empty)
  }
}
