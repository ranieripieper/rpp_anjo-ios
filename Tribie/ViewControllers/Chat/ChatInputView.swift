//
//  ChatInputView.swift
//  Tribie
//  
//
//  Created by Gilson Gil on 9/21/16. 
//    Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

protocol ChatInputViewDelegate: class {
  func send(message: String)
}

final class ChatInputView: UIView {
  internal let sendButtonSize: CGFloat = 56
  internal let sendButtonAlpha: CGFloat = 0.7
  internal let maxHeight: CGFloat = 200
  
  internal let textView: ChatInputTextView
  internal let sendButton: RoundButton = {
    let button = RoundButton(type: .custom)
    button.setImage(UIImage.icnSend(), for: .normal)
    button.backgroundColor = UIColor.trbTiffanyBlue
    button.contentEdgeInsets = UIEdgeInsets(top: 6, left: 0, bottom: 0, right: 4)
    return button
  }()
  
  weak var delegate: ChatInputViewDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(recieverName: String) {
    self.textView = ChatInputTextView(recieverName: recieverName)
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.clear
    
    textView.chatDelegate = self
    addSubview(textView)
    
    sendButton.addTarget(self, action: #selector(sendTapped), for: .touchUpInside)
    isTextInputEmpty(empty: true)
    addSubview(sendButton)
    
    constrain(textView, sendButton) { textView, sendButton in
      sendButton.top == sendButton.superview!.top
      sendButton.right == sendButton.superview!.rightMargin
      sendButton.width == sendButtonSize
      sendButton.height == sendButtonSize
      
      textView.top == sendButton.centerY
      textView.left == textView.superview!.left
      textView.bottom == textView.superview!.bottom
      textView.right == textView.superview!.right
    }
  }
  
  override var intrinsicContentSize: CGSize {
    get {
      let text = textView.attributedText?.string ?? textView.text ?? ""
      let size = (text as NSString).boundingRect(with: CGSize(width: UIScreen.main.bounds.width - 2 * CGFloat(TribieConstants.textFieldHorizontalMargin), height: CGFloat(MAXFLOAT)), options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.avenirRoman(size: 15)], context: nil).size
      let width = size.width
      let height = min(size.height + sendButtonSize / 2 + 2 * CGFloat(TribieConstants.textFieldVerticalMargin), maxHeight)
      return CGSize(width: width, height: height)
    }
  }
}

// MARK: - Actions
extension ChatInputView {
  func sendTapped() {
    guard let text = textView.text, text.characters.count > 0 else {
      return
    }
    textView.text = nil
    isTextInputEmpty(empty: true)
    textView.resignFirstResponder()
    invalidateIntrinsicContentSize()
    setNeedsLayout()
    layoutIfNeeded()
    delegate?.send(message: text)
  }
}

// MARK: ChatInputTextView Delegate
extension ChatInputView: ChatInputTextViewDelegate {
  func isTextInputEmpty(empty: Bool) {
    sendButton.isEnabled = !empty
    sendButton.backgroundColor = sendButton.backgroundColor?.withAlphaComponent(empty ? sendButtonAlpha : 1)
  }
}
