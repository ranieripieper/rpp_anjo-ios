//
//  ChatItem.swift
//  Tribie
//
//  Created by Gilson Gil on 9/21/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import Chatto

final class ChatItem: ChatItemProtocol {
 var type: ChatItemType {
    if isIncoming {
      return ChatItem.incomingType
    } else {
      return ChatItem.sendingType
    }
  }
  
  let uid: String
  let isIncoming: Bool
  let message: String
  let userId: Int
  let username: String
  let date: Date
  let avatarImageName: String
  
  init(uid: String, isIncoming: Bool, message: String, userId: Int, username: String, date: Date, avatarImageName: String) {
    self.uid = uid
    self.isIncoming = isIncoming
    self.message = message
    self.userId = userId
    self.username = username
    self.date = date
    self.avatarImageName = avatarImageName
  }
}

extension ChatItem {
  static var incomingType: String {
    return "INCOMING"
  }
  static var sendingType: String {
    return "SENDING"
  }
}
