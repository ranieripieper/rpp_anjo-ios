//
//  ChatItemAvatar.swift
//  Tribie
//
//  Created by Gilson Gil on 9/22/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import Chatto

final class ChatItemAvatar: ChatItemProtocol {
  var type: ChatItemType {
    if isIncoming {
      return ChatItemAvatar.incomingType
    } else {
      return ChatItemAvatar.sendingType
    }
  }
  
  let uid: String
  let isIncoming: Bool
  let userId: Int
  let username: String
  let date: Date
  let avatarImageName: String
  
  init(uid: String, isIncoming: Bool, userId: Int, username: String, date: Date, avatarImageName: String) {
    self.uid = uid
    self.isIncoming = isIncoming
    self.userId = userId
    self.username = username
    self.date = date
    self.avatarImageName = avatarImageName
  }
}

extension ChatItemAvatar {
  static var incomingType: String {
    return "INCOMING-AVATAR"
  }
  static var sendingType: String {
    return "SENDING-AVATAR"
  }
}
