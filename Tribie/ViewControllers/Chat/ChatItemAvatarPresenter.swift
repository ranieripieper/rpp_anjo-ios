//
//  ChatItemAvatarPresenter.swift
//  Tribie
//
//  Created by Gilson Gil on 9/22/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import Chatto

final class ChatItemAvatarPresenter: ChatItemPresenterProtocol {
  private let chatItemAvatar: ChatItemAvatar
  
  init(chatItemAvatar: ChatItemAvatar) {
    self.chatItemAvatar = chatItemAvatar
  }
  
  static func registerCells(_ collectionView: UICollectionView) {
    collectionView.register(ChatAvatarCollectionViewCell.self, forCellWithReuseIdentifier: ChatAvatarCollectionViewCell.reuseIdentifier())
  }
  
  func heightForCell(maximumWidth width: CGFloat, decorationAttributes: ChatItemDecorationAttributesProtocol?) -> CGFloat {
    return ChatAvatarCollectionViewCell.height()
  }
  
  public func dequeueCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ChatAvatarCollectionViewCell.reuseIdentifier(), for: indexPath) as? ChatAvatarCollectionViewCell else {
      return UICollectionViewCell()
    }
    return cell
  }
  
  func configureCell(_ cell: UICollectionViewCell, decorationAttributes: ChatItemDecorationAttributesProtocol?) {
    (cell as? ChatAvatarCollectionViewCell)?.update(chatItemAvatar: chatItemAvatar)
  }
}
