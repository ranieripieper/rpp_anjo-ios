//
//  ChatItemAvatarPresenterBuilder.swift
//  Tribie
//
//  Created by Gilson Gil on 9/22/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import Chatto

final class ChatItemAvatarPresenterBuilder: ChatItemPresenterBuilderProtocol {
  var presenterType: ChatItemPresenterProtocol.Type
  
  init() {
    presenterType = ChatItemAvatarPresenter.self
  }
  
  func canHandleChatItem(_ chatItem: ChatItemProtocol) -> Bool {
    return chatItem is ChatItemAvatar
  }
  
  func createPresenterWithChatItem(_ chatItem: ChatItemProtocol) -> ChatItemPresenterProtocol {
    let chatItemPresenter = ChatItemAvatarPresenter(chatItemAvatar: chatItem as! ChatItemAvatar)
    return chatItemPresenter
  }
}
