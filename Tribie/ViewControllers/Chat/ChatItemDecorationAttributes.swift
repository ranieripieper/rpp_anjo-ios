//
//  ChatItemDecorationAttributes.swift
//  Tribie
//
//  Created by Gilson Gil on 9/22/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import Chatto

final class ChatItemDecorationAttributes: ChatItemDecorationAttributesProtocol {
  let bottomMargin: CGFloat
  
  init(bottomMargin: CGFloat) {
    self.bottomMargin = bottomMargin
  }
}
