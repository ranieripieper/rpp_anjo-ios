//
//  ChatItemDecorator.swift
//  Tribie
//
//  Created by Gilson Gil on 9/22/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import Chatto

final class ChatItemDecorator: ChatItemsDecoratorProtocol {
  static let sameSenderSeparation: CGFloat = 8
  static let messageAvatarSeparation: CGFloat = -1
  static let avatarMessageSeparation: CGFloat = 20
  
  func decorateItems(_ chatItems: [ChatItemProtocol]) -> [DecoratedChatItem] {
    var decoratedChatItems = [DecoratedChatItem]()
    
    for (index, chatItem) in chatItems.enumerated() {
      let next: ChatItemProtocol? = (index + 1 < chatItems.count) ? chatItems[index + 1] : nil
      
      var showsAvatar = false
      
      if let currentMessage = chatItem as? ChatItem {
        if let nextMessage = next as? ChatItem {
          showsAvatar = currentMessage.userId != nextMessage.userId
        } else {
          showsAvatar = true
        }
        
        let bottomMargin = separationAfterItem(current: currentMessage, next: next)
        let decorationAttributes = ChatItemDecorationAttributes(bottomMargin: bottomMargin)
        decoratedChatItems.append(DecoratedChatItem(chatItem: chatItem, decorationAttributes: decorationAttributes))
        
        if showsAvatar {
          let chatItemAvatar = ChatItemAvatar(uid: "\(currentMessage.uid)-avatar", isIncoming: currentMessage.isIncoming, userId: currentMessage.userId, username: currentMessage.username, date: currentMessage.date, avatarImageName: currentMessage.avatarImageName)
          let avatarDecorationAttributes = ChatItemDecorationAttributes(bottomMargin: ChatItemDecorator.avatarMessageSeparation)
          decoratedChatItems.append(DecoratedChatItem(chatItem: chatItemAvatar, decorationAttributes: avatarDecorationAttributes))
        }
      }
    }
    return decoratedChatItems
  }
  
  func separationAfterItem(current: ChatItemProtocol?, next: ChatItemProtocol?) -> CGFloat {
    guard let currentMessage = current as? ChatItem, let nextMessage = next as? ChatItem, currentMessage.userId == nextMessage.userId else {
      return ChatItemDecorator.messageAvatarSeparation
    }
    
    return ChatItemDecorator.sameSenderSeparation
  }
}
