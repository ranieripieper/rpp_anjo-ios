//
//  ChatItemPresenter.swift
//  Tribie
//
//  Created by Gilson Gil on 9/21/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import Chatto

final class ChatItemPresenter: ChatItemPresenterProtocol {
  private let chatItem: ChatItem
  
  init(chatItem: ChatItem) {
    self.chatItem = chatItem
  }
  
  static func registerCells(_ collectionView: UICollectionView) {
    collectionView.register(ChatCollectionViewCell.self, forCellWithReuseIdentifier: ChatCollectionViewCell.reuseIdentifier() + ChatItem.incomingType)
    collectionView.register(ChatCollectionViewCell.self, forCellWithReuseIdentifier: ChatCollectionViewCell.reuseIdentifier() + ChatItem.sendingType)
  }
  
  func heightForCell(maximumWidth width: CGFloat, decorationAttributes: ChatItemDecorationAttributesProtocol?) -> CGFloat {
    let text = chatItem.message
    let proposedHeight = (text as NSString).boundingRect(with: CGSize(width: width - 2 * CGFloat(TribieConstants.textFieldHorizontalMargin) - 2 * 8, height: CGFloat(MAXFLOAT)), options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: NSAttributedString.textViewAttributes(), context: nil).height
    return ceil(proposedHeight + 2 * CGFloat(TribieConstants.textFieldVerticalMargin)) + 1
  }
  
  func dequeueCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ChatCollectionViewCell.reuseIdentifier() + chatItem.type, for: indexPath as IndexPath) as? ChatCollectionViewCell else {
      return UICollectionViewCell()
    }
    return cell
  }
  
  func configureCell(_ cell: UICollectionViewCell, decorationAttributes: ChatItemDecorationAttributesProtocol?) {
    (cell as? ChatCollectionViewCell)?.update(chatItem: chatItem)
  }
}
