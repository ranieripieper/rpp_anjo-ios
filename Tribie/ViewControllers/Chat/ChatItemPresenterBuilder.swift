//
//  ChatItemPresenterBuilder.swift
//  Tribie
//
//  Created by Gilson Gil on 9/21/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import Chatto

final class ChatItemPresenterBuilder: ChatItemPresenterBuilderProtocol {
  var presenterType: ChatItemPresenterProtocol.Type
  
  init() {
    presenterType = ChatItemPresenter.self
  }
  
  func canHandleChatItem(_ chatItem: ChatItemProtocol) -> Bool {
    return chatItem is ChatItem
  }
  
  func createPresenterWithChatItem(_ chatItem: ChatItemProtocol) -> ChatItemPresenterProtocol {
    let chatItemPresenter = ChatItemPresenter(chatItem: chatItem as! ChatItem)
    return chatItemPresenter
  }
}
