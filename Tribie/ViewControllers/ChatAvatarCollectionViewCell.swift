//
//  ChatAvatarCollectionViewCell.swift
//  Tribie
//
//  Created by Gilson Gil on 9/22/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

final class ChatAvatarCollectionViewCell: UICollectionViewCell {
  internal struct Constant {
    static let margin: CGFloat = 10
    static let avatarSize: CGFloat = 46
    static let pointerMargin: CGFloat = 40
    static let pointerWidth: CGFloat = 40
    static let pointerHeight: CGFloat = 15
  }
  
  internal let avatarImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.layer.cornerRadius = Constant.avatarSize / 2
    imageView.contentMode = .scaleAspectFit
    return imageView
  }()
  
  internal let descriptionLabel: UILabel = {
    let label = UILabel()
    label.textColor = UIColor.trbPinkishGrey
    label.font = UIFont.avenirRoman(size: 14)
    return label
  }()
  
  internal var constraintGroup: ConstraintGroup?
  
  internal var pointerLayer: CAShapeLayer = {
    let path = UIBezierPath()
    path.move(to: CGPoint(x: 0, y: 0))
    path.addLine(to: CGPoint(x: Constant.pointerWidth, y: 0))
    path.addLine(to: CGPoint(x: Constant.pointerWidth / 2, y: Constant.pointerHeight))
    path.close()
    
    let layer = CAShapeLayer()
    layer.path = path.cgPath
    return layer
  }()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.clear
    layer.addSublayer(pointerLayer)
    
    addSubview(avatarImageView)
    
    addSubview(descriptionLabel)
    
    constrain(avatarImageView, descriptionLabel) { imageView, label in
      imageView.width == Constant.avatarSize
      imageView.height == Constant.avatarSize
      imageView.bottom == imageView.superview!.bottom - Constant.margin
      
      label.centerY == imageView.centerY
    }
  }
}

// MARK: - Public
extension ChatAvatarCollectionViewCell {
  static func height() -> CGFloat {
    return Constant.avatarSize + Constant.pointerHeight + 2 * Constant.margin
  }
  
  func update(chatItemAvatar: ChatItemAvatar) {
    if let constraintGroup = constraintGroup {
      constrain(clear: constraintGroup)
    }
    avatarImageView.image = UIImage(named: chatItemAvatar.avatarImageName)
    descriptionLabel.text = chatItemAvatar.username + " - " + TimestampDateFormatter.dateFormatter.string(from: chatItemAvatar.date) + " - " + TimestampDateFormatter.timeFormatter.string(from: chatItemAvatar.date)
    if chatItemAvatar.isIncoming {
      pointerLayer.frame = CGRect(x: Constant.pointerMargin, y: 0, width: Constant.pointerWidth, height: Constant.pointerHeight)
      pointerLayer.fillColor = UIColor.trbLightBlueGrey.cgColor
      constraintGroup = constrain(avatarImageView, descriptionLabel) { imageView, label in
        imageView.centerX == imageView.superview!.left + Constant.pointerMargin + Constant.pointerWidth / 2
        
        label.left == imageView.right + Constant.margin
        label.right <= label.superview!.right - Constant.margin
      }
    } else {
      pointerLayer.frame = CGRect(x: bounds.width - Constant.pointerMargin - Constant.pointerWidth, y: 0, width: Constant.pointerWidth, height: Constant.pointerHeight)
      pointerLayer.fillColor = UIColor.trbAlmostWhite.cgColor
      constraintGroup = constrain(avatarImageView, descriptionLabel) { imageView, label in
        imageView.centerX == imageView.superview!.right - Constant.pointerMargin - Constant.pointerWidth / 2
        
        label.left >= label.superview!.left + Constant.margin
        label.right == imageView.left - Constant.margin
      }
    }
  }
}

extension ChatAvatarCollectionViewCell {
  static func reuseIdentifier() -> String {
    return NSStringFromClass(self)
  }
}
