//
//  ChatCollectionViewCell.swift
//  Tribie
//
//  Created by Gilson Gil on 9/21/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

final class ChatCollectionViewCell: UICollectionViewCell {
  internal let textView: UITextView = {
    let textView = UITextView()
    textView.font = UIFont.avenirRoman(size: 15)
    textView.textColor = UIColor.trbBrownishGrey
    textView.layer.cornerRadius = 12
    textView.isEditable = false
    textView.isScrollEnabled = false
    textView.textAlignment = .left
    textView.textContainerInset = UIEdgeInsets(top: CGFloat(TribieConstants.textFieldVerticalMargin), left: CGFloat(TribieConstants.textFieldHorizontalMargin), bottom: CGFloat(TribieConstants.textFieldVerticalMargin), right: CGFloat(TribieConstants.textFieldHorizontalMargin))
    return textView
  }()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.clear
    
    addSubview(textView)
    constrain(textView) { textView in
      textView.left == textView.superview!.leftMargin
      textView.bottom == textView.superview!.bottom
      textView.right == textView.superview!.rightMargin
      textView.top == textView.superview!.top
    }
  }
}

// MARK: - Public
extension ChatCollectionViewCell {
  func update(chatItem: ChatItem) {
    if chatItem.isIncoming {
      textView.backgroundColor = UIColor.trbLightBlueGrey
    } else {
      textView.backgroundColor = UIColor.trbAlmostWhite
    }
    textView.text = chatItem.message
    layoutIfNeeded()
  }
}

extension ChatCollectionViewCell {
  static func reuseIdentifier() -> String {
    return NSStringFromClass(self)
  }
}
