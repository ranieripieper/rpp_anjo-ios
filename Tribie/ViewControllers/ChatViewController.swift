//
//  ChatViewController.swift
//  Tribie
//  
//
//  Created by Gilson Gil on 9/21/16. 
//    Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography
import Chatto

final class ChatViewController: BaseChatViewController {
  internal let bgMargin: CGFloat = 20
  internal let chatInputView: ChatInputView
  
  internal var chatViewModel: ChatViewModel
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(discomfort: Discomfort, ablyHelper: AblyHelper?) {
    chatViewModel = ChatViewModel(discomfort: discomfort, ablyHelper: ablyHelper)
    chatInputView = ChatInputView(recieverName: discomfort.otherNickname)
    super.init(nibName: nil, bundle: nil)
    chatInputView.delegate = self
    chatViewModel.dataSource.delegate = self
    chatDataSource = chatViewModel.dataSource
    chatItemsDecorator = chatViewModel.itemsDecorator
    setUp()
    addSettingsButton(action: #selector(menuTapped))
    defer {
      NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: Notification.Name(rawValue: TribieConstants.reloadChatViewNotificationKey), object: nil)
    }
  }
  
  private func setUp() {
    let bgImageView = newBgImageView()
    view.insertSubview(bgImageView, at: 0)
    
    let bgView = newBgView()
    view.insertSubview(bgView, aboveSubview: bgImageView)
    
    constrain(bgImageView, bgView) { bgImage, bg in
      bgImage.top == bgImage.superview!.top
      bgImage.left == bgImage.superview!.left
      bgImage.bottom == bgImage.superview!.bottom
      bgImage.right == bgImage.superview!.right
      
      bg.top == bg.superview!.top
      bg.left == bg.superview!.left + bgMargin
      bg.bottom == bg.superview!.bottom
      bg.right == bg.superview!.right - bgMargin
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.setHidesBackButton(true, animated: false)
  }
  
  override func createChatInputView() -> UIView {
    return chatInputView
  }
  
  override func createPresenterBuilders() -> [ChatItemType : [ChatItemPresenterBuilderProtocol]] {
    return [ChatItem.incomingType: [ChatItemPresenterBuilder()],
            ChatItem.sendingType: [ChatItemPresenterBuilder()],
            ChatItemAvatar.incomingType: [ChatItemAvatarPresenterBuilder()],
            ChatItemAvatar.sendingType: [ChatItemAvatarPresenterBuilder()]]
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: TribieConstants.reloadChatViewNotificationKey), object: nil)
  }
}

// MARK: - Views
private extension ChatViewController {
  func newBgImageView() -> UIImageView {
    let image = UIImage.bgChat()
    let imageView = UIImageView(image: image)
    return imageView
  }
  
  func newBgView() -> UIView {
    let view = UIView()
    view.backgroundColor = UIColor.white
    return view
  }
}

// MARK - Public
extension ChatViewController {
  func received(_ event: Event) {
    guard event.channel == chatViewModel.channel else {
      return
    }
    chatViewModel.handle([event])
  }
  
  func reloadData() {
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: TribieConstants.reloadChatViewNotificationKey), object: nil)
    guard let chatDataSource = chatDataSource else {
      return
    }
    DispatchQueue.main.async {
      chatDataSource.delegate?.chatDataSourceDidUpdate(chatDataSource, updateType: .reload)
    }
  }
}

// MARK: - ChatInputView Delegate
extension ChatViewController: ChatInputViewDelegate {
  func send(message: String) {
    chatViewModel.publish(message) { [weak self] inner in
      do {
        try inner()
      } catch {
        AlertView.present(error: error, inView: self?.view)
      }
    }
  }
}

// MARK: - MainNavigationControllerProtocol
extension ChatViewController: MainNavigationControllerProtocol {
  func menuTapped() {
    presentMenu(discomfort: chatViewModel.discomfort, completion: nil)
  }
}
