//
//  CreateProblemViewController.swift
//  Tribie
//  
//
//  Created by Gilson Gil on 9/15/16. 
//    Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

final class CreateProblemViewController: UIViewController {
  internal struct Constant {
    static let margin: CGFloat = 20
  }
  
  internal let titleTextField: CenteredGrayTextField = {
    let textField = CenteredGrayTextField()
    textField.autocapitalizationType = .sentences
    textField.placeholder = String.createProblemTitlePlaceholder()
    return textField
  }()
  internal let textView = CounterTextView(allowedCharacters: 300)
  internal var textFieldIsFirstResponder = false
  internal var createProblemViewModel = CreateProblemViewModel()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  init() {
    super.init(nibName: nil, bundle: nil)
    setUp()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    let keyboardHandler = KeyboardHandlerViewController(scrollView: nil)
    keyboardHandler.delegate = self
    addChildViewController(keyboardHandler)
    view.addSubview(keyboardHandler.view)
    keyboardHandler.view.frame = .zero
    keyboardHandler.didMove(toParentViewController: self)
    addSettingsButton(action: #selector(menuTapped))
  }
  
  private func setUp() {
    view.backgroundColor = UIColor.white
    
    let descriptionLabel = newDescriptionLabel()
    view.addSubview(descriptionLabel)
    
    let titleLabel = newTitleLabel()
    view.addSubview(titleLabel)
    
    titleTextField.delegate = self
    view.addSubview(titleTextField)
    
    view.addSubview(textView)
    
    let searchButton = newSearchButton()
    view.addSubview(searchButton)
    
    let margin = Constant.margin
    constrain(descriptionLabel, titleLabel, titleTextField, textView, searchButton) { description, title, textField, textView, button in
      description.top == topLayoutGuideCartography + margin
      description.left == description.superview!.left + margin
      description.right == description.superview!.right - margin
      
      title.top == description.bottom + margin
      title.left == title.superview!.left + margin
      title.right == title.superview!.right - margin
      
      textField.top == title.bottom + margin
      textField.left == textField.superview!.left + margin
      textField.right == textField.superview!.right - margin
      
      textView.top == textField.bottom + margin
      textView.left == textView.superview!.left + margin
      textView.right == textView.superview!.right - margin
      
      button.top == textView.bottom + margin
      button.left == button.superview!.left + margin
      button.bottom == button.superview!.bottom - margin
      button.right == button.superview!.right - margin
    }
  }
}

// MARK: - Views
internal extension CreateProblemViewController {
  func newDescriptionLabel() -> UILabel {
    let label = UILabel()
    label.numberOfLines = 0
    label.textAlignment = .center
    label.textColor = UIColor.black
    label.font = UIFont.champagne(size: 20)
    label.text = String.createProblemDescription()
    return label
  }
  
  func newTitleLabel() -> UILabel {
    let label = UILabel()
    label.textAlignment = .center
    label.textColor = UIColor.black
    label.adjustsFontSizeToFitWidth = true
    label.minimumScaleFactor = 0.6
    label.font = UIFont.champagneBold(size: 36)
    label.text = String.createProblemTitle()
    return label
  }
  
  func newSearchButton() -> RoundedCornerButton {
    let button = RoundedCornerButton()
    button.backgroundColor = UIColor.trbTiffanyBlue
    button.setTitle(String.createProblemSearchButton(), for: .normal)
    button.addTarget(self, action: #selector(searchTapped), for: .touchUpInside)
    return button
  }
}

// MARK: - Actions
extension CreateProblemViewController {
  func searchTapped() {
    SVProgressHUDHelper.show()
    createProblemViewModel.create(titleTextField.text, description: textView.text) { [weak self] inner in
      SVProgressHUDHelper.dismiss()
      do {
        let _ = try inner()
        (self?.navigationController as? MainNavigationController)?.refresh()
      } catch {
        AlertView.present(error: error, inView: self?.navigationController?.view ?? self?.view)
      }
    }
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesBegan(touches, with: event)
    view.endEditing(true)
  }
}

extension CreateProblemViewController: UITextFieldDelegate {
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    keyboardDidDisapper()
    textFieldIsFirstResponder = true
    return true
  }
  
  func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    textFieldIsFirstResponder = false
    return true
  }
}

// MARK: - KeyboardHandler Delegate
extension CreateProblemViewController: KeyboardHandlerViewControllerDelegate {
  func keyboardDidAppear(height: CGFloat) {
    guard !textFieldIsFirstResponder else {
      return
    }
    let bottomSpacing = view.bounds.height - textView.frame.origin.y - textView.bounds.height
    guard bottomSpacing < height else {
      return
    }
    view.transform = CGAffineTransform.identity.translatedBy(x: 0, y: bottomSpacing - height)
  }
  
  func keyboardDidDisapper() {
    guard !textFieldIsFirstResponder else {
      return
    }
    UIView.animate(withDuration: TribieConstants.defaultAnimationDuration, delay: TribieConstants.defaultAnimationDelay, options: .curveEaseInOut, animations: { [weak self] in
      self?.view.transform = CGAffineTransform.identity
      }, completion: nil)
  }
}

// MARK: - MainNavigationController Protocol
extension CreateProblemViewController: MainNavigationControllerProtocol {
  func menuTapped() {
    presentMenu(discomfort: nil, completion: nil)
  }
}
