//
//  LoginContainer.swift
//  Tribie
//
//  Created by Gilson Gil on 9/19/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

protocol LoginContainerProtocol: class {
  var loginContainer: LoginContainer? { get set }
  
  func push(viewController: UIViewController)
  func popViewController()
}

extension LoginContainerProtocol {
  func push(viewController: UIViewController) {
    loginContainer?.pushViewController(viewController: viewController)
  }
  
  func popViewController() {
    loginContainer?.popViewController()
  }
}

final class LoginContainer: UIViewController {
  internal static let margin: CGFloat = 20
  
  internal let containerView = UIView()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  init() {
    super.init(nibName: nil, bundle: nil)
    setUp()
    view.layoutIfNeeded()
    loadSignup()
  }
  
  private func setUp() {
    let bgImageView = newBgImageView()
    view.addSubview(bgImageView)
    
    containerView.backgroundColor = UIColor.clear
    view.addSubview(containerView)
    
    let margin = LoginContainer.margin
    constrain(bgImageView, containerView) { bg, containerView in
      bg.top == bg.superview!.top
      bg.left == bg.superview!.left
      bg.bottom == bg.superview!.bottom
      bg.right == bg.superview!.right
      
      containerView.top == containerView.superview!.top + margin
      containerView.left == containerView.superview!.left
      containerView.bottom == containerView.superview!.bottom - margin
      containerView.right == containerView.superview!.right
    }
  }
  
  override var prefersStatusBarHidden: Bool {
    get {
      return true
    }
  }
}

// MARK: - Views
private extension LoginContainer {
  func newBgImageView() -> UIImageView {
    let image = UIImage.bgWelcome()
    let imageView = UIImageView(image: image)
    return imageView
  }
}

// MARK: - Public
extension LoginContainer {
  func loadSignup() {
    let signupViewController = SignupViewController()
    present(viewController: signupViewController)
  }
}

// MARK: Private
private extension LoginContainer {
  func present(viewController: UIViewController) {
    if let vc = viewController as? LoginContainerProtocol {
      vc.loginContainer = self
    }
    viewController.willMove(toParentViewController: self)
    addChildViewController(viewController)
    let margin = LoginContainer.margin
    let frame = CGRect(x: margin, y: 0, width: containerView.bounds.width - 2 * margin, height: containerView.bounds.height)
    viewController.view.frame = frame
    containerView.addSubview(viewController.view)
    viewController.didMove(toParentViewController: self)
  }
  
  func pushViewController(viewController: UIViewController) {
    guard let topViewController = childViewControllers.last else {
      return
    }
    if let vc = viewController as? LoginContainerProtocol {
      vc.loginContainer = self
    }
    viewController.willMove(toParentViewController: self)
    addChildViewController(viewController)
    let margin = LoginContainer.margin
    let width = containerView.bounds.width
    let height = containerView.bounds.height
    viewController.view.frame = CGRect(x: margin, y: 0, width: width - 2 * margin, height: height)
    containerView.addSubview(viewController.view)
    viewController.didMove(toParentViewController: self)
    viewController.view.transform = CGAffineTransform.identity.translatedBy(x: width - margin, y: 0)
    UIView.animate(withDuration: TribieConstants.defaultAnimationDuration, delay: TribieConstants.defaultAnimationDelay, options: .curveEaseInOut, animations: {
      topViewController.view.transform = CGAffineTransform.identity.translatedBy(x: -width, y: 0)
      viewController.view.transform = CGAffineTransform.identity
      }, completion: { _ in
        topViewController.view.removeFromSuperview()
    })
  }
  
  func popViewController() {
    let topViewControllers = childViewControllers.suffix(2).flatMap { $0 }
    guard topViewControllers.count == 2, let topViewController = topViewControllers.last, let bottomViewController = topViewControllers.first else {
      return
    }
    let width = containerView.bounds.width
    containerView.insertSubview(bottomViewController.view, belowSubview: topViewController.view)
    UIView.animate(withDuration: TribieConstants.defaultAnimationDuration, delay: TribieConstants.defaultAnimationDelay, options: .curveEaseInOut, animations: {
      topViewController.view.transform = CGAffineTransform.identity.translatedBy(x: width, y: 0)
      bottomViewController.view.transform = CGAffineTransform.identity
      }, completion: { _ in
        topViewController.view.removeFromSuperview()
        topViewController.removeFromParentViewController()
    })
  }
}
