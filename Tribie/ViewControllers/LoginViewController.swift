//
//  LoginViewController.swift
//  Tribie
//
//  Created by Gilson Gil on 9/14/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

final class LoginViewController: UIViewController {
  fileprivate struct Constant {
    static let hMargin: CGFloat = 20
    static let vMargin: CGFloat = 12
    static let topMargin: CGFloat = 130
  }
  
  var loginContainer: LoginContainer?
  internal let emailTextField: CenteredGrayTextField = {
    let textField = CenteredGrayTextField()
    textField.autocapitalizationType = .none
    textField.autocorrectionType = .no
    textField.keyboardType = .emailAddress
    textField.placeholder = String.signupEmailPlaceholder()
    return textField
  }()
  internal let passwordTextField: CenteredGrayTextField = {
    let textField = CenteredGrayTextField()
    textField.isSecureTextEntry = true
    textField.placeholder = String.signupPasswordPlaceholder()
    return textField
  }()
  
  internal var loginViewModel = LoginViewModel()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  init() {
    super.init(nibName: nil, bundle: nil)
    setUp()
  }
  
  private func setUp() {
    view.backgroundColor = UIColor.white
    
    let titleLabel = newTitleLabel()
    view.addSubview(titleLabel)
    
    emailTextField.delegate = self
    view.addSubview(emailTextField)
    
    passwordTextField.delegate = self
    view.addSubview(passwordTextField)
    
    let loginButton = newLoginButton()
    view.addSubview(loginButton)
    
    let forgotPasswordButton = newForgotPasswordButton()
    view.addSubview(forgotPasswordButton)
    
    let scale = UIScreen.main.bounds.height / 667
    let hMargin = Constant.hMargin * scale
    let vMargin = Constant.vMargin * scale
    let topMargin = Constant.topMargin * scale
    
    constrain([titleLabel, emailTextField, passwordTextField, loginButton, forgotPasswordButton]) { views in
      views.forEach {
        $0.left == $0.superview!.left + hMargin ~ 900
        $0.right == $0.superview!.right - hMargin ~ 900
        $0.centerX == $0.superview!.centerX
      }
      views[0].top == views[0].superview!.top + topMargin
      views[1].top == views[0].bottom + 4 * vMargin
      views[2].top == views[1].bottom + vMargin
      views[3].top == views[2].bottom + 2 * hMargin
      views[4].top == views[3].bottom + 2 * hMargin
    }
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesBegan(touches, with: event)
    view.endEditing(true)
  }
}

// MARK: - Views
private extension LoginViewController {
  func newTitleLabel() -> UILabel {
    let label = UILabel()
    label.textAlignment = .center
    label.textColor = UIColor.black
    label.text = String.loginTitle()
    label.font = UIFont.champagneBold(size: 36)
    return label
  }
  
  func newLoginButton() -> RoundedCornerButton {
    let button = RoundedCornerButton()
    button.backgroundColor = UIColor.trbTiffanyBlue
    button.setTitle(String.loginLoginButton(), for: .normal)
    button.addTarget(self, action: #selector(loginTapped), for: .touchUpInside)
    return button
  }
  
  func newForgotPasswordButton() -> MultipleLineButton {
    let button = MultipleLineButton()
    button.titleLabel?.font = UIFont.avenirRoman(size: 16)
    button.highlightColor = UIColor.trbTiffanyBlue
    button.highlightString = String.loginForgotPasswordButtonHighlight()
    button.setTitle(String.loginForgotPasswordButton(), for: .normal)
    button.addTarget(self, action: #selector(forgotPasswordTapped), for: .touchUpInside)
    return button
  }
}

// MARK: - Actions
extension LoginViewController {
  func loginTapped() {
    login()
    view.endEditing(true)
  }
  
  func forgotPasswordTapped() {
    forgotPassword()
    view.endEditing(true)
  }
}

// MARK: - Private
private extension LoginViewController {
  func login() {
    SVProgressHUDHelper.show()
    loginViewModel.login(email: emailTextField.text, password: passwordTextField.text) { [weak self] inner in
      SVProgressHUDHelper.dismiss()
      do {
        try inner()
        (UIApplication.shared.delegate as? AppDelegate)?.loadApplication(fromSignup: false)
      } catch let error as LoginError {
        switch error {
        case .missingField:
          self?.emailTextField.validate()
          self?.passwordTextField.validate()
        case .wrongCredentials:
          self?.presentAlert(any: error)
        }
      } catch {
        self?.presentAlert(any: error)
      }
    }
  }
  
  func forgotPassword() {
    SVProgressHUDHelper.show()
    loginViewModel.forgotPassword(email: emailTextField.text) { [weak self] inner in
      SVProgressHUDHelper.dismiss()
      do {
        try inner()
        self?.presentAlert(any: String.loginForgotPasswordSuccessMessage)
      } catch let error as LoginError {
        switch error {
        case .missingField:
          self?.emailTextField.validate()
        default:
          break
        }
      } catch {
        self?.presentAlert(any: error)
      }
    }
  }
  
  func presentAlert(any: Any) {
    let view = loginContainer?.view ?? self.view
    if let message = any as? String {
      AlertView.present(message: message, inView: view, bottomSpacing: 20)
    } else if let error = any as? Error {
      AlertView.present(error: error, inView: view, bottomSpacing: 20)
    }
  }
}

// MARK: UITextField Delegate
extension LoginViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    switch textField {
    case emailTextField:
      passwordTextField.becomeFirstResponder()
    case passwordTextField:
      passwordTextField.resignFirstResponder()
    default:
      break
    }
    return true
  }
}

// MARK: - NavigationControllerProtocol
extension LoginViewController: LoginContainerProtocol {}
