//
//  MainNavigationBar.swift
//  Tribie
//
//  Created by Gilson Gil on 24/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class MainNavigationBar: UINavigationBar {
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  private func setUp() {
    barTintColor = UIColor.trbBrownishGrey
    tintColor = UIColor.white
  }
}
