//
//  MainNavigationController.swift
//  Tribie
//
//  Created by Gilson Gil on 24/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Ably

protocol MainNavigationControllerProtocol: class {
  var navigationController: UINavigationController? { get }
  var navigationItem: UINavigationItem { get }
  
  func push(viewController: UIViewController)
  func addSettingsButton(action: Selector)
  func presentMenu(discomfort: Discomfort?, completion: (() -> ())?)
  func menuTapped()
  func presentWaitingResponse()
}

extension MainNavigationControllerProtocol {
  func push(viewController: UIViewController) {
    navigationController?.pushViewController(viewController, animated: true)
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
  }
  
  func addSettingsButton(action: Selector) {
    var items = navigationItem.rightBarButtonItems ?? []
    items = items.filter { $0.tag != 1 }
    let settingsButton = UIBarButtonItem(image: UIImage.icnMenu(), style: .plain, target: self, action: action)
    settingsButton.tag = 1
    items.insert(settingsButton, at: 0)
    navigationItem.rightBarButtonItems = items
  }
  
  func presentMenu(discomfort: Discomfort?, completion: (() -> ())?) {
    (navigationController as? MainNavigationController)?.toggleMenu(discomfort: discomfort, completion: completion)
  }
  
  func presentWaitingResponse() {
    let mainNavigationController = navigationController as? MainNavigationController
    mainNavigationController?.viewControllers = [WaitingMatchViewController(reported: false)]
  }
}

final class MainNavigationController: UINavigationController {
  fileprivate var dimView: UIView?
  fileprivate var chatBarButtonItem: ChatBarButtonItem?
  
  fileprivate var otherViewController: MainNavigationControllerProtocol?
  var paginatedResults = [ARTPaginatedResult<ARTMessage>]()
  
  internal var mainNavigationViewModel: MainNavigationViewModel? {
    willSet(newViewModel) {
      guard let newViewModel = newViewModel else {
        return
      }
      switch newViewModel.status {
      case .inChat(let discomforts, let selectedIndex, let showNotification):
        chatBarButtonItem?.showNotification(showNotification)
        addOtherChatButton(discomforts: discomforts, selectedIndex: selectedIndex)
      default:
        break
      }
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  init(fromSignup: Bool) {
    super.init(navigationBarClass: MainNavigationBar.self, toolbarClass: nil)
    guard !fromSignup else {
      viewControllers = [WelcomeViewController()]
      return
    }
    let viewController = UIViewController()
    viewController.view?.backgroundColor = .white
    viewControllers = [viewController]
    SVProgressHUDHelper.show()
    refresh()
    NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: NSNotification.Name(rawValue: TribieConstants.reloadMainFromBackgroundNotificationKey), object: nil)
  }
  
  fileprivate func setUp() {
    switch self.mainNavigationViewModel!.status {
    case .fromSignup:
      viewControllers = [WelcomeViewController()]
    case .noDiscomfort:
      viewControllers = [CreateProblemViewController()]
    case .inChat(let discomforts, _, _):
      discomforts.forEach {
        guard let channel = $0.channel else {
          return
        }
        if $0.isSubscribable() {
          mainNavigationViewModel?.subscribe(to: channel)
        }
      }
      let viewController: UIViewController
      if let discomfort = discomforts.first {
        if discomfort.isChatable() {
          viewController = ChatViewController(discomfort: discomfort, ablyHelper: mainNavigationViewModel?.ablyHelper)
        } else if User.isCurrent(discomfort.userId) && (discomfort.status == .waitingResponse || discomfort.status == .created) {
          viewController = WaitingMatchViewController(reported: false)
        } else if User.isCurrent(discomfort.userId) && discomfort.status == .tribierRejected {
          viewController = WaitingMatchViewController(reported: true)
        } else {
          viewController = CreateProblemViewController()
        }
      } else {
        viewController = CreateProblemViewController()
      }
      self.viewControllers = [viewController]
      if discomforts.count == 2 {
        if let secondDiscomfort = discomforts.last, User.isCurrent(secondDiscomfort.tribierId) && (secondDiscomfort.status == .waitingResponse || secondDiscomfort.status == .created) {
          
        } else {
          addOtherChatButton(discomforts: discomforts, selectedIndex: 0)
        }
      } else if let first = discomforts.first, first.isChatable() && User.isCurrent(first.tribierId) {
        let addButton = AddButton(target: self, action: #selector(goToCreateDiscomfort))
        viewController.navigationItem.leftBarButtonItems = [UIBarButtonItem(customView: addButton)]
      }
      let waiting = discomforts.filter { $0.status == .waitingResponse && User.isCurrent($0.tribierId) }.first
      if let waitingDiscomfort = waiting {
        defer {
          DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) { [weak self] in
            self?.view.endEditing(true)
            self?.presentUserHelpInvite(discomfort: waitingDiscomfort)
          }
        }
      } else {
        let accomplished = discomforts.filter { $0.status == .missionAccomplished }.first
        if let missionAccomplished = accomplished {
          guard User.isCurrent(missionAccomplished.tribierId) else {
            return
          }
          Event.closeEvent(missionAccomplished) { success in
            guard let success = success, success else {
              return
            }
            defer {
              DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) { [weak self] in
                self?.view.endEditing(true)
                self?.presentUserAccomplished(name: missionAccomplished.otherNickname)
              }
            }
          }
        } else {
          let left = discomforts.filter { $0.status == .tribierDeleteAccount || $0.status == .tribierDisappeared || $0.status == .tribierLogout || $0.status == .tribierTalkingNonsense || $0.status == .userDeleteAccount || $0.status == .userDisappeared || $0.status == .userLogout || $0.status == .userTalkingNonsense }.first
          if let left = left {
            let otherLeft = (
              User.isCurrent(left.userId) &&
              (left.status == .tribierDeleteAccount || left.status == .tribierLogout)
              ||
              User.isCurrent(left.tribierId) &&
              (left.status == .userDeleteAccount || left.status == .userLogout)
            )
            guard otherLeft else {
              return
            }
            Event.closeEvent(left) { success in
              guard let success = success, success else {
                return
              }
              defer {
                if left.otherNickname.characters.count > 0 {
                  DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) { [weak self] in
                    self?.view.endEditing(true)
                    self?.presentUserGone(name: left.otherNickname)
                  }
                } else {
                  RealmHelper.otherUser(in: left.channel) { name in
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) { [weak self] in
                      self?.view.endEditing(true)
                      self?.presentUserGone(name: name)
                    }
                  }
                }
              }
            }
//          } else {
//            let reported = discomforts.filter { $0.status == .tribierRejected }.first
//            if let reported = reported {
//              
//            }
          }
        }
      }
    }
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    get {
      return .lightContent
    }
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: TribieConstants.reloadMainFromBackgroundNotificationKey), object: nil)
  }
}

// MARK: - Public
extension MainNavigationController {
  func toggleMenu(discomfort: Discomfort?, completion: (() -> ())?) {
    topViewController?.view.endEditing(true)
    guard let menuViewController = topViewController as? MenuViewController else {
      guard completion == nil else {
        completion?()
        return
      }
      presentMenu(discomfort: discomfort)
      return
    }
    dismissMenu(menuViewController, completion: completion)
  }
  
  func presentUserHelpInvite(discomfort: Discomfort) {
    guard !(topViewController is AlertController) && !(topViewController is NewConnectionPopupViewController) else {
      return
    }
    let type = PopupType.askTribier(discomfort: discomfort)
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else {
        return
      }
      weakSelf.topViewController?.view.endEditing(true)
      weakSelf.toggleMenu(discomfort: nil) {
        DispatchQueue.main.async {
          let viewController = NewConnectionPopupViewController.present(type: type, in: weakSelf)
          viewController?.delegate = weakSelf
        }
      }
    }
  }
  
  func presentUserHelp(discomfort: Discomfort) {
    guard !(topViewController is AlertController) && !(topViewController is NewConnectionPopupViewController) else {
      return
    }
    let type = PopupType.newTribier(discomfort: discomfort)
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else {
        return
      }
      weakSelf.topViewController?.view.endEditing(true)
      weakSelf.toggleMenu(discomfort: nil) {
        let viewController = NewConnectionPopupViewController.present(type: type, in: weakSelf)
        viewController?.delegate = weakSelf
      }
    }
  }
  
  func presentUserAccomplished(name: String) {
    guard !(topViewController is AlertController) && !(topViewController is NewConnectionPopupViewController) else {
      return
    }
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else {
        return
      }
      weakSelf.topViewController?.view.endEditing(true)
      weakSelf.toggleMenu(discomfort: nil) {
        let alert = AlertController.present(String.alertAccomplishedTitle(), message: String.alertAccomplishedSubtitle(name: name), style: .alert, in: weakSelf)
        alert.delegate = weakSelf
      }
    }
  }
  
  func presentUserGone(name: String) {
    guard !(topViewController is AlertController) && !(topViewController is NewConnectionPopupViewController) else {
      return
    }
    DispatchQueue.main.async { [weak self] in
      guard let weakSelf = self else {
        return
      }
      weakSelf.topViewController?.view.endEditing(true)
      weakSelf.toggleMenu(discomfort: nil) {
        let alert = AlertController.present(String.alertGoneTitle(name: name), message: String.alertGoneSubtitle(name: name), style: .alert, in: weakSelf)
        alert.delegate = weakSelf
      }
    }
  }
  
  func received(_ event: Event) {
    viewControllers.forEach {
      guard let chatViewController = $0 as? ChatViewController else {
        return
      }
      chatViewController.received(event)
    }
    guard let showNotification = mainNavigationViewModel?.received(event) else {
      return
    }
    chatBarButtonItem?.showNotification(showNotification)
  }
  
  func addOtherChatButton(discomforts: [Discomfort], selectedIndex: Int) {
    guard discomforts.count > 1 else {
      return
    }
    let otherIndex = selectedIndex == 0 ? 1 : 0
    let otherDiscomfort = discomforts[otherIndex]
    guard otherDiscomfort.status.isChatable() else {
      return
    }
    let chatButton = ChatBarButtonItem(image: UIImage(named: otherDiscomfort.otherImageId), target: self, action: #selector(goToOtherChat))
    let item = UIBarButtonItem(customView: chatButton)
    item.tag = 2
    var items = topViewController?.navigationItem.rightBarButtonItems ?? []
    items = items.filter { $0.tag != 2 }
    items.append(item)
    chatBarButtonItem = chatButton
    DispatchQueue.main.async { [weak self] in
      self?.topViewController?.navigationItem.rightBarButtonItems = items
    }
  }

  func refresh() {
    MainNavigationViewModel.viewModel { [weak self] status in
      SVProgressHUDHelper.dismiss()
      guard let weakSelf = self else {
        return
      }
      DispatchQueue.main.async {
        weakSelf.mainNavigationViewModel?.unsubscribe()
        weakSelf.mainNavigationViewModel = MainNavigationViewModel(status: status, mainNavigationController: weakSelf)
        weakSelf.setUp()
      }
    }
  }
}

// MARK: - Actions
extension MainNavigationController {
  func goToCreateDiscomfort() {
    let createDiscomfortViewController = viewControllers.filter { $0 is CreateProblemViewController }
      .first ?? CreateProblemViewController()
    otherViewController = topViewController as? MainNavigationControllerProtocol
    otherViewController?.push(viewController: createDiscomfortViewController)
  }
  
  func goToOtherChat() {
    guard let discomfort = mainNavigationViewModel?.changeDiscomfort() else {
      return
    }
    let other = otherViewController
    otherViewController = topViewController as? MainNavigationControllerProtocol
    let viewController: UIViewController
    if let other = other as? UIViewController {
      viewController = other
    } else if discomfort.isChatable() {
      viewController = ChatViewController(discomfort: discomfort, ablyHelper: mainNavigationViewModel?.ablyHelper)
    } else if discomfort.status == .waitingResponse || discomfort.status == .created {
      viewController = WaitingMatchViewController(reported: false)
    } else {
      viewController = WaitingMatchViewController(reported: true)
    }
    otherViewController?.push(viewController: viewController)
    let discomforts: [Discomfort]
    let selectedIndex: Int
    switch mainNavigationViewModel!.status {
    case .inChat(let aDiscomforts, let aSelectedIndex, _):
      discomforts = aDiscomforts
      selectedIndex = aSelectedIndex
    default:
      return
    }
    viewControllers = [viewController]
    addOtherChatButton(discomforts: discomforts, selectedIndex: selectedIndex)
  }
}

// MARK: - Private
fileprivate extension MainNavigationController {
  func presentMenu(discomfort: Discomfort?) {
    let dimView = UIView()
    dimView.backgroundColor = .black
    dimView.alpha = 0
    dimView.frame = view?.bounds ?? .zero
    view?.insertSubview(dimView, belowSubview: navigationBar)
    self.dimView = dimView
    
    let menuViewController = MenuViewController(discomfort: discomfort)
    menuViewController.delegate = self
    menuViewController.willMove(toParentViewController: self)
    menuViewController.view?.frame = view?.bounds ?? .zero
    view?.insertSubview(menuViewController.view, belowSubview: navigationBar)
    addChildViewController(menuViewController)
    menuViewController.didMove(toParentViewController: self)
    
    menuViewController.view.transform = CGAffineTransform.identity.translatedBy(x: 0, y: -menuViewController.view.bounds.height)
    
    UIView.animate(withDuration: TribieConstants.defaultAnimationDuration, delay: TribieConstants.defaultAnimationDelay, options: .curveEaseInOut, animations: {
      dimView.alpha = 0.8
      menuViewController.view.transform = CGAffineTransform.identity
      }, completion: nil)
  }
  
  func dismissMenu(_ menuViewController: MenuViewController, completion: (() -> ())?) {
    UIView.animate(withDuration: TribieConstants.defaultAnimationDuration, delay: TribieConstants.defaultAnimationDelay, options: .curveEaseInOut, animations: { [weak self] in
      self?.dimView?.alpha = 0
      menuViewController.view.transform = CGAffineTransform.identity.translatedBy(x: 0, y: -menuViewController.view.bounds.height)
      }, completion: { [weak self] _ in
        self?.dimView?.removeFromSuperview()
        self?.dimView = nil
        menuViewController.removeFromParentViewController()
        menuViewController.view.removeFromSuperview()
        completion?()
      })
  }
}

// MARK: - MenuViewController Delegate
extension MainNavigationController: MenuViewControllerDelegate {
  func goToSettings() {
    toggleMenu(discomfort: nil) { [weak self] in
      let settingsViewController = SettingsViewController()
      self?.viewControllers.flatMap { $0 as? MainNavigationControllerProtocol }
        .last?.push(viewController: settingsViewController)
    }
  }
  
  func reloadMainViewController() {
    toggleMenu(discomfort: nil) { [weak self] in
      self?.refresh()
    }
  }
}

// MARK: - NewConnectionViewController Delegate
extension MainNavigationController: NewConnectionPopupViewControllerDelegate {
  func didStartNewChat() {
    refresh()
  }
}

// MARK: - AlertController Delegate
extension MainNavigationController: AlertControllerDelegate {
  func didDismiss() {
    refresh()
  }
  
  func deleteAccount() {
    
  }
}
