//
//  MenuCell.swift
//  Tribie
//
//  Created by Gil on 30/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

final class MenuCell: UITableViewCell {
  fileprivate struct Constant {
    static let margin: CGFloat = 20
    static let smallMargin: CGFloat = 12
  }
  
  fileprivate var menuCellViewModel: MenuCellViewModel?
  
  fileprivate let bg: UIView = {
    let view = UIView()
    view.backgroundColor = .white
    return view
  }()
  
  fileprivate let iconImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.contentMode = .scaleAspectFit
    return imageView
  }()
  
  fileprivate let subBg: UIView = {
    let view = UIView()
    return view
  }()
  
  fileprivate let label: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.textColor = .white
    label.font = UIFont.avenirHeavy(size: 16)
    label.adjustsFontSizeToFitWidth = true
    label.minimumScaleFactor = 0.7
    return label
  }()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = .clear
    selectionStyle = .none
    contentView.backgroundColor = .clear
    contentView.addSubview(bg)
    contentView.addSubview(subBg)
    contentView.addSubview(iconImageView)
    contentView.addSubview(label)
    
    constrain(bg, subBg, iconImageView, label) { bg, subBg, icon, label in
      bg.top == bg.superview!.top
      bg.left == icon.right * 0.4
      bg.bottom == bg.superview!.bottom
      bg.right == bg.superview!.right - Constant.margin
      
      subBg.top == bg.top + Constant.smallMargin
      subBg.left == bg.left + Constant.smallMargin
      subBg.bottom == bg.bottom - Constant.smallMargin
      subBg.right == bg.right - Constant.smallMargin
      
      icon.centerY == icon.superview!.centerY
      icon.left == icon.superview!.left + Constant.smallMargin
      icon.width == icon.height
      
      label.top == subBg.top
      label.left == icon.right + Constant.smallMargin
      label.bottom == subBg.bottom
      label.right == subBg.right - Constant.smallMargin
    }
  }
  
  override func setHighlighted(_ highlighted: Bool, animated: Bool) {
    super.setHighlighted(highlighted, animated: animated)
    guard let menuCellViewModel = menuCellViewModel else {
      return
    }
    if highlighted {
      subBg.backgroundColor = UIColor.trbTiffanyBlue
    } else {
      subBg.backgroundColor = menuCellViewModel.highlightedBackground ? UIColor.trbWheat : UIColor.trbBlush
    }
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    guard let menuCellViewModel = menuCellViewModel else {
      return
    }
    if selected {
      subBg.backgroundColor = UIColor.trbTiffanyBlue
      label.text = menuCellViewModel.selectedText
    } else {
      subBg.backgroundColor = menuCellViewModel.highlightedBackground ? UIColor.trbWheat : UIColor.trbBlush
      label.text = menuCellViewModel.text
    }
  }
}

// MARK: - Public
extension MenuCell {
  static func height() -> CGFloat {
    return 100
  }
  
  func update(menuCellViewModel: MenuCellViewModel) {
    self.menuCellViewModel = menuCellViewModel
    iconImageView.image = UIImage(named: menuCellViewModel.iconName)
    label.text = menuCellViewModel.text
    subBg.backgroundColor = menuCellViewModel.highlightedBackground ? UIColor.trbWheat : UIColor.trbBlush
  }
}
