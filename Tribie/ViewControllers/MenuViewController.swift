//
//  MenuViewController.swift
//  Tribie
//
//  Created by Gil on 30/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

enum MenuType {
  case settings, user, tribier
}

protocol MenuViewControllerDelegate: class {
  func goToSettings()
  func reloadMainViewController()
}

final class MenuViewController: UIViewController {
  fileprivate var menuViewModel: MenuViewModel
  
  weak var delegate: MenuViewControllerDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(discomfort: Discomfort?) {
    self.menuViewModel = MenuViewModel(discomfort: discomfort)
    super.init(nibName: nil, bundle: nil)
    automaticallyAdjustsScrollViewInsets = false
    setUp()
  }
  
  private func setUp() {
    view.backgroundColor = .clear
    
    let tableView = createTableView()
    view.addSubview(tableView)
    
    constrain(tableView) { table in
      table.left == table.superview!.left
      table.right == table.superview!.right
      table.centerY == table.superview!.centerY
      table.height == MenuCell.height() * CGFloat(menuViewModel.items.count)
    }
  }
}

// MARK: - Private
fileprivate extension MenuViewController {
  func delete(indexPath: IndexPath, from tableView: UITableView, completion: @escaping () -> ()) {
    guard let cell = tableView.cellForRow(at: indexPath) else {
      return
    }
    UIView.animate(withDuration: TribieConstants.defaultAnimationDuration, delay: TribieConstants.defaultAnimationDelay, options: .curveEaseInOut, animations: {
      cell.alpha = 0
      }, completion: { _ in
        cell.isHidden = true
        CATransaction.begin()
        tableView.beginUpdates()
        CATransaction.setCompletionBlock {
          completion()
        }
        tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.top)
        tableView.endUpdates()
        CATransaction.commit()
    })
  }
}

// MARK: - Views
fileprivate extension MenuViewController {
  func createTableView() -> UITableView {
    let tableView = UITableView()
    tableView.dataSource = self
    tableView.delegate = self
    tableView.separatorStyle = .none
    tableView.backgroundColor = .clear
    tableView.rowHeight = MenuCell.height()
    tableView.isScrollEnabled = false
    tableView.register(MenuCell.self, forCellReuseIdentifier: NSStringFromClass(MenuCell.self))
    return tableView
  }
}

// MARK: - UITableView DataSource
extension MenuViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return menuViewModel.items.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let viewModel = menuViewModel.viewModels[indexPath.row]
    let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(MenuCell.self), for: indexPath)
    (cell as? MenuCell)?.update(menuCellViewModel: viewModel)
    return cell
  }
}

// MARK: - UITableView Delegate
extension MenuViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.isUserInteractionEnabled = false
    let currentDate = Date()
    menuViewModel.selected(at: indexPath.row) { [weak self] inner in
      do {
        let tuple = try inner()
        let shouldWait = tuple.1
        let viewModel = tuple.0
        let block = {
          self?.menuViewModel = viewModel
          self?.delete(indexPath: indexPath, from: tableView) {
            if indexPath.row == viewModel.items.count {
              self?.delegate?.goToSettings()
            } else {
              self?.delegate?.reloadMainViewController()
            }
          }
        }
        let timeDiff = -CGFloat(currentDate.timeIntervalSinceNow)
        if !shouldWait || timeDiff > 2 {
          block()
        } else {
          let milliseconds = Int((2 - timeDiff) * 1000)
          DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(milliseconds), execute: block)
        }
      } catch {
        tableView.isUserInteractionEnabled = true
        AlertView.present(error: error, inView: self?.view)
      }
    }
  }
}
