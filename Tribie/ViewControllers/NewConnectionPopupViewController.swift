//
//  NewConnectionPopupViewController.swift
//  Tribie
//
//  Created by Gil on 30/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

enum PopupType {
  case newTribier(discomfort: Discomfort)
  case askTribier(discomfort: Discomfort)
  
  func discomfort() -> Discomfort {
    switch self {
    case .newTribier(let discomfort):
      return discomfort
    case .askTribier(let discomfort):
      return discomfort
    }
  }
}

protocol NewConnectionPopupViewControllerDelegate: class {
  func didStartNewChat()
}

final class NewConnectionPopupViewController: UIViewController {
  fileprivate struct Constant {
    static let margin: CGFloat = 20
    static let bigMargin: CGFloat = 50
    static let smallMargin: CGFloat = 8
  }
  
  fileprivate let dimView: UIView = {
    let view = UIView()
    view.backgroundColor = .black
    view.alpha = 0
    return view
  }()
  
  fileprivate let avatarButton: RoundButton = {
    let button = RoundButton()
    button.isUserInteractionEnabled = false
    button.setBackgroundImage(UIImage.bgNewInvite(), for: .normal)
    button.imageView?.contentMode = .scaleAspectFill
    return button
  }()
  
  fileprivate let scrollViewContentView: UIView = {
    let view = UIView()
    view.backgroundColor = .clear
    return view
  }()
  
  fileprivate let discomfort: Discomfort
  fileprivate let newConnectionViewModel: NewConnectionViewModel
  
  weak var delegate: NewConnectionPopupViewControllerDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(type: PopupType) {
    self.discomfort = type.discomfort()
    self.newConnectionViewModel = NewConnectionViewModel(discomfort: self.discomfort)
    super.init(nibName: nil, bundle: nil)
    setUp()
    configure(type: type)
  }
  
  private func setUp() {
    view.addSubview(dimView)
    
    let scrollView = UIScrollView()
    view.addSubview(scrollView)
    
    scrollView.addSubview(scrollViewContentView)
    
    let bgView = createBgView()
    scrollViewContentView.addSubview(bgView)
    
    scrollViewContentView.addSubview(avatarButton)
    
    constrain(dimView, scrollView, scrollViewContentView, bgView, avatarButton) { dim, scroll, content, bg, avatar in
      dim.top == dim.superview!.top
      dim.left == dim.superview!.left
      dim.bottom == dim.superview!.bottom
      dim.right == dim.superview!.right
      
      scroll.top == scroll.superview!.top
      scroll.left == scroll.superview!.left + Constant.margin
      scroll.bottom == scroll.superview!.bottom
      scroll.right == scroll.superview!.right - Constant.margin
      
      content.top == content.superview!.top
      content.left == content.superview!.left
      content.bottom == content.superview!.bottom
      content.right == content.superview!.right
      content.width == scroll.superview!.width - 2 * Constant.margin
      content.height >= scroll.superview!.height
      
      avatar.top == avatar.superview!.top + Constant.margin
      avatar.centerX == avatar.superview!.centerX
      avatar.width == avatar.height
      avatar.width == 120
      
      bg.top == avatar.centerY
      bg.left == bg.superview!.left
      bg.bottom == bg.superview!.bottom * 2
      bg.right == bg.superview!.right
    }
  }
}

// MARK: - Views
fileprivate extension NewConnectionPopupViewController {
  func createBgView() -> UIView {
    let view = UIView()
    view.backgroundColor = .trbAmethyst
    view.layer.borderColor = UIColor.white.cgColor
    view.layer.borderWidth = 4
    view.layer.cornerRadius = 8
    return view
  }
  
  func createTitleLabel() -> UILabel {
    let label = UILabel()
    label.numberOfLines = 0
    label.textAlignment = .center
    label.textColor = .white
    label.font = .champagneBold(size: 36)
    return label
  }
  
  func createDescriptionLabel() -> UILabel {
    let label = UILabel()
    label.numberOfLines = 0
    label.textAlignment = .center
    label.textColor = .white
    label.font = .avenirHeavy(size: 16)
    return label
  }
  
  func createRoundedButton() -> RoundedCornerButton {
    let button = RoundedCornerButton()
    button.backgroundColor = .trbTiffanyBlue
    return button
  }
  
  func createNoteButton() -> MultipleLineButton {
    let button = MultipleLineButton()
    button.titleLabel?.font = UIFont.avenirHeavy(size: 16)
    button.setTitleColor(.white, for: .normal)
    button.highlightColor = .trbTiffanyBlue
    return button
  }
}

// MARK: - Actions
extension NewConnectionPopupViewController {
  func okTapped() {
    dismiss()
    delegate?.didStartNewChat()
  }
  
  func yesTapped() {
    showWillHelp()
    newConnectionViewModel.accept()
  }
  
  func noTapped() {
    dismiss()
    newConnectionViewModel.reject()
  }
  
  func reportTapped() {
    showReport()
    newConnectionViewModel.report()
    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) { [weak self] in
      self?.dismiss()
    }
  }
}

// MARK: - Public
extension NewConnectionPopupViewController {
  func configure(type: PopupType) {
    switch type {
    case .newTribier(let discomfort):
      let titleLabel = createTitleLabel()
      titleLabel.text = String.newConnectionIntroTitle(name: discomfort.tribierNickname ?? "")
      scrollViewContentView.addSubview(titleLabel)
      
      let descriptionLabel = createDescriptionLabel()
      descriptionLabel.text = String.newConnectionUserDescription(name: discomfort.tribierNickname ?? "", discomfortTitle: discomfort.title)
      scrollViewContentView.addSubview(descriptionLabel)
      
      avatarButton.setImage(UIImage(named: discomfort.otherImageId), for: .normal)
      
      let okButton = createRoundedButton()
      okButton.setTitle(String.newConnectionOk(), for: .normal)
      okButton.addTarget(self, action: #selector(okTapped), for: .touchUpInside)
      scrollViewContentView.addSubview(okButton)
      
      constrain(titleLabel, descriptionLabel, okButton, avatarButton) { title, description, button, avatar in
        title.top == avatar.bottom + Constant.bigMargin
        title.left == title.superview!.left + Constant.margin
        title.right == title.superview!.right - Constant.margin
        
        description.top == title.bottom + Constant.bigMargin
        description.left == description.superview!.left + Constant.margin
        description.right == description.superview!.right - Constant.margin
        
        button.top == description.bottom + Constant.bigMargin
        button.left == button.superview!.left + Constant.margin
        button.bottom == button.superview!.bottom - Constant.bigMargin
        button.right == button.superview!.right - Constant.margin
      }
    case .askTribier(let discomfort):
      let titleLabel = createTitleLabel()
      titleLabel.text = String.newConnectionIntroTitle(name: discomfort.userNickname ?? "")
      scrollViewContentView.addSubview(titleLabel)
      
      let descriptionLabel = createDescriptionLabel()
      descriptionLabel.text = String.newConnectionTribierDescription(name: discomfort.userNickname ?? "", discomfortTitle: discomfort.title, discomfortDescription: discomfort.description)
      scrollViewContentView.addSubview(descriptionLabel)
      
      avatarButton.setImage(UIImage(named: discomfort.otherImageId), for: .normal)
      
      let yesButton = createRoundedButton()
      yesButton.setTitle(String.newConnectionHelp(name: discomfort.userNickname ?? "").uppercased(), for: .normal)
      yesButton.addTarget(self, action: #selector(yesTapped), for: .touchUpInside)
      scrollViewContentView.addSubview(yesButton)
      
      let noButton = createRoundedButton()
      noButton.setTitle(String.newConnectionNext(), for: .normal)
      noButton.addTarget(self, action: #selector(noTapped), for: .touchUpInside)
      scrollViewContentView.addSubview(noButton)
      
      let reportButton = createNoteButton()
      reportButton.highlightString = String.newConnectionReportHighlight()
      reportButton.setTitle(String.newConnectionReport(name: discomfort.userNickname ?? ""), for: .normal)
      reportButton.addTarget(self, action: #selector(reportTapped), for: .touchUpInside)
      scrollViewContentView.addSubview(reportButton)
      
      constrain(titleLabel, descriptionLabel, yesButton, avatarButton) { title, description, yes, avatar in
        title.top == avatar.bottom + Constant.margin
        title.left == title.superview!.left + Constant.margin
        title.right == title.superview!.right - Constant.margin
        
        description.top == title.bottom + Constant.margin
        description.left == description.superview!.left + Constant.margin
        description.right == description.superview!.right - Constant.margin
        
        yes.top == description.bottom + Constant.margin
        yes.left == yes.superview!.left + Constant.margin
        yes.right == yes.superview!.right - Constant.margin
      }
      
      constrain(yesButton, noButton, reportButton) { yes, no, report in
        no.top == yes.bottom + Constant.smallMargin
        no.left == no.superview!.left + Constant.margin
        no.right == no.superview!.right - Constant.margin
        
        report.top == no.bottom + Constant.margin
        report.left == report.superview!.left + Constant.margin
        report.bottom == report.superview!.bottom - Constant.margin
        report.right == report.superview!.right - Constant.margin
      }
    }
  }
  
  static func present(type: PopupType, in parentViewController: UIViewController) -> NewConnectionPopupViewController? {
    let viewController = NewConnectionPopupViewController(type: type)
    viewController.willMove(toParentViewController: parentViewController)
    parentViewController.addChildViewController(viewController)
    viewController.view.frame = parentViewController.view.bounds
    viewController.didMove(toParentViewController: parentViewController)
    parentViewController.view.addSubview(viewController.view)
    viewController.scrollViewContentView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: parentViewController.view.bounds.height)
    UIView.animate(withDuration: TribieConstants.defaultAnimationDuration, delay: TribieConstants.defaultAnimationDelay, options: .curveEaseInOut, animations: {
      viewController.scrollViewContentView.transform = CGAffineTransform.identity
      viewController.dimView.alpha = 0.8
      }, completion: { _ in
        
    })
    return viewController
  }
  
  func dismiss() {
    UIView.animate(withDuration: TribieConstants.defaultAnimationDuration, delay: TribieConstants.defaultAnimationDelay, options: .curveEaseInOut, animations: { [weak self] in
      self?.scrollViewContentView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: self!.view!.superview!.bounds.height)
      self?.dimView.alpha = 0
      }, completion: { [weak self] _ in
        self?.view.removeFromSuperview()
        self?.removeFromParentViewController()
    })
  }
}

// MARK: - Private
fileprivate extension NewConnectionPopupViewController {
  func showWillHelp() {
    scrollViewContentView.subviews.forEach {
      guard $0 is UILabel || $0 is RoundedCornerButton || $0 is MultipleLineButton else {
        return
      }
      $0.removeFromSuperview()
    }
    let titleLabel = createTitleLabel()
    titleLabel.text = String.newConnectionHelpTitle()
    scrollViewContentView.addSubview(titleLabel)
    
    let descriptionLabel = createDescriptionLabel()
    descriptionLabel.text = String.newConnectionHelpDescription(name: self.discomfort.otherNickname, discomfortTitle: self.discomfort.title)
    scrollViewContentView.addSubview(descriptionLabel)
    
    let okButton = createRoundedButton()
    okButton.setTitle(String.newConnectionOk(), for: .normal)
    okButton.addTarget(self, action: #selector(okTapped), for: .touchUpInside)
    scrollViewContentView.addSubview(okButton)
    
    constrain(titleLabel, descriptionLabel, okButton, avatarButton) { title, description, button, avatar in
      title.top == avatar.bottom + Constant.bigMargin
      title.left == title.superview!.left + Constant.margin
      title.right == title.superview!.right - Constant.margin
      
      description.top == title.bottom + Constant.bigMargin
      description.left == description.superview!.left + Constant.margin
      description.right == description.superview!.right - Constant.margin
      
      button.top == description.bottom + Constant.bigMargin
      button.left == button.superview!.left + Constant.margin
      button.bottom <= button.superview!.bottom - Constant.bigMargin
      button.right == button.superview!.right - Constant.margin
    }
  }
  
  func showReport() {
    scrollViewContentView.subviews.forEach {
      guard $0 is UILabel || $0 is RoundedCornerButton || $0 is MultipleLineButton else {
        return
      }
      $0.removeFromSuperview()
    }
    let titleLabel = createTitleLabel()
    titleLabel.text = String.newConnectionReportTitle()
    scrollViewContentView.addSubview(titleLabel)
    
    let descriptionLabel = createDescriptionLabel()
    descriptionLabel.text = String.newConnectionReportDescription()
    scrollViewContentView.addSubview(descriptionLabel)
    
    constrain(titleLabel, descriptionLabel, avatarButton) { title, description, avatar in
      title.top == avatar.bottom + Constant.bigMargin
      title.left == title.superview!.left + Constant.margin
      title.right == title.superview!.right - Constant.margin
      
      description.top == title.bottom + Constant.bigMargin
      description.left == description.superview!.left + Constant.margin
      description.bottom <= description.superview!.bottom - Constant.margin
      description.right == description.superview!.right - Constant.margin
    }
  }
}
