//
//  OnboardingViewController.swift
//  Tribie
//  
//
//  Created by Gilson Gil on 9/14/16. 
//    Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

final class OnboardingViewController: UIViewController {
  fileprivate struct Constant {
    static let margin: CGFloat = 12
    static let pageHeight: CGFloat = 20
    static let buttonHeight: CGFloat = 44
  }
  
  fileprivate let scrollView: UIScrollView = {
    let scrollView = UIScrollView()
    scrollView.backgroundColor = .white
    scrollView.isPagingEnabled = true
    scrollView.showsHorizontalScrollIndicator = false
    return scrollView
  }()
  
  fileprivate let skipButton: UIButton = {
    let button = UIButton(type: .custom)
    button.backgroundColor = UIColor.trbAlmostWhite
    button.setTitle(String.onboardingSkip(), for: .normal)
    button.setTitleColor(UIColor.trbTiffanyBlue, for: .normal)
    button.titleLabel?.font = UIFont.avenirMedium(size: 18)
    return button
  }()
  
  fileprivate let nextButton: UIButton = {
    let button = UIButton(type: .custom)
    button.backgroundColor = UIColor.trbAlmostWhite
    button.setTitle(String.onboardingNext(), for: .normal)
    button.setTitleColor(UIColor.trbTiffanyBlue, for: .normal)
    button.titleLabel?.font = UIFont.avenirMedium(size: 18)
    return button
  }()
  
  fileprivate let pageControl: UIPageControl = {
    let pageControl = UIPageControl()
    pageControl.pageIndicatorTintColor = UIColor.trbLightTiffany
    pageControl.currentPageIndicatorTintColor = UIColor.trbTiffanyBlue
    pageControl.numberOfPages = 4
    return pageControl
  }()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  init() {
    super.init(nibName: nil, bundle: nil)
    setUp()
  }
  
  private func setUp() {
    view.backgroundColor = .white
    
    scrollView.delegate = self
    view.addSubview(scrollView)
    
    let scrollViewContentView = createScrollViewContentView()
    scrollView.addSubview(scrollViewContentView)
    
    view.addSubview(pageControl)
    
    skipButton.addTarget(self, action: #selector(skipTapped), for: .touchUpInside)
    view.addSubview(skipButton)
    
    nextButton.addTarget(self, action: #selector(nextTapped), for: .touchUpInside)
    view.addSubview(nextButton)
    
    constrain(scrollView, scrollViewContentView, pageControl, skipButton, nextButton) { scroll, content, page, skip, next in
      scroll.top == scroll.superview!.top
      scroll.left == scroll.superview!.left
      scroll.right == scroll.superview!.right
      
      content.top == content.superview!.top
      content.left == content.superview!.left
      content.bottom == content.superview!.bottom
      content.right == content.superview!.right
      content.bottom == scroll.superview!.bottom - Constant.buttonHeight - Constant.pageHeight
      
      page.top == scroll.bottom
      page.left == page.superview!.left
      page.right == page.superview!.right
      page.height == Constant.pageHeight
      
      skip.top == page.bottom
      skip.left == skip.superview!.left
      skip.bottom == skip.superview!.bottom
      skip.right == skip.superview!.centerX
      skip.height == Constant.buttonHeight
      
      next.top == skip.top
      next.left == next.superview!.centerX
      next.bottom == next.superview!.bottom
      next.right == next.superview!.right
      next.height == Constant.buttonHeight
    }
    
    let page1 = createPage1()
    scrollViewContentView.addSubview(page1)
    
    let page2 = createPage2()
    scrollViewContentView.addSubview(page2)
    
    let page3 = createPage3()
    scrollViewContentView.addSubview(page3)
    
    let page4 = createPage4()
    scrollViewContentView.addSubview(page4)
    
    constrain(page1, page2, page3, page4, view!) { p1, p2, p3, p4, view in
      p1.top == p1.superview!.top
      p1.left == p1.superview!.left
      p1.bottom == p1.superview!.bottom
      p1.width == view.width
      
      p2.top == p2.superview!.top
      p2.left == p1.right
      p2.bottom == p2.superview!.bottom
      p2.width == view.width
      
      p3.top == p3.superview!.top
      p3.left == p2.right
      p3.bottom == p3.superview!.bottom
      p3.width == view.width
      
      p4.top == p4.superview!.top
      p4.left == p3.right
      p4.bottom == p4.superview!.bottom
      p4.right == p4.superview!.right
      p4.width == view.width
    }
  }
  
  override var prefersStatusBarHidden: Bool {
    get {
      return true
    }
  }
}

// MARK: - Views
fileprivate extension OnboardingViewController {
  func createScrollViewContentView() -> UIView {
    let view = UIView()
    view.backgroundColor = .white
    return view
  }
  
  func createPage1() -> UIView {
    let views = createOnboardingPage()
    let page = views.page
    views.imageButton.setImage(UIImage.img48(), for: .normal)
    views.imageButton.setBackgroundImage(UIImage.imgWelcome(), for: .normal)
    views.titleLabel.text = String.onboarding1Title()
    views.subtitleLabel.text = String.onboarding1Subtitle()
    return page
  }
  
  func createPage2() -> UIView {
    let views = createOnboardingPage()
    let page = views.page
    views.imageButton.setBackgroundImage(UIImage.imgNew(), for: .normal)
    views.titleLabel.text = String.onboarding2Title()
    views.subtitleLabel.text = String.onboarding2Subtitle()
    return page
  }
  
  func createPage3() -> UIView {
    let views = createOnboardingPage()
    let page = views.page
    views.imageButton.setBackgroundImage(UIImage.imgPrivacy(), for: .normal)
    views.titleLabel.text = String.onboarding3Title()
    views.subtitleLabel.text = String.onboarding3Subtitle()
    return page
  }
  
  func createPage4() -> UIView {
    let views = createOnboardingPage()
    let page = views.page
    views.imageButton.setBackgroundImage(UIImage.imgHelping(), for: .normal)
    views.titleLabel.text = String.onboarding4Title()
    views.subtitleLabel.text = String.onboarding4Subtitle()
    return page
  }
  
  func createOnboardingPage() -> (page: UIView, imageButton: UIButton, titleLabel: UILabel, subtitleLabel: UILabel) {
    let page = UIView()
    page.backgroundColor = .white
    
    let buttonContainer = UIView()
    buttonContainer.backgroundColor = .white
    page.addSubview(buttonContainer)
    
    let button = UIButton(type: .custom)
    button.isUserInteractionEnabled = false
    button.imageView?.contentMode = .center
    button.setContentHuggingPriority(UILayoutPriorityDefaultLow, for: .vertical)
    buttonContainer.addSubview(button)
    
    let titleLabel = UILabel()
    titleLabel.numberOfLines = 0
    titleLabel.textAlignment = .center
    titleLabel.textColor = .black
    titleLabel.font = UIFont.champagneBold(size: 32)
    titleLabel.setContentHuggingPriority(UILayoutPriorityRequired, for: .vertical)
    page.addSubview(titleLabel)
    
    let subtitleLabel = UILabel()
    subtitleLabel.numberOfLines = 0
    subtitleLabel.textAlignment = .center
    subtitleLabel.textColor = .black
    subtitleLabel.font = UIFont.champagne(size: 22)
    subtitleLabel.adjustsFontSizeToFitWidth = true
    subtitleLabel.minimumScaleFactor = 0.7
    subtitleLabel.setContentHuggingPriority(UILayoutPriorityRequired, for: .vertical)
    page.addSubview(subtitleLabel)
    
    constrain(buttonContainer, button, titleLabel, subtitleLabel) { container, button, title, subtitle in
      container.top == container.superview!.top
      container.left == container.superview!.left
      container.right == container.superview!.right
      
      button.top >= button.superview!.top
      button.left >= button.superview!.left
      button.bottom <= button.superview!.bottom
      button.right <= button.superview!.right
      button.center == button.superview!.center
      
      title.top == container.bottom + Constant.margin
      title.left == title.superview!.left + Constant.margin
      title.right == title.superview!.right - Constant.margin
      
      subtitle.top == title.bottom + Constant.margin
      subtitle.left == subtitle.superview!.left + Constant.margin
      subtitle.bottom == subtitle.superview!.bottom - Constant.margin
      subtitle.right == subtitle.superview!.right - Constant.margin
    }
    return (page: page, imageButton: button, titleLabel: titleLabel, subtitleLabel: subtitleLabel)
  }
}

// MARK: - Actions
extension OnboardingViewController {
  func skipTapped() {
    startTapped()
  }
  
  func nextTapped() {
    let currentX = scrollView.contentOffset.x
    let newX = currentX + scrollView.bounds.width
    scrollView.setContentOffset(CGPoint(x: newX, y: 0), animated: true)
  }
  
  func startTapped() {
    UserDefaults.standard.set(true, forKey: TribieConstants.onboardedUserDefaultsKey)
    UserDefaults.standard.synchronize()
    defer {
      (UIApplication.shared.delegate as? AppDelegate)?.loadApplication(fromSignup: false)
    }
  }
}

// MARK: - Private
fileprivate extension OnboardingViewController {
  func handlePage(_ page: Int) {
    guard pageControl.currentPage != page else {
      return
    }
    pageControl.currentPage = page
    nextButton.removeTarget(self, action: nil, for: .touchUpInside)
    if page == 3 {
      nextButton.setTitle(String.onboardingStart(), for: .normal)
      nextButton.addTarget(self, action: #selector(startTapped), for: .touchUpInside)
    } else {
      nextButton.setTitle(String.onboardingNext(), for: .normal)
      nextButton.addTarget(self, action: #selector(nextTapped), for: .touchUpInside)
    }
  }
}

// MARK: - UIScrollView Delegate
extension OnboardingViewController: UIScrollViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let page = Int(scrollView.contentOffset.x / scrollView.bounds.width + 0.5)
    handlePage(page)
  }
}
