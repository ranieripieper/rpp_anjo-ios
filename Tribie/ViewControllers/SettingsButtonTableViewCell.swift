//
//  SettingsButtonTableViewCell.swift
//  Tribie
//
//  Created by Gilson Gil on 25/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

final class SettingsButtonTableViewCell: UITableViewCell {
  internal let label: UILabel = {
    let label = UILabel()
    label.textColor = UIColor.trbPinkishGrey
    label.font = UIFont.avenirRoman(size: 18)
    return label
  }()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.addSubview(label)
    
    let margin: CGFloat = 16
    constrain(label) { label in
      label.top == label.superview!.top + margin
      label.left == label.superview!.left + margin
      label.bottom == label.superview!.bottom - margin
      label.right == label.superview!.right - margin
    }
  }
}

// MARK: - Public
extension SettingsButtonTableViewCell {
  func update(settingsItem: SettingsItem) {
    switch settingsItem {
    case .button(let title, _):
      label.text = title
    default:
      break
    }
  }
}
