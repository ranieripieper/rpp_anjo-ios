//
//  SettingsSwitchTableViewCell.swift
//  Tribie
//
//  Created by Gilson Gil on 25/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

final class SettingsSwitchTableViewCell: UITableViewCell {
  internal let label: UILabel = {
    let label = UILabel()
    label.textColor = UIColor.trbPinkishGrey
    label.adjustsFontSizeToFitWidth = true
    label.minimumScaleFactor = 0.7
    label.font = UIFont.avenirRoman(size: 18)
    return label
  }()
  
  internal let aSwitch: UISwitch = {
    let aSwitch = UISwitch()
    aSwitch.onTintColor = UIColor.trbBlush
    return aSwitch
  }()
  
  internal var action: ((SettingsViewModel, Bool, @escaping (Error?) -> ()) -> ())?
  internal var viewModel: SettingsViewModel?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.addSubview(label)
    
    aSwitch.addTarget(self, action: #selector(switchChangedValue(aSwitch:)), for: .valueChanged)
    contentView.addSubview(aSwitch)
    
    let margin: CGFloat = 16
    constrain(label, aSwitch) { label, aSwitch in
      label.top == label.superview!.top + margin
      label.left == label.superview!.left + margin
      label.bottom == label.superview!.bottom - margin
      
      aSwitch.top == aSwitch.superview!.top + margin
      aSwitch.left == label.right + margin
      aSwitch.bottom == aSwitch.superview!.bottom - margin
      aSwitch.right == aSwitch.superview!.right - margin
    }
  }
}

// MARK: - Actions
extension SettingsSwitchTableViewCell {
  func switchChangedValue(aSwitch: UISwitch) {
    guard let viewModel = viewModel else {
      return
    }
    action?(viewModel, aSwitch.isOn) { error in
      guard error != nil else {
        return
      }
      aSwitch.isOn = !aSwitch.isOn
    }
  }
}

// MARK: - Public
extension SettingsSwitchTableViewCell {
  func update(settingsItem: SettingsItem, viewModel: SettingsViewModel) {
    self.viewModel = viewModel
    switch settingsItem {
    case .aSwitch(let title, let value, let action):
      label.text = title
      aSwitch.isOn = value
      self.action = action
    default:
      break
    }
  }
}
