//
//  SettingsViewController.swift
//  Tribie
//
//  Created by Gilson Gil on 25/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

final class SettingsViewController: UIViewController {
  fileprivate var settingsViewModel: SettingsViewModel!
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  init() {
    super.init(nibName: nil, bundle: nil)
    settingsViewModel = SettingsViewModel(self)
    setUp()
  }
  
  private func setUp() {
    let tableView = newTableView()
    view.addSubview(tableView)
    
    constrain(tableView) { tableView in
      tableView.top == tableView.superview!.top
      tableView.left == tableView.superview!.left
      tableView.bottom == tableView.superview!.bottom
      tableView.right == tableView.superview!.right
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.setHidesBackButton(false, animated: false)
  }
}

// MARK: - Views
internal extension SettingsViewController {
  func newTableView() -> UITableView {
    let tableView = UITableView()
    tableView.dataSource = self
    tableView.delegate = self
    tableView.estimatedRowHeight = 44
    tableView.separatorInset = .zero
    tableView.register(SettingsSwitchTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(SettingsSwitchTableViewCell.self))
    tableView.register(SettingsButtonTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(SettingsButtonTableViewCell.self))
    return tableView
  }
}

// MARK: - UITableView DataSource
extension SettingsViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return settingsViewModel.items.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let item = settingsViewModel.items[indexPath.row]
    switch item {
    case .aSwitch:
      guard let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(SettingsSwitchTableViewCell.self), for: indexPath) as? SettingsSwitchTableViewCell else {
        return UITableViewCell()
      }
      cell.update(settingsItem: item, viewModel: settingsViewModel)
      return cell
    case .button:
      guard let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(SettingsButtonTableViewCell.self), for: indexPath) as? SettingsButtonTableViewCell else {
        return UITableViewCell()
      }
      cell.update(settingsItem: item)
      return cell
    }
  }
}

// MARK: - UITableView Delegate
extension SettingsViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 1
  }
  
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let footer = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 1))
    footer.backgroundColor = UIColor.groupTableViewBackground
    return footer
  }
  
  func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
    let item = settingsViewModel.items[indexPath.row]
    switch item {
    case .button:
      return indexPath
    default:
      return nil
    }
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let item = settingsViewModel.items[indexPath.row]
    switch item {
    case .button(_, let action):
      action(settingsViewModel) { [weak self] error in
        SVProgressHUDHelper.dismiss()
        if let error = error {
          AlertView.present(error: error, inView: self?.view)
        } else {
          (UIApplication.shared.delegate as? AppDelegate)?.loadApplication(fromSignup: false)
        }
      }
    default:
      break
    }
    tableView.deselectRow(at: indexPath, animated: true)
  }
}

// MARK: - AlertController Delegate
extension SettingsViewController: AlertControllerDelegate {
  func deleteAccount() {
    SVProgressHUDHelper.show()
    SignupService.deleteAccount { [weak self] inner in
      SVProgressHUDHelper.dismiss()
      do {
        try inner()
        User.depersist()
        (UIApplication.shared.delegate as? AppDelegate)?.loadApplication(fromSignup: false)
      } catch {
        AlertView.present(error: error, inView: self?.view)
      }
    }
  }
  
  func didDismiss() {
    
  }
}
