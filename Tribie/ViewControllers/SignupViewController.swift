//
//  SignupViewController.swift
//  Tribie
//
//  Created by Gilson Gil on 9/14/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

final class SignupViewController: UIViewController {
  internal let hMargin: CGFloat = 20
  internal let vMargin: CGFloat = 12
  internal let topMargin: CGFloat = 140
  
  var loginContainer: LoginContainer?
  
  fileprivate let scrollView = UIScrollView()
  internal let nameTextField: CenteredGrayTextField = {
    let textField = CenteredGrayTextField()
    textField.autocapitalizationType = .words
    textField.placeholder = String.signupNamePlaceholder()
    return textField
  }()
  internal let emailTextField: CenteredGrayTextField = {
    let textField = CenteredGrayTextField()
    textField.autocapitalizationType = .none
    textField.autocorrectionType = .no
    textField.keyboardType = .emailAddress
    textField.placeholder = String.signupEmailPlaceholder()
    return textField
  }()
  internal let passwordTextField: CenteredGrayTextField = {
    let textField = CenteredGrayTextField()
    textField.isSecureTextEntry = true
    textField.installSeePasswordButton()
    textField.placeholder = String.signupPasswordPlaceholder()
    return textField
  }()
  internal let birthyearTextField: CenteredGrayTextField = {
    let textField = CenteredGrayTextField()
    let datePicker = UIDatePicker()
    datePicker.datePickerMode = .date
    datePicker.maximumDate = Date()
    textField.inputView = datePicker
    textField.placeholder = String.signupAgePlaceholder()
    return textField
  }()
  
  internal var signupViewModel = SignupViewModel()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }

  init() {
    super.init(nibName: nil, bundle: nil)
    setUp()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    let keyboardHandler = KeyboardHandlerViewController(scrollView: scrollView)
    addChildViewController(keyboardHandler)
    view.addSubview(keyboardHandler.view)
    keyboardHandler.view.frame = .zero
    keyboardHandler.didMove(toParentViewController: self)
  }

  private func setUp() {
    view.backgroundColor = UIColor.white
    
    view.addSubview(scrollView)
    
    let scrollViewContentView = UIView()
    scrollView.addSubview(scrollViewContentView)
    
    let tap = UITapGestureRecognizer(target: self, action: #selector(tapped))
    scrollViewContentView.addGestureRecognizer(tap)
    
    let titleLabel = newTitleLabel()
    scrollViewContentView.addSubview(titleLabel)
    
    let subtitleLabel = newSubtitleLabel()
    scrollViewContentView.addSubview(subtitleLabel)
    
    nameTextField.delegate = self
    scrollViewContentView.addSubview(nameTextField)
    
    emailTextField.delegate = self
    scrollViewContentView.addSubview(emailTextField)
    
    passwordTextField.delegate = self
    scrollViewContentView.addSubview(passwordTextField)
    
    birthyearTextField.delegate = self
    scrollViewContentView.addSubview(birthyearTextField)
    
    let signupButton = newSignupButton()
    scrollViewContentView.addSubview(signupButton)
    
    let termsButton = newTermsButton()
    scrollViewContentView.addSubview(termsButton)
    
    let loginButton = newLoginButton()
    scrollViewContentView.addSubview(loginButton)
    
    constrain([titleLabel, subtitleLabel, nameTextField, emailTextField, passwordTextField, birthyearTextField, signupButton, termsButton, loginButton]) { views in
      views.forEach {
        $0.left == $0.superview!.left + hMargin ~ 900
        $0.right == $0.superview!.right - hMargin ~ 900
        $0.centerX == $0.superview!.centerX
      }
      views[0].top == views[0].superview!.top + topMargin
      views[1].top == views[0].bottom + vMargin
      views[2].top == views[1].bottom + 2 * hMargin
      views[3].top == views[2].bottom + vMargin
      views[4].top == views[3].bottom + vMargin
      views[5].top == views[4].bottom + vMargin
      views[6].top == views[5].bottom + 2 * hMargin
      views[7].top == views[6].bottom + vMargin
      views[8].top == views[7].bottom + vMargin
      views[8].superview!.bottom == views[8].bottom + 2 * vMargin
    }
    
    constrain(scrollViewContentView, scrollView) { scrollViewContentView, scrollView in
      scrollViewContentView.top == scrollView.top
      scrollViewContentView.left == scrollView.left
      scrollViewContentView.bottom == scrollView.bottom
      scrollViewContentView.right == scrollView.right
      
      scrollView.top == scrollView.superview!.top
      scrollView.left == scrollView.superview!.left
      scrollView.bottom == scrollView.superview!.bottom
      scrollView.right == scrollView.superview!.right
      
      scrollViewContentView.width == scrollView.superview!.width
      scrollViewContentView.height >= scrollView.superview!.height
    }
  }
}

// MARK: - Views
private extension SignupViewController {
  func newTitleLabel() -> UILabel {
    let titleLabel = UILabel()
    titleLabel.textAlignment = .center
    titleLabel.textColor = UIColor.black
    titleLabel.text = String.signupTitle()
    titleLabel.font = UIFont.champagneBold(size: 36)
    return titleLabel
  }
  
  func newSubtitleLabel() -> UILabel {
    let subtitleLabel = UILabel()
    subtitleLabel.numberOfLines = 0
    subtitleLabel.textAlignment = .center
    subtitleLabel.textColor = UIColor.black
    subtitleLabel.text = String.signupSubtitle()
    subtitleLabel.font = UIFont.champagne(size: 24)
    return subtitleLabel
  }
  
  func newSignupButton() -> RoundedCornerButton {
    let signupButton = RoundedCornerButton()
    signupButton.backgroundColor = UIColor.trbTiffanyBlue
    signupButton.setTitle(String.signupSignupButton(), for: .normal)
    signupButton.addTarget(self, action: #selector(signupTapped), for: .touchUpInside)
    return signupButton
  }
  
  func newTermsButton() -> MultipleLineButton {
    let termsButton = MultipleLineButton()
    termsButton.titleLabel?.font = UIFont.champagne(size: 16)
    termsButton.setTitleColor(UIColor.trbBrownishGrey, for: .normal)
    termsButton.highlightColor = UIColor.trbBlush
    termsButton.highlightString = String.signupTermsButtonHighlight()
    termsButton.setTitle(String.signupTermsButton(), for: .normal)
    termsButton.addTarget(self, action: #selector(termsTapped), for: .touchUpInside)
    return termsButton
  }
  
  func newLoginButton() -> MultipleLineButton {
    let loginButton = MultipleLineButton()
    loginButton.titleLabel?.font = UIFont.avenirRoman(size: 16)
    loginButton.highlightColor = UIColor.trbTiffanyBlue
    loginButton.highlightString = String.signupLoginButtonHighlight()
    loginButton.setTitle(String.signupLoginButton(), for: .normal)
    loginButton.addTarget(self, action: #selector(loginTapped), for: .touchUpInside)
    return loginButton
  }
}

// MARK: - Actions
extension SignupViewController {
  func tapped() {
    view.endEditing(true)
  }
  
  func loginTapped() {
    goToLogin()
  }
  
  func termsTapped() {
    goToTerms()
  }
  
  func signupTapped() {
    signup()
  }
}

// MARK: - Private
private extension SignupViewController {
  func goToLogin() {
    push(viewController: LoginViewController())
  }
  
  func goToTerms() {
    present(TermsViewController(), animated: true, completion: nil)
  }
  
  func signup() {
    SVProgressHUDHelper.show()
    signupViewModel.signup(name: nameTextField.text, email: emailTextField.text, password: passwordTextField.text, birthDate: (birthyearTextField.inputView as? UIDatePicker)?.date) { [weak self] inner in
      SVProgressHUDHelper.dismiss()
      do {
        try inner()
        (UIApplication.shared.delegate as? AppDelegate)?.loadApplication(fromSignup: true)
      } catch let error as SignUpError {
        switch error {
        case .missingField:
          [self?.nameTextField, self?.emailTextField, self?.passwordTextField, self?.birthyearTextField].forEach {
            $0?.validate()
          }
        case .ageMinor, .invalidPassword:
          let view: UIView = self?.loginContainer?.view ?? (self?.view)!
          AlertView.present(error: error, inView: view, bottomSpacing: 20)
        }
      } catch {
        let view: UIView = self?.loginContainer?.view ?? (self?.view)!
        AlertView.present(error: error, inView: view, bottomSpacing: 20)
      }
    }
  }
}

// MARK: UITextField Delegate
extension SignupViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    switch textField {
    case nameTextField:
      emailTextField.becomeFirstResponder()
    case emailTextField:
      passwordTextField.becomeFirstResponder()
    case passwordTextField:
      birthyearTextField.becomeFirstResponder()
    case birthyearTextField:
      birthyearTextField.resignFirstResponder()
    default:
      break
    }
    return true
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    guard textField == birthyearTextField else {
      return true
    }
    guard let text = textField.text?.replacingCharacters(in: range.range(for: textField.text!)!, with: string) else {
      return true
    }
    return text.characters.count <= 4
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    guard let datePicker = textField.inputView as? UIDatePicker else {
      return
    }
    textField.text = signupViewModel.toAPIDateFormatter.string(from: datePicker.date)
  }
}

// MARK: - NavigationControllerProtocol
extension SignupViewController: LoginContainerProtocol {}
