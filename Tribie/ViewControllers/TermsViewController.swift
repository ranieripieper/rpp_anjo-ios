//
//  TermsViewController.swift
//  Tribie
//  
//
//  Created by Gilson Gil on 9/14/16. 
//    Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

final class TermsViewController: UIViewController {
  private let margin: CGFloat = 20
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  init() {
    super.init(nibName: nil, bundle: nil)
    setUp()
  }
  
  private func setUp() {
    view.backgroundColor = UIColor.white
    
    let backgroundImageView = newBackgroundImageView()
    view.addSubview(backgroundImageView)
    
    let containerView = newContainerView()
    view.addSubview(containerView)
    
    let closeButton = newCloseButton()
    containerView.addSubview(closeButton)
    
    let titleLabel = newTitleLabel()
    containerView.addSubview(titleLabel)
    
    let textView = newTermsTextView()
    containerView.addSubview(textView)
    
    constrain(backgroundImageView) { view in
      view.top == view.superview!.top
      view.left == view.superview!.left
      view.bottom == view.superview!.bottom
      view.right == view.superview!.right
    }
    
    constrain(containerView) { view in
      view.top == view.superview!.top
      view.left == view.superview!.left
      view.bottom == view.superview!.bottom - margin
      view.right == view.superview!.right
    }
    
    constrain(closeButton, titleLabel, textView) { button, label, textview in
      button.top == button.superview!.top + margin
      button.right == button.superview!.right - margin
      
      label.top == button.bottomMargin
      label.left == label.superview!.left + margin
      label.right == label.superview!.right - margin
      
      textview.top == label.bottom + margin
      textview.left == textview.superview!.left + margin
      textview.bottom == textview.superview!.bottom - margin
      textview.right == textview.superview!.right - margin
    }
  }
}

// MARK: - Views
private extension TermsViewController {
  func newBackgroundImageView() -> UIImageView {
    let image = UIImage.imgBottomTerms()
    let imageView = UIImageView(image: image)
    return imageView
  }
  
  func newContainerView() -> UIView {
    let view = UIView()
    view.backgroundColor = UIColor.white
    return view
  }
  
  func newCloseButton() -> UIButton {
    let button = UIButton(type: .custom)
    button.setTitleColor(UIColor.black, for: .normal)
    button.setTitle("X", for: .normal)
    button.titleLabel?.font = UIFont.avenirHeavy(size: 40)
    button.addTarget(self, action: #selector(dismissViewController), for: .touchUpInside)
    return button
  }
  
  func newTitleLabel() -> UILabel {
    let label = UILabel()
    label.textColor = UIColor.black
    label.font = UIFont.champagneBold(size: 36)
    label.text = String.termsTitle()
    return label
  }
  
  func newTermsTextView() -> UITextView {
    let textView = UITextView()
    textView.textColor = UIColor.black
    textView.font = UIFont.champagne(size: 16)
    if let path = Bundle.main.path(forResource: "Terms", ofType: "txt") {
      let url = URL(fileURLWithPath: path)
      let terms = try? String(contentsOf: url, encoding: String.Encoding.utf8)
      textView.text = terms
    }
    return textView
  }
}

// MARK: Actions
extension TermsViewController {
  func dismissViewController() {
    dismiss(animated: true, completion: nil)
  }
}
