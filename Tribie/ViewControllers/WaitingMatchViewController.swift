//
//  WaitingMatchViewController.swift
//  Tribie
//
//  Created by Gilson Gil on 25/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography
import SwiftGifOrigin

final class WaitingMatchViewController: UIViewController {
  fileprivate let reported: Bool
  
  required init?(coder aDecoder: NSCoder) {
    self.reported = false
    super.init(coder: aDecoder)
    setUp()
  }
  
  init(reported: Bool) {
    self.reported = reported
    super.init(nibName: nil, bundle: nil)
    setUp()
    addSettingsButton(action: #selector(menuTapped))
  }
  
  private func setUp() {
    view.backgroundColor = UIColor.white
    
    let imageView = newImageView()
    view.addSubview(imageView)
    
    let label = newLabel()
    view.addSubview(label)
    
    let hMargin: CGFloat = 20
    let vMargin: CGFloat = 60
    constrain(imageView, label) { imageView, label in
      imageView.centerX == imageView.superview!.centerX
      imageView.bottom == imageView.superview!.centerY
      
      label.left >= label.superview!.left + hMargin
      label.right <= label.superview!.right - hMargin
      label.centerX == label.superview!.centerX
      label.top == label.superview!.centerY + vMargin
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.setHidesBackButton(true, animated: false)
  }
}

// MARK: - Views
internal extension WaitingMatchViewController {
  func newImageView() -> UIImageView {
    let imageView = UIImageView()
    imageView.contentMode = .scaleAspectFit
    imageView.loadGif(name: "searching")
    return imageView
  }
  
  func newLabel() -> UILabel {
    let label = UILabel()
    label.numberOfLines = 0
    label.textAlignment = .center
    label.textColor = UIColor.trbWhiteTwo
    label.font = UIFont.avenirBlack(size: 20)
    if reported {
      label.text = String.waitingMatchReport()
    } else {
      label.text = String.waitingMatchLabel()
    }
    label.adjustsFontSizeToFitWidth = true
    label.minimumScaleFactor = 0.8
    return label
  }
}

// MARK: - MainNavigationController Protocol
extension WaitingMatchViewController: MainNavigationControllerProtocol {
  func menuTapped() {
    presentMenu(discomfort: nil, completion: nil)
  }
}
