//
//  WelcomeViewController.swift
//  Tribie
//
//  Created by Gilson Gil on 24/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

final class WelcomeViewController: UIViewController {
  internal struct Constant {
    static let margin: CGFloat = 20
    static let contentMargin: CGFloat = 40
    static let buttonHeight: CGFloat = 60
  }
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  init() {
    super.init(nibName: nil, bundle: nil)
    setUp()
    addSettingsButton(action: #selector(menuTapped))
  }
  
  private func setUp() {
    let bgImageView = newBgImageView()
    view.addSubview(bgImageView)
    
    let bgView = newBgView()
    view.addSubview(bgView)
    
    let titleLabel = newTitleLabel()
    bgView.addSubview(titleLabel)
    
    let contentLabel = newContentLabel()
    bgView.addSubview(contentLabel)
    
    let button = newButton()
    bgView.addSubview(button)
    
    let margin = Constant.margin
    let contentMargin = Constant.contentMargin
    let buttonHeight = Constant.buttonHeight
    constrain(bgImageView, bgView, titleLabel, contentLabel, button) { bg, bgView, title, content, button in
      bg.top == bg.superview!.top
      bg.left == bg.superview!.left
      bg.bottom == bg.superview!.bottom
      bg.right == bg.superview!.right
      
      bgView.top == topLayoutGuideCartography + margin
      bgView.left == bgView.superview!.left + margin
      bgView.bottom == bgView.superview!.bottom - margin
      bgView.right == bgView.superview!.right - margin
      
      title.top >= title.superview!.top + margin / 2
      title.left == title.superview!.left + margin / 2
      title.right == title.superview!.right - margin / 2
      
      content.top == title.bottom + contentMargin
      content.left == content.superview!.left + margin / 2
      content.right == content.superview!.right - margin / 2
      content.centerY == content.superview!.centerY ~ 750
      
      button.top == content.bottom + contentMargin
      button.left == button.superview!.left + margin / 2
      button.bottom <= button.superview!.bottom - margin / 2
      button.right == button.superview!.right - margin / 2
      button.height == buttonHeight
    }
  }
}

// MARK: - Views
private extension WelcomeViewController {
  func newBgImageView() -> UIImageView {
    let image = UIImage.bgWelcome()
    let imageView = UIImageView(image: image)
    return imageView
  }
  
  func newBgView() -> UIView {
    let view = UIView()
    view.backgroundColor = UIColor.white
    return view
  }
  
  func newTitleLabel() -> UILabel {
    let label = UILabel()
    label.numberOfLines = 0
    label.textAlignment = .center
    label.textColor = UIColor.trbTiffanyBlue
    label.font = UIFont.champagneBold(size: 36)
    label.text = String.welcomeTitle()
    return label
  }
  
  func newContentLabel() -> UILabel {
    let label = UILabel()
    label.numberOfLines = 0
    label.textAlignment = .center
    label.textColor = UIColor.black
    label.font = UIFont.champagne(size: 20)
    label.text = String.welcomeContent()
    return label
  }
  
  func newButton() -> UIButton {
    let button = RoundedCornerButton()
    button.backgroundColor = UIColor.trbTiffanyBlue
    button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
    button.setTitle(String.welcomeButton(), for: .normal)
    return button
  }
}

// MARK: - Actions
extension WelcomeViewController {
  func buttonTapped() {
    push(viewController: CreateProblemViewController())
    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) { [weak self] in
      guard let topViewController = self?.navigationController?.viewControllers.last else {
        return
      }
      topViewController.navigationItem.leftBarButtonItems = [UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)]
      self?.navigationController?.viewControllers = [topViewController]
    }
  }
}

// MARK: - MainNavigationControllerProtocol
extension WelcomeViewController: MainNavigationControllerProtocol {
  func menuTapped() {
    presentMenu(discomfort: nil, completion: nil)
  }
}
