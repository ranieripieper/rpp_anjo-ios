//
//  ChatViewModel.swift
//  Tribie
//
//  Created by Gil on 28/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import RealmSwift

struct ChatViewModel {
  let dataSource = ChatDataSource()
  let itemsDecorator = ChatItemDecorator()
  let ablyHelper: AblyHelper?
  private let realm = try? Realm()
  
  let discomfort: Discomfort
  internal var currentUserId: Int {
    return User.current?.userId ?? 0
  }
  internal var username: String {
    if discomfort.userId == currentUserId {
      return discomfort.userNickname ?? ""
    } else {
      return discomfort.tribierNickname ?? ""
    }
  }
  internal var otherUser: (userId: Int, username: String) {
    if discomfort.userId == currentUserId {
      return (userId: discomfort.tribierId ?? 0, username: discomfort.tribierNickname ?? "")
    } else {
      return (userId: discomfort.userId ?? 0, username: discomfort.userNickname ?? "")
    }
  }
  var channel: String? {
    guard let userId = discomfort.userId, let tribierId = discomfort.tribierId else {
      return nil
    }
    return "tribie:discomfort_\(discomfort.discomfortId)_user_\(userId)_tribier_\(tribierId)"
  }
  
  init(discomfort: Discomfort, ablyHelper: AblyHelper?) {
    self.discomfort = discomfort
    self.ablyHelper = ablyHelper
    guard let channel = channel else {
      return
    }
    let predicate = NSPredicate(format: "name == %@ && channel == %@", argumentArray: ["message", channel])
    let results = realm?.objects(Event.self).filter(predicate).sorted(byProperty: "date", ascending: true)
    handle(results?.flatMap { $0 } ?? [])
  }

  func handle(_ events: [Event]) {
    events.forEach {
      let avatarImageName: String
      let username: String
      if $0.clientId == $0.discomfort.userId {
        avatarImageName = $0.discomfort.userImageId
        username = $0.discomfortUserNickname
      } else {
        avatarImageName = $0.discomfort.tribierImageId
        username = $0.discomfortTribierNickname
      }
      dataSource.addTextMessage(text: $0.message, userId: $0.clientId, username: username, date: $0.date, avatarImageName: avatarImageName)
    }
    dataSource.delegate?.chatDataSourceDidUpdate(dataSource)
  }
  
  func publish(_ message: String, completion: @escaping (_ inner: () throws -> ()) -> ()) {
    ablyHelper?.send(message: message, to: discomfort, from: username) { inner in
      do {
        try inner()
        
        completion { return }
      } catch {
        completion { throw error }
      }
    }
  }
}
