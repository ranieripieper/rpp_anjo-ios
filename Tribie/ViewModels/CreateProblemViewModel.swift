//
//  CreateProblemViewModel.swift
//  Tribie
//
//  Created by Gil on 28/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

enum CreateProblemError: Error, ErrorProtocol {
  case missingField
  
  func localizedDescription() -> String {
    switch self {
    case .missingField:
      return ""
    }
  }
}

struct CreateProblemViewModel {
  func create(_ title: String?, description: String?, completion: @escaping (_ inner: () throws -> ()) -> ()) {
    guard let title = title, title.characters.count > 0, let description = description, description.characters.count > 0 else {
      completion { throw CreateProblemError.missingField }
      return
    }
    DiscomfortService.create(title, description: description) { inner in
      do {
        try inner()
        
        completion { return }
      } catch {
        completion { throw error }
      }
    }
  }
}
