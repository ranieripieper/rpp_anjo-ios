//
//  LoginViewModel.swift
//  Tribie
//
//  Created by Gilson Gil on 9/14/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

enum LoginError: Error, ErrorProtocol {
  case missingField, wrongCredentials
  
  func localizedDescription() -> String {
    switch self {
    case .missingField:
      return ""
    case .wrongCredentials:
      return ""
    }
  }
}

struct LoginViewModel {
  func login(email: String?, password: String?, completion: @escaping (_ inner: () throws -> ()) -> ()) {
    guard let email = email, email.characters.count > 0, let password = password, password.characters.count > 0 else {
      completion { throw LoginError.missingField }
      return
    }
    LoginService.signin(email, password: password, completion: completion)
  }
  
  func forgotPassword(email: String?, completion: @escaping (_ inner: () throws -> ()) -> ()) {
    guard let email = email, email.characters.count > 0 else {
      completion { throw LoginError.missingField }
      return
    }
    ForgotPasswordService.reset(email, completion: completion)
  }
}
