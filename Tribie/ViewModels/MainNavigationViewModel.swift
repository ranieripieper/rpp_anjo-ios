//
//  MainNavigationViewModel.swift
//  Tribie
//
//  Created by Gil on 27/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import Ably

enum MainStatus {
  case fromSignup, noDiscomfort, inChat([Discomfort], Int, Bool)
}

struct MainNavigationViewModel {
  var status: MainStatus
  var ablyHelper: AblyHelper!
  let mainNavigationController: MainNavigationController
  fileprivate var paginatedResults = [ARTPaginatedResult<ARTMessage>]()
  
  fileprivate static let operationQueue = OperationQueue()
  
  init(status: MainStatus, mainNavigationController: MainNavigationController) {
    self.status = status
    self.mainNavigationController = mainNavigationController
    ablyHelper = AblyHelper(userId: String(User.current?.userId ?? 0))
    ablyHelper.delegate = self
    connect()
  }
  
  func connect() {
    ablyHelper.connect { _ in }
  }
  
  func subscribe(to channel: String) {
    ablyHelper.subscribeTo(channelString: channel)
  }
  
  func unsubscribe() {
    switch status {
    case .inChat(let discomforts, _, _):
      discomforts.forEach {
        guard let channel = $0.channel else {
          return
        }
        ablyHelper.unsubscribe(to: channel)
      }
    default:
      break
    }
    ablyHelper.disconnect()
  }
  
  static func viewModel(_ completion: @escaping (MainStatus) -> ()) {
    var currentDiscomfort: Discomfort?
    var currentDiscomfortHelping: Discomfort?
    
    let operation1 = BlockOperation {
      let dispatchGroup = DispatchGroup()
      dispatchGroup.enter()
      DiscomfortService.current { inner in
        currentDiscomfort = try? inner()
        if let aCurrentDiscomfort = currentDiscomfort {
          RealmHelper.isClosed(aCurrentDiscomfort.channel) { closed in
            if closed {
              currentDiscomfort = nil
            }
            dispatchGroup.leave()
          }
        } else {
          DiscomfortService.discomforts { inner2 in
            let discomfortss = try? inner2()
            currentDiscomfort = discomfortss?.filter { discomfort in
              discomfort.status == .created || discomfort.status == .waitingResponse || discomfort.status == .timeout
              }.first
            dispatchGroup.leave()
          }
        }
      }
      dispatchGroup.wait()
    }
    
    let operation2 = BlockOperation {
      let dispatchGroup = DispatchGroup()
      dispatchGroup.enter()
      DiscomfortService.currentHelping { inner in
        do {
          currentDiscomfortHelping = try inner()
        } catch {
          
        }
        dispatchGroup.leave()
      }
      dispatchGroup.wait()
    }
    
    let completionOperation = BlockOperation {
      let mainStatus: MainStatus
      var discomforts = [Discomfort]()
      if let currentDiscomfort = currentDiscomfort {
        discomforts.append(currentDiscomfort)
      }
      if let currentDiscomfortHelping = currentDiscomfortHelping, currentDiscomfortHelping.status != .tribierLogout {
        discomforts.append(currentDiscomfortHelping)
      }
      let sorted = discomforts.sorted { lhs, rhs in
//        let isUserLeft = User.isCurrent(lhs.tribierId) ? -1 : 0
//        let isUserRight = User.isCurrent(rhs.tribierId) ? -1 : 0
        return lhs.status.priority() < rhs.status.priority()// + isUserRight
      }
      if discomforts.count > 0 {
        mainStatus = .inChat(sorted, 0, false)
      } else {
        mainStatus = .noDiscomfort
      }
      completion(mainStatus)
    }
    
    operation2.addDependency(operation1)
    completionOperation.addDependency(operation2)
    
    operationQueue.addOperations([operation1, operation2, completionOperation], waitUntilFinished: false)
  }
  
  func discomfort(channel: String) -> Discomfort? {
    switch status {
    case .inChat(let discomforts, _, _):
      let discomfort = discomforts.filter { $0.channel == channel }.first
      return discomfort
    default:
      return nil
    }
  }
  
  func currentDiscomfort(completion: @escaping (Discomfort?) -> ()) {
    DiscomfortService.current { inner in
      do {
        let discomfort = try inner()
        completion(discomfort)
      } catch {
        completion(nil)
      }
    }
  }
}

// MARK: - Mutating
extension MainNavigationViewModel {
  mutating func received(_ event: Event) -> Bool? {
    switch status {
    case .inChat(let discomforts, let index, let hasNewMessage):
      guard !hasNewMessage && discomforts.count > 1 else {
        return true
      }
      let otherIndex = index == 0 ? 1 : 0
      let discomfort = discomforts[otherIndex]
      if discomfort.channel == event.channel {
        status = .inChat(discomforts, index, true)
        return true
      }
      return false
    default:
      return nil
    }
  }
  
  mutating func changeDiscomfort() -> Discomfort? {
    let newDiscomforts: [Discomfort]
    let newIndex: Int
    switch status {
    case .inChat(let discomforts, let index, _):
      newDiscomforts = discomforts
      newIndex = index == 0 ? 1 : 0
    default:
      return nil
    }
    status = .inChat(newDiscomforts, newIndex, false)
    return newDiscomforts[newIndex]
  }
}

// MARK: - AblyHelper Delegate
extension MainNavigationViewModel: AblyHelperDelegate {
  func handle(_ event: Event) {
    switch event.type {
    case .tribierCanHelping:
      mainNavigationController.presentUserHelpInvite(discomfort: event.discomfort)
    case .tribierAcceptTalking:
      currentDiscomfort { discomfort in
        guard let discomfort = discomfort else {
          return
        }
        self.mainNavigationController.presentUserHelp(discomfort: discomfort)
      }
    case .tribierDeleteAccount, .tribierLogout, .tribierTalkingNonsense, .tribierDisappeared:
      if let name = event.discomfort.tribierNickname, name.characters.count > 0 {
        mainNavigationController.presentUserGone(name: name)
      } else {
        RealmHelper.otherUser(in: event.channel) { name in
          self.mainNavigationController.presentUserGone(name: name)
        }
      }
    case .userDeleteAccount, .userLogout, .userTalkingNonsense, .userDisappeared:
      if let name = event.discomfort.userNickname, name.characters.count > 0 {
        mainNavigationController.presentUserGone(name: name)
      } else {
        RealmHelper.otherUser(in: event.channel) { name in
          self.mainNavigationController.presentUserGone(name: name)
        }
      }
    case .missionAccomplished:
      if let name = event.discomfort.userNickname, name.characters.count > 0 {
        mainNavigationController.presentUserAccomplished(name: name)
      } else {
        RealmHelper.otherUser(in: event.channel) { name in
          self.mainNavigationController.presentUserAccomplished(name: name)
        }
      }
    case .message:
      mainNavigationController.received(event)
    case .tribierRejectTalking:
      mainNavigationController.refresh()
    default:
      break
    }
  }
  
  func append(paginatedResult: ARTPaginatedResult<ARTMessage>) {
    mainNavigationController.paginatedResults.append(paginatedResult)
  }
  
  func remove(paginatedResult: ARTPaginatedResult<ARTMessage>) {
    guard let index = paginatedResults.index(of: paginatedResult) else {
      return
    }
    mainNavigationController.paginatedResults.remove(at: index)
  }
}
