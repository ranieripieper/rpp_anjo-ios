//
//  MenuCellViewModel.swift
//  Tribie
//
//  Created by Gil on 30/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

enum MenuCellType {
  case time
  case accomplished
  case report
  case settings
  
  func iconName() -> String {
    switch self {
    case .time:
      return "icnTime"
    case .accomplished:
      return "icnAccomplished"
    case .report:
      return "icnReport"
    case .settings:
      return "icnSettings"
    }
  }
  
  func text(name: String) -> String {
    switch self {
    case .time:
      return String(format: String.menuDisappearedText(), arguments: [name])
    case .accomplished:
      return String(format: String.menuAccomplishedText(), arguments: [name])
    case .report:
      return String(format: String.menuNonsenseText(), arguments: [name])
    case .settings:
      return String.menuSettingsText()
    }
  }
  
  func selectedText(name: String) -> String {
    switch self {
    case .time:
      return String(format: String.menuDisappearedSelectedText(), arguments: [name])
    case .accomplished:
      return String(format: String.menuAccomplishedSelectedText(), arguments: [name])
    case .report:
      return String(format: String.menuNonsenseSelectedText(), arguments: [name])
    case .settings:
      return String.menuSettingsSelectedText()
    }
  }
  
  func hightlightedBackground() -> Bool {
    switch self {
    case .time:
      return false
    case .accomplished:
      return false
    case .report:
      return false
    case .settings:
      return true
    }
  }
}

struct MenuCellViewModel {
  let iconName: String
  let text: String
  let selectedText: String
  let highlightedBackground: Bool
  
  init(type: MenuCellType, name: String) {
    self.iconName = type.iconName()
    self.text = type.text(name: name)
    self.selectedText = type.selectedText(name: name)
    self.highlightedBackground = type.hightlightedBackground()
  }
}
