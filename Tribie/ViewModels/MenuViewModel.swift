//
//  MenuViewModel.swift
//  Tribie
//
//  Created by Gil on 01/10/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct MenuViewModel {
  let items: [MenuCellType]
  let viewModels: [MenuCellViewModel]
  let discomfort: Discomfort?
  
  init(discomfort: Discomfort?) {
    self.discomfort = discomfort
    let name: String
    if let discomfort = discomfort {
      if User.isCurrent(discomfort.userId ?? 0) {
        self.items = [.time, .report, .accomplished, .settings]
        name = discomfort.otherNickname
      } else {
        self.items = [.time, .report, .settings]
        name = discomfort.userNickname ?? ""
      }
    } else {
      self.items = [.settings]
      name = ""
    }
    self.viewModels = items.flatMap { MenuCellViewModel(type: $0, name: name) }
  }
  
  init(items: [MenuCellType], discomfort: Discomfort?) {
    let name: String
    if User.isCurrent(discomfort?.userId ?? 0) {
      name = discomfort?.otherNickname ?? ""
    } else {
      name = discomfort?.userNickname ?? ""
    }
    self.items = items
    self.viewModels = items.flatMap { MenuCellViewModel(type: $0, name: name) }
    self.discomfort = discomfort
  }
}

// MARK: - Public
extension MenuViewModel {
  mutating func selected(at index: Int, completion: @escaping (_ inner: () throws -> (MenuViewModel, Bool)) -> ()) {
    var items = self.items
    let item = items.remove(at: index)
    let menuViewModel = MenuViewModel(items: items, discomfort: discomfort)
    guard let discomfort = discomfort else {
      completion { return (menuViewModel, false) }
      return
    }
    switch item {
    case .time:
      guard let tribierId = discomfort.tribierId else {
        completion { return (menuViewModel, false) }
        return
      }
      DiscomfortService.disappeared(discomfort.discomfortId, tribierId: tribierId) { inner in
        do {
          try inner()
          completion { return (menuViewModel, true) }
        } catch {
          completion { throw error }
        }
      }
    case .report:
      guard let tribierId = discomfort.tribierId else {
        completion { return (menuViewModel, false) }
        return
      }
      DiscomfortService.nonsense(discomfort.discomfortId, tribierId: tribierId) { inner in
        do {
          try inner()
          completion { return (menuViewModel, true) }
        } catch {
          completion { throw error }
        }
      }
    case .accomplished:
      DiscomfortService.accomplished(discomfort.discomfortId) { inner in
        do {
          try inner()
          completion { return (menuViewModel, true) }
        } catch {
          completion { throw error }
        }
      }
    case .settings:
      completion { return (menuViewModel, false) }
    }
  }
}
