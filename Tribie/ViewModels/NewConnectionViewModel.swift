//
//  NewConnectionViewModel.swift
//  Tribie
//
//  Created by Gil on 30/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct NewConnectionViewModel {
  let discomfort: Discomfort
  
  func accept() {
    DiscomfortService.accept(discomfort.discomfortId) { inner in
      try? inner()
    }
  }
  
  func reject() {
    DiscomfortService.reject(discomfort.discomfortId, report: false) { inner in
      do {
        try inner()
      } catch {
        
      }
    }
  }
  
  func report() {
    DiscomfortService.reject(discomfort.discomfortId, report: true) { inner in
      do {
        try inner()
      } catch {
        
      }
    }
  }
}
