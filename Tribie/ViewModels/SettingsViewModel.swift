//
//  SettingsViewModel.swift
//  Tribie
//
//  Created by Gilson Gil on 25/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

enum SettingsItem {
  case aSwitch(String, Bool, (_ viewModel: SettingsViewModel, _ value: Bool, _ callback: @escaping (Error?) -> ()) -> ())
  case button(String, (_ viewModel: SettingsViewModel, _ callback: @escaping (Error?) -> ()) -> ())
}

struct SettingsViewModel {
  let items: [SettingsItem] = {
    let profileType = User.current?.profileType ?? ""
    let isTribier = profileType == "tribier_user"
    return [
      SettingsItem.aSwitch(String.settingsGetInvites(), isTribier) { viewModel, value, callback in
        viewModel.toggleHelp(value: value, completion: callback)
      },
      SettingsItem.button(String.settingsLogout()) { viewModel, callback in
        SVProgressHUDHelper.show()
        viewModel.logout(callback)
      },
      SettingsItem.button(String.settingsDeleteAccount()) { viewModel, callback in
        viewModel.deleteAccount(callback)
      }
    ]
  }()
  let settingsViewController: SettingsViewController
  
  init(_ viewController: SettingsViewController) {
    settingsViewController = viewController
  }
  
  func toggleHelp(value: Bool, completion: @escaping (Error?) -> ()) {
    SettingsService.becomeTribier(value) { inner in
      do {
        try inner()
        User.current?.setTribier(value)
        completion(nil)
      } catch {
        completion(error)
      }
    }
  }
  
  func logout(_ completion: @escaping (Error?) -> ()) {
    LoginService.signout { inner in
      do {
        try inner()
        User.depersist()
        completion(nil)
      } catch {
        completion(error)
      }
    }
  }
  
  func deleteAccount(_ completion: @escaping (Error?) -> ()) {
    let alertController = AlertController.present(String.settingsDeleteAccountTitle(), message: String.settingsDeleteAccountSubtitle(), style: .action, in: settingsViewController.navigationController!)
    alertController.delegate = settingsViewController
  }
}
