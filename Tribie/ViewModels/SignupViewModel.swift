//
//  SignupViewModel.swift
//  Tribie
//  
//
//  Created by Gilson Gil on 9/14/16. 
//    Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

enum SignUpError: Error, ErrorProtocol {
  case missingField, ageMinor, invalidPassword
  
  func localizedDescription() -> String {
    switch self {
    case .missingField:
      return ""
    case .ageMinor:
      return String.signupErrorAgeMinor()
    case .invalidPassword:
      return String.signupErrorInvalidPassword()
    }
  }
}

struct SignupViewModel {
  let toAPIDateFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = NSLocale.current
    dateFormatter.calendar = NSCalendar.current
    dateFormatter.dateFormat = "dd/MM/yyyy"
    return dateFormatter
  }()

  func signup(name: String?, email: String?, password: String?, birthDate: Date?, completion: @escaping (_ inner: () throws -> ()) -> ()) {
    guard let name = name, name.characters.count > 0, let email = email, email.characters.count > 0, let password = password, password.characters.count > 0, let birthDate = birthDate else {
      completion { throw SignUpError.missingField }
      return
    }
    guard let year = NSCalendar.current.dateComponents([.year], from: birthDate, to: Date()).year, year > 18 else {
      completion { throw SignUpError.ageMinor }
      return
    }
    let date = toAPIDateFormatter.string(from: birthDate)
    SignupService.signup(name, email: email, password: password, birthDate: date, completion: completion)
  }
}
