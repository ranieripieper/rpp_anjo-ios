//
//  AddButtonView.swift
//  Tribie
//
//  Created by Gil on 06/10/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class AddButton: UIButton {
  private static let buttonSize: CGFloat = 26
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  init(target: AnyObject?, action: Selector) {
    super.init(frame: CGRect(x: 0, y: 0, width: AddButton.buttonSize, height: AddButton.buttonSize))
    addTarget(target, action: action, for: .touchUpInside)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.trbTiffanyBlue
    
    let size = AddButton.buttonSize
    
    layer.masksToBounds = true
    layer.cornerRadius = size / 2
    layer.borderColor = UIColor.white.cgColor
    layer.borderWidth = 1
    
    let width: CGFloat = 1
    let padding: CGFloat = 4
    
    let verticalPath = UIBezierPath()
    verticalPath.move(to: .zero)
    verticalPath.addLine(to: CGPoint(x: 0, y: size - 2 * padding))
    
    let verticalLayer = CAShapeLayer()
    verticalLayer.frame = CGRect(x: size / 2, y: padding, width: width, height: size - 2 * padding)
    verticalLayer.path = verticalPath.cgPath
    verticalLayer.strokeColor = UIColor.white.cgColor
    layer.addSublayer(verticalLayer)
    
    let horizontalPath = UIBezierPath()
    horizontalPath.move(to: .zero)
    horizontalPath.addLine(to: CGPoint(x: size - 2 * padding, y: 0))
    
    let horizontalLayer = CAShapeLayer()
    horizontalLayer.frame = CGRect(x: padding, y: size / 2, width: size - 2 * padding, height: width)
    horizontalLayer.path = horizontalPath.cgPath
    horizontalLayer.strokeColor = UIColor.white.cgColor
    layer.addSublayer(horizontalLayer)
  }
}
