//
//  AlertView.swift
//  Tribie
//
//  Created by Gilson Gil on 9/20/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

final class AlertView: UIView {
  internal let hMargin: CGFloat = 16
  internal let vMargin: CGFloat = 20
  internal let duration: TimeInterval = 2
  
  internal let messageLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.textColor = UIColor.white
    label.font = UIFont.avenirMedium(size: 16)
    return label
  }()
  
  internal let message: String
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(message: String) {
    self.message = message
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.trbAmethyst
    
    messageLabel.text = message
    addSubview(messageLabel)
    
    constrain(messageLabel) { messageLabel in
      messageLabel.left == messageLabel.superview!.left + hMargin
      messageLabel.right == messageLabel.superview!.right - hMargin
      messageLabel.top == messageLabel.superview!.top + vMargin
      messageLabel.bottom == messageLabel.superview!.bottom - vMargin
    }
  }
}

// MARK: Public
extension AlertView {
  static func present(error: Error, inView view: UIView?) {
    present(error: error, inView: view, bottomSpacing: 0)
  }
  
  static func present(error: Error, inView view: UIView?, bottomSpacing: CGFloat) {
    let message: String
    switch error {
    case let error as ErrorProtocol:
      message = error.localizedDescription()
    case let error as NSError:
      message = error.localizedDescription
    default:
      message = String.defaultErrorMessage()
    }
    present(message: message, inView: view, bottomSpacing: bottomSpacing)
  }
  
  static func present(message: String, inView view: UIView?) {
    present(message: message, inView: view, bottomSpacing: 0)
  }
  
  static func present(message: String, inView view: UIView?, bottomSpacing: CGFloat) {
    guard let view = view ?? UIApplication.shared.keyWindow else {
      return
    }
    let alert = AlertView(message: message)
    alert.present(view: view, bottomSpacing: bottomSpacing)
  }
}

// MARK: Private
private extension AlertView {
  func present(view: UIView, bottomSpacing: CGFloat) {
    view.addSubview(self)
    constrain(self) { alert in
      alert.left == alert.superview!.left
      alert.right == alert.superview!.right
      alert.bottom == alert.superview!.bottom - bottomSpacing
    }
    layoutIfNeeded()
    transform = CGAffineTransform.identity.translatedBy(x: 0, y: bounds.height + bottomSpacing)
    UIView.animate(withDuration: TribieConstants.defaultAnimationDuration, delay: TribieConstants.defaultAnimationDelay, options: .curveEaseInOut, animations: { [weak self] in
      self?.transform = CGAffineTransform.identity
      }, completion: { [weak self] _ in
        UIView.animate(withDuration: TribieConstants.defaultAnimationDuration, delay: self?.duration ?? TribieConstants.defaultAnimationDelay, options: .curveEaseInOut, animations: {
          self?.transform = CGAffineTransform.identity.translatedBy(x: 0, y: (self?.bounds.height ?? view.bounds.height) + bottomSpacing)
          }, completion: { _ in
            self?.removeFromSuperview()
        })
      })
  }
}
