//
//  CenteredGrayTextField.swift
//  Tribie
//
//  Created by Gilson Gil on 9/19/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class CenteredGrayTextField: UITextField {
  override var placeholder: String? {
    didSet {
      guard let placeholder = placeholder else {
        return
      }
      attributedPlaceholder = NSAttributedString.placeholder(string: placeholder)
    }
  }
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.trbAlmostWhite
    textColor = UIColor.black
    
    textAlignment = .center
    font = UIFont.avenirRoman(size: 18)
  }
  
  override var intrinsicContentSize: CGSize {
    get {
      return CGSize(width: 0, height: 56)
    }
  }
}

// MARK: Public
extension CenteredGrayTextField {
  func validate() {
    let placeholder = self.placeholder?.replacingOccurrences(of: "*", with: "")
    if let text = text, text.characters.count > 0 {
      self.placeholder = placeholder
    } else {
      let string = (placeholder ?? "") + "*"
      attributedPlaceholder = NSAttributedString.placeholderError(string: string)
    }
  }
  
  func installSeePasswordButton() {
    let button = UIButton(type: .custom)
    button.setImage(UIImage.btnVerSenha()?.withRenderingMode(.alwaysTemplate), for: .normal)
    button.setImage(UIImage.icnHidePassword(), for: .selected)
    button.addTarget(self, action: #selector(toggleSecureTextEntry), for: .touchUpInside)
    var size = button.intrinsicContentSize
    size.width += 8
    button.frame = CGRect(origin: .zero, size: size)
    button.tintColor = UIColor.trbPinkishGrey
    rightView = button
    rightViewMode = .always
    leftView = UIView(frame: button.frame)
    leftViewMode = .always
  }
}

// MARK: Actions
extension CenteredGrayTextField {
  func toggleSecureTextEntry(_ button: UIButton) {
    isSecureTextEntry = !isSecureTextEntry
    button.isSelected = !button.isSelected
  }
}
