//
//  ChatBarButtonItem.swift
//  Tribie
//
//  Created by Gilson Gil on 24/09/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

final class ChatBarButtonItem: UIButton {
  fileprivate static let size: CGFloat = 40
  fileprivate static let notificationSize: CGFloat = 8
  fileprivate static let margin: CGFloat = 4
  
  fileprivate var notificationIcon: RoundButton = {
    let button = RoundButton()
    button.isUserInteractionEnabled = false
    button.frame = CGRect(x: 0, y: 0, width: ChatBarButtonItem.notificationSize, height: ChatBarButtonItem.notificationSize)
    button.backgroundColor = UIColor.trbRedPink
    button.layer.borderColor = UIColor.white.cgColor
    button.layer.borderWidth = 1
    button.layer.cornerRadius = ChatBarButtonItem.notificationSize / 2
    button.layer.masksToBounds = true
    return button
  }()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  init(image: UIImage?, target: Any?, action: Selector) {
    super.init(frame: CGRect(x: 0, y: 0, width: ChatBarButtonItem.size, height: ChatBarButtonItem.size))
    setImage(image, for: .normal)
    addTarget(target, action: action, for: .touchUpInside)
    setUp()
  }
  
  private func setUp() {
    imageView?.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
    
    let backgroundView = createBackgroundView()
    insertSubview(backgroundView, belowSubview: imageView!)
    
    addSubview(notificationIcon)
    showNotification(false)
    
    constrain(backgroundView, notificationIcon) { bg, icon in
      bg.top == bg.superview!.top// - ChatBarButtonItem.margin
      bg.left == bg.superview!.left// - ChatBarButtonItem.margin
      bg.bottom == bg.superview!.bottom// + ChatBarButtonItem.margin
      bg.right == bg.superview!.right// + ChatBarButtonItem.margin
      
      icon.top == bg.top + ChatBarButtonItem.margin
      icon.centerX == bg.right
      icon.width == ChatBarButtonItem.notificationSize
      icon.height == ChatBarButtonItem.notificationSize
    }
  }
}

// MARK: - Views
internal extension ChatBarButtonItem {
  func createBackgroundView() -> UIView {
    let view = UIView()
    view.isUserInteractionEnabled = false
    view.backgroundColor = UIColor.trbBlush
    view.layer.borderColor = UIColor.white.cgColor
    view.layer.borderWidth = 1
    view.layer.cornerRadius = ChatBarButtonItem.size / 2// + ChatBarButtonItem.margin
    view.layer.masksToBounds = true
    return view
  }
}

// MARK: - Public
extension ChatBarButtonItem {
  func showNotification(_ show: Bool) {
    notificationIcon.isHidden = !show
  }
}
