//
//  CounterTextView.swift
//  Tribie
//
//  Created by Gilson Gil on 9/16/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Cartography

final class CounterTextView: UIView {
  internal struct Constant {
    static let margin: CGFloat = 8
  }
  internal let allowedCharacters: Int
  internal let textView: UITextView = {
    let textView = UITextView()
    textView.backgroundColor = UIColor.trbAlmostWhite
    textView.textColor = UIColor.trbPinkishGrey
    textView.font = UIFont.avenirRoman(size: 18)
    textView.setContentHuggingPriority(UILayoutPriorityRequired, for: .vertical)
    textView.textContainerInset = UIEdgeInsets(top: Constant.margin, left: Constant.margin, bottom: Constant.margin, right: Constant.margin)
    textView.text = String.createProblemDescriptionPlaceholder()
    return textView
  }()
  internal let counterLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .right
    label.textColor = UIColor.trbWhiteTwo
    label.font = UIFont.avenirMedium(size: 16)
    return label
  }()
  
  required init?(coder aDecoder: NSCoder) {
    allowedCharacters = Int(MAXFLOAT)
    super.init(coder: aDecoder)
    setUp()
  }
  
  init(allowedCharacters: Int) {
    self.allowedCharacters = allowedCharacters
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    textView.delegate = self
    addSubview(textView)
    
    counterLabel.text = "0 / \(allowedCharacters)"
    addSubview(counterLabel)
    
    let margin = Constant.margin
    constrain(textView, counterLabel) { textView, label in
      textView.top == textView.superview!.top
      textView.left == textView.superview!.left
      textView.right == textView.superview!.right
      
      label.top == textView.bottom + margin
      label.left == label.superview!.left
      label.bottom == label.superview!.bottom
      label.right == label.superview!.right
    }
  }
}

// MARK: - Public
extension CounterTextView {
  var text: String? {
    guard textView.text != String.createProblemDescriptionPlaceholder() else {
      return nil
    }
    return textView.text
  }
}

// MARK: - UITextView Delegate
extension CounterTextView: UITextViewDelegate {
  func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    guard let text = textView.text?.replacingCharacters(in: range.range(for: textView.text!)!, with: text) else {
      return true
    }
    let count = text.characters.count
    guard count <= allowedCharacters else {
      return false
    }
    counterLabel.text = "\(count) / \(allowedCharacters)"
    return true
  }
  
  func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
    if textView.text == String.createProblemDescriptionPlaceholder() {
      textView.text = ""
    }
    return true
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    if textView.text == "" {
      textView.text = String.createProblemDescriptionPlaceholder()
    }
  }
}
