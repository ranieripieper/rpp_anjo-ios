//
//  MultipleLineButton.swift
//  Tribie
//
//  Created by Gilson Gil on 9/19/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class MultipleLineButton: UIButton {
  var highlightColor: UIColor?
  var highlightString: String?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    titleLabel?.numberOfLines = 0
    titleLabel?.textAlignment = .center
    setTitleColor(UIColor.black, for: .normal)
  }
  
  override func setTitle(_ title: String?, for state: UIControlState) {
    guard let highlightColor = highlightColor, let highlightString = highlightString, let aTitle = title else {
      super.setTitle(title, for: state)
      return
    }
    let font = titleLabel?.font ?? UIFont.boldSystemFont(ofSize: 17)
    let range = (aTitle as NSString).range(of: highlightString)
    let attr = NSMutableAttributedString(string: title ?? "", attributes: [NSForegroundColorAttributeName: titleColor(for: state) ?? UIColor.black, NSFontAttributeName: font])
    attr.addAttributes([NSForegroundColorAttributeName: highlightColor], range: range)
    setAttributedTitle(attr, for: state)
  }
}
