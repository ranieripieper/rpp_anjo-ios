//
//  RoundButton.swift
//  Tribie
//
//  Created by Gilson Gil on 9/21/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class RoundButton: UIButton {
  override var bounds: CGRect {
    didSet {
      layer.cornerRadius = bounds.height / 2
    }
  }
}
