//
//  RoundedCornerButton.swift
//  Tribie
//
//  Created by Gilson Gil on 9/19/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class RoundedCornerButton: UIButton {
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    layer.cornerRadius = 8
    titleLabel?.font = UIFont.avenirHeavy(size: 18)
    setTitleColor(UIColor.white, for: .normal)
    setTitleColor(UIColor(white: 0.8, alpha: 1), for: .highlighted)
  }
  
  override var intrinsicContentSize: CGSize {
    get {
      return CGSize(width: 0, height: 64)
    }
  }
}
